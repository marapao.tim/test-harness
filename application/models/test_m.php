<?php

class test_m extends MY_Model {

	protected $_table_name = 'srn_test';

	function __construct() {
        parent::__construct();	
    }

    function getTest() {
		$this->db->select('t.id, t.full_name, t.age, t.gender');
		$this->db->from($this->_table_name . ' as t'); 
		return $this->db->get()->result();
	}

	function getTestSearch($name) {
		$this->db->select('t.id, t.full_name, t.age, t.gender');
		$this->db->from($this->_table_name . ' as t');  
		$this->db->or_like('t.full_name', $name, 'both'); 
		$this->db->or_like('t.age', $name, 'both');
		$this->db->or_like('t.gender',$name, 'after'); 
		return $this->db->get()->result();
	}

}