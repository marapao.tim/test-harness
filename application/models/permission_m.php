<?php

class Permission_M extends My_Model {

	protected $_table_name 			= 'srn_permissions';
	protected $_primary_key 		= 'permission_id';
	protected $_primary_filter 		= 'intval';
	protected $_order_by 			= 'permission_id';
	protected $_order 				= 'ASC';
	public $rules 					= array();
	protected $_timestamps 			= TRUE;
	private $users_table 			= 'srn_master_users';

	function __construct() {
		parent::__construct();
	}

	function getPermissions($order_by = null) {
		$this->db->select('p.permission_id, p.permission_title, p.permission_code, p.permission_group, p.date_created, p.date_modified, p.permission_description, c.user_id, c.user_fullname as created_by, m.user_id, m.user_fullname as modified_by');
		$this->db->from($this->_table_name . ' as p');
		$this->db->join($this->users_table . ' as c', 'p.created_by = c.user_id');
		$this->db->join($this->users_table . ' as m', 'p.created_by = m.user_id');
		if($order_by) {
			foreach ($order_by as $key => $value) {
				$this->db->order_by($value['column'], $value['order']);
			}
		} else {
			$this->db->order_by($this->_order_by, $this->_order);
		}
		return $this->db->get()->result();
	}

	function savePermission($id = null) {
		if(isset($_POST['redirect'])) {
 			$redirect = $_POST['redirect'];
 			unset($_POST['redirect']);
 		}

		$this->save($this->input->post(), $id);

		if($id) {
			$this->session->set_flashdata('alert', 'Theme successfully updated.');
		} else {
			$this->session->set_flashdata('alert', 'Theme successfully added.');
		}

		$this->session->set_flashdata('alert_type','success');

		//save log
		$this->logs_m->save_log('Add Theme', serialize($this->input->post()));

		if(isset($redirect)) {
 			redirect($redirect);
 		} else {
 			redirect('themes/themes_management');
 		}
	}
}

?>