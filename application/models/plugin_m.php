<?php 

class Plugin_M extends MY_Model {

	protected $_table_name = 'srn_plugins';
	protected $_primary_key = 'plugin_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'plugin_id';
	protected $_order = 'DESC';
	protected $_timestamps = TRUE;
	public $rules = array();

	function __construct() {
        parent::__construct();

        $this->load->helper('array');
    }

 	function get_new() {
 		$plugin = new stdClass();

 		$plugin->title = '';
 		$plugin->author = '';
 		$plugin->current_version = 1;
 		$plugin->category = '';
 		$plugin->description = '';
 		$plugin->version_metas = '';
 		$plugin->icon = '';
 		$plugin->home_page = '';
 		$plugin->documentation = '';

 		return $plugin;
 	}   

 	function save_plugin($id = NULL) {
 		if(isset($_POST['redirect'])) {
 			$redirect = $_POST['redirect'];
 			unset($_POST['redirect']);
 		}

 		$_POST = elements(array('name','author','category','description','home_page','documentation'), $_POST);

 		if($id) {
 			$plugin_path = FCPATH.'/plugins/'.$id;

			if(isset($_FILES['icon'])) {
				$ext = explode('.',$_FILES['icon']['name']);
				$ext = array_pop($ext);

				$_POST['icon'] = 'icon.'.$ext;

				//upload icon
				$config['upload_path'] = $plugin_path;
				$config['allowed_types'] = 'gif|jpg|png';
				$config['file_name'] = 'icon.'.$ext;
				$config['max_size']	= '2000';
				$config['max_height'] = '5000';
				$config['max_width'] = '5000';
				$config['overwrite'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if($this->upload->do_upload('icon')) {
					
					$config['image_library'] = 'gd2';
					$config['source_image']	= $plugin_path.'/'.'icon.'.$ext;
					$config['maintain_ratio'] = TRUE;
					$config['width']	= 150;
					$config['height']	= 150;

					$this->load->library('image_lib', $config); 

					$this->image_lib->resize();
				} else {
					$_POST['icon'] = 'icon.png';
					copy(FCPATH.'assets/img/default-plugin.png',$plugin_path.'/icon.png');
				}
			}

 			$this->save($this->input->post(), $id);
 			$this->session->set_flashdata('alert_type','success');
 			$this->session->set_flashdata('alert', 'Plugin successfully updated.');
 		} else {
 			$filename = md5(microtime());

 			$version_meta = array(
 				"1.00" => array(
 					'release_date' => date('Y-m-d H:i:s'),
 					'version'      => '1.00',
 					'filename'     => $filename.'.zip',
 					'remarks'      => 'Initial Release',
 					'updated_by'   => $this->session->userdata('user_id')
 				)
 			);

 			$_POST['version_metas'] = serialize($version_meta);
 			$_POST['current_version'] = 1.00;
 			$this->save($this->input->post());

	 		$plugin_path = FCPATH.'/plugins/'.$this->db->insert_id();
	 		$versions_path = $plugin_path.'/versions';
	 		$screenshots_path = $plugin_path.'/images';
	 		$extract_path = $versions_path.'/1.00';

			file_exists($plugin_path) || mkdir($plugin_path,0777);
			file_exists($versions_path) || mkdir($versions_path,0777);
			file_exists($screenshots_path) || mkdir($screenshots_path,0777);
			file_exists($extract_path) || mkdir($extract_path,0777);

			$config['upload_path'] = $extract_path;
			$config['allowed_types'] = 'zip';
			$config['file_name'] = $filename.'.zip';
			$config['max_size']	= '0';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('file')) {

				if(isset($_FILES['icon'])  && $_FILES['icon']['name'] !== '') {
					$ext = explode('.',$_FILES['icon']['name']);
					$ext = array_pop($ext);

					$file_data = array('icon' => 'icon.'.$ext);
					$this->save($file_data, $this->db->insert_id());

					//upload icon
					$config['upload_path'] = $plugin_path;
					$config['allowed_types'] = 'gif|jpg|png';
					$config['file_name'] = 'icon.'.$ext;
					$config['max_size']	= '2000';
					$config['max_height'] = '5000';
					$config['max_width'] = '5000';

					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					if($this->upload->do_upload('icon')) {
						
						$config['image_library'] = 'gd2';
						$config['source_image']	= $plugin_path.'/'.'icon.'.$ext;
						$config['maintain_ratio'] = TRUE;
						$config['width']	= 150;
						$config['height']	= 150;

						$this->load->library('image_lib', $config); 

						$this->image_lib->resize();
					} else {
						$file_data = array('icon' => 'icon.png');
						$this->save($file_data, $this->db->insert_id());
						copy(FCPATH.'assets/img/default-plugin.png',$plugin_path.'/icon.png');
					}
				} else {
					$file_data = array('icon' => 'icon.png');
					$this->save($file_data, $this->db->insert_id());
					copy(FCPATH.'assets/img/default-plugin.png',$plugin_path.'/icon.png');
				}

				$this->session->set_flashdata('alert_type','success');
				//save log
				$this->logs_m->save_log('Add Plugin', serialize($this->input->post()));
				$this->session->set_flashdata('alert', 'Plugin successfully added.');
			} else {
				$this->delete($this->db->insert_id()); //delete the database entry
				$this->session->set_flashdata('alert', 'Problem uploading the file.');
				$this->session->set_flashdata('alert_type','danger');
			}
 		}	

 		if(isset($redirect)) {
 			redirect($redirect);
 		} else {
 			redirect('plugin/plugin_management');
 		}
 	}

 	function update_plugin($plugin_id) {
 		$plugin = $this->get($plugin_id);
 		$version_meta = unserialize($plugin->version_metas);
 		$new_version = number_format($this->input->post('version'), 2);
 		$filename = md5(microtime());

 		$plugin_path = FCPATH.'/plugins/'.$plugin_id;
 		$versions_path = $plugin_path.'/versions';
 		$screenshots_path = $plugin_path.'/images';
 		$extract_path = $versions_path.'/'.$new_version;

		file_exists($plugin_path) || mkdir($plugin_path,0777);
		file_exists($versions_path) || mkdir($versions_path,0777);
		file_exists($screenshots_path) || mkdir($screenshots_path,0777);
		file_exists($extract_path) || mkdir($extract_path,0777);

		$config['upload_path'] = $extract_path;
		$config['allowed_types'] = 'zip';
		$config['file_name'] = $filename.'.zip';
		$config['max_size']	= '0';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if($this->upload->do_upload('file')) {

	 		$version_meta[$new_version] = array(
					'release_date' => date('Y-m-d H:i:s'),
					'version'      => $new_version,
					'filename'     => $filename.'.zip',
					'remarks'      => $this->input->post('version_remarks'),
					'updated_by'   => $this->session->userdata('user_id')
			);

	 		$new_plugin_data = array(
	 			'current_version' => $new_version,
	 			'version_metas' => serialize($version_meta)
	 		);

	 		$this->save($new_plugin_data, $plugin_id);

			$this->session->set_flashdata('alert_type','success');
			$this->session->set_flashdata('alert', 'Plugin successfully updated.');

			//save log
			$this->logs_m->save_log('Update Plugin', serialize($this->input->post()));
		} else {
			$this->session->set_flashdata('alert', 'Problem uploading the file.');
			$this->session->set_flashdata('alert_type','danger');
		}
 		
 		if(isset($redirect)) {
 			redirect($redirect);
 		} else {
 			redirect('plugin/plugin_management');
 		}
		
 	}

 	function delete_plugin($plugin_id) {
 		
 	}
}