<?php 

class Jmeter_Testscripts_M extends MY_Model {

	protected $_table_name = 'srn_jmeter_testscripts';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'id';
	protected $_order = 'DESC';
	protected $_timestamps = TRUE;
	public $rules = array();

	function __construct() {
        parent::__construct();

        //$this->load->model('theme_meta_m');
        $this->load->model("db_write_m");
    	$this->db_write_m->create("srn_jmeter_testscripts");
    }

 	function get_new() {
 		$project = new stdClass();

 		$project->project_title = '';
 		$project->project_name = '';
 		$project->project_description = '';
 		$project->themes = '';
 		$project->tools = '';
 		$project->config_meta = '';
 		$project->client_name = '';

 		return $project;
 	}   

 	function get_projects() {
 		$this->db->select('project');
		$this->db->distinct();
		$result = $this->db->get('srn_jmeter_testscripts')->result();
		return $result;
 	}

 	function create_testscript(){
 		$date = date("Y-m-d H:i:s");

 		$arr = $this->input->post();

 		$arr["datecreated"] = $date;
 		$arr["datemodified"] = $date;
 		$arr["createdby"] = $this->session->userdata('user_fullname');

		//var_dump($arr);
 		$this->db->insert('srn_jmeter_testscripts', $arr);
 		$id = $this->db->insert_id();
		//$this->session->set_flashdata('alert', 'Template successfully added.');
		//save log
		$this->logs_m->save_log('Create Jmeter Test Script', serialize($arr));

		return $id;

 	}


 	function update_template($id){
 		$date = date("Y-m-d H:i:s");

 		$arr = $this->input->post();

 		$arr["datemodified"] = $date;
 		
		//var_dump($arr);
		$this->db->where('id', $id);
 		$this->db->update('srn_jmeter_testscripts', $arr);
 		//$this->session->set_flashdata('alert', 'Template successfully added.');
		//save log
		$this->logs_m->save_log('Update Jmeter Test Script', serialize($arr));

 	}
}