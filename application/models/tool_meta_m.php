<?php 

class Tool_Meta_M extends MY_Model {

	protected $_table_name = 'srn_toolmeta';
	protected $_primary_key = 'tm_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'tm_id';
	protected $_order = 'DESC';
	protected $_timestamps = TRUE;
	public $rules = array();

	function __construct() {
        parent::__construct();
    }
}