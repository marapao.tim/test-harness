<?php

class User_Role_M extends My_Model {

	protected $_table_name 			= 'srn_user_roles';
	protected $_primary_key 		= 'user_role_id';
	protected $_primary_filter 		= 'intval';
	protected $_order_by 			= 'user_role_id';
	protected $_order 				= 'DESC';
	public $rules 					= array();
	protected $_timestamps 			= TRUE;
	private $users_table 			= 'srn_master_users';

	function __construct() {
		parent::__construct();
	}

	function getUserRoles() {
/*		$this->db->select('ur.user_role_id, ur.user_role_title, ur.user_role_code, ur.user_role_permissions, ur.date_created, ur.date_modified, c.user_id, c.user_fullname as created_by, m.user_id, m.user_fullname as modified_by');
		$this->db->from($this->_table_name . ' as ur');
		$this->db->join($this->users_table . ' as c', 'ur.created_by = c.user_id');
		$this->db->join($this->users_table . ' as m', 'ur.created_by = m.user_id');*/
		
		//return $this->db->get('srn_user_roles');
	}

	function saveUserRole($id = null) {
		if(isset($_POST['redirect'])) {
 			$redirect = $_POST['redirect'];
 			unset($_POST['redirect']);
 		}

		$this->save($this->input->post(), $id);

		if($id) {
			$this->session->set_flashdata('alert', 'Role successfully updated.');
		} else {
			$this->session->set_flashdata('alert', 'Role successfully added.');
		}

		$this->session->set_flashdata('alert_type','success');

		//save log
		$this->logs_m->save_log('Add Role', serialize($this->input->post()));

		if(isset($redirect)) {
 			redirect($redirect);
 		} else {
 			redirect('themes/themes_management');
 		}
	}
}

?>