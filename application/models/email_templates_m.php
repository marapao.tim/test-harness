<?php 

class Email_Templates_M extends MY_Model {

	protected $_table_name = 'srn_email_templates';
	protected $_primary_key = 'template_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'template_id';
	protected $_order = 'DESC';
	protected $_timestamps = TRUE;
	public $rules = array();

	function __construct() {
        parent::__construct();

        //$this->load->model('theme_meta_m');
        $this->load->model("db_write_m");
    	$this->db_write_m->create("srn_email_templates");
    }

 	function get_new() {
 		$project = new stdClass();

 		$project->project_title = '';
 		$project->project_name = '';
 		$project->project_description = '';
 		$project->themes = '';
 		$project->tools = '';
 		$project->config_meta = '';
 		$project->client_name = '';

 		return $project;
 	}   

 	function get_template_categories() {
 		$this->db->select('template_module');
		$this->db->distinct();
		$result = $this->db->get('srn_email_templates')->result();
		return $result;
 	}

 	function create_template(){
 		$date = date("Y-m-d H:i:s");

 		$arr = $this->input->post();

 		$arr["template_datecreated"] = $date;
 		$arr["template_datemodified"] = $date;
 		$arr["createdby"] = $this->session->userdata('user_fullname');

		//var_dump($arr);
 		$this->db->insert('srn_email_templates', $arr);
 		$id = $this->db->insert_id();
		//$this->session->set_flashdata('alert', 'Template successfully added.');
		//save log
		$this->logs_m->save_log('Create Email Template', serialize($arr));

		return $id;

 	}


 	function update_template($id){
 		$date = date("Y-m-d H:i:s");

 		$arr = $this->input->post();

 		$arr["template_body"] = trim($arr["template_body"]);
 		$arr["template_description"] = trim($arr["template_description"]);

 		$arr["template_datemodified"] = $date;
 		
		//var_dump($arr);
		$this->db->where('template_id', $id);
 		$this->db->update('srn_email_templates', $arr);
 		//$this->session->set_flashdata('alert', 'Template successfully added.');
		//save log
		$this->logs_m->save_log('Update Email Template', serialize($arr));

 	}
}