<?php 

class User_M extends MY_Model {

	protected $_table_name = 'srn_master_users';
	protected $_primary_key = 'user_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'user_id';
	protected $_order = 'ASC';
	protected $_timestamps = TRUE;
	public $rules = array();
	public $user_roles_table = "srn_user_roles";

	function __construct() {
        parent::__construct();	
    }

    function get_new() {
    	$user = new stdClass();

    	$user->user_name = '';
    	$user->user_password = '';
    	$user->user_fullname = '';
    	$user->user_email = '';

    	return $user;
    }

    function getUsers($order_by = null) {
		$this->db->select('u.user_id, u.user_name, u.user_fullname, u.user_email, u.date_created, u.date_modified, ur.user_role_id, ur.user_role_title');
		$this->db->from($this->_table_name . ' as u');
		$this->db->join($this->user_roles_table . ' as ur', 'u.user_role_id = ur.user_role_id');
		
		return $this->db->get()->result();
	}

 	function login() {

		$user = $this->get_by(array(
			'user_name' => $this->input->post('user_name'),
			'user_password' => md5($this->input->post('user_password')),
		), TRUE);
		
		if (count($user)) {
			//check if activated or deactivated

/*			if($user->status == 'active') {*/
				// Log in user
				$data = $user;
				$data->loggedin = true;
				$this->session->set_userdata($data);

				//add log
				//$this->logs_m->save_log($user->user_id, 'Login', NULL);

				//redirect to a specific page if redirect is true
				if($this->input->post('redirect')) {
					redirect($this->input->post('redirect'));
				} else {
					redirect('main');
				}
/*			} else {
				$this->session->set_flashdata('alert', 'Account is deactivated.');
				$this->session->set_flashdata('alert_type', 'danger');
				redirect('login','refresh');
			}*/
		} else {
			$this->session->set_flashdata('alert', 'Invalid login credentials.');
			$this->session->set_flashdata('alert_type', 'danger');
			redirect('login','refresh');
		}
 	}

 	function loggedin () {
		return (bool) $this->session->userdata('loggedin');
	}

	function checkIfAdmin($user_id) {
		$user = $this->get($user_id);
		
		if($user->user_role_id == 0 || $user->user_role_id == 1) {
			return true;
		}

		return false;
	}

	function checkPermissions($request_permission, $secure_page = false){
		$this->config->load("permissions");
		$site_permissions = $this->config->item("site_permissions");

		$user_role_id = $this->session->userdata('user_role_id');

		if($user_role_id == 0){
			return true;
		}
		else{

			$this->db->select('user_role_permissions');
			$this->db->from($this->user_roles_table);
			$this->db->where("user_role_id",$user_role_id);
			$role = $this->db->get()->result();
			
			if(isset($role[0])){
				$perms = $role[0]->user_role_permissions;
				$perms = unserialize($perms);

				if(is_array($request_permission)){
					$result = array_intersect($request_permission, $perms);
					$ret = (count($result) > 0 ? true : false);
					if($ret){
						return true;
					}
					else{
						if($secure_page){
							redirect('denied');
						}
						return false;
					}
				}
				else{
					$ret = in_array($request_permission, $perms);

					if($ret){
						return true;
					}
					else{
						if($secure_page){
							redirect('denied');
						}
						return false;
					}
				}
			}
			
		}
	}

	function save_user($user_id = NULL) {
		if($user_id) {
			$this->save($this->input->post(), $user_id);
			$this->session->set_flashdata('alert', 'User successfully updated.');
		} else {
			$_POST['user_password'] = md5($this->input->post('user_password'));

			$this->save($this->input->post());
			$this->session->set_flashdata('alert', 'User successfully added.');
		}

		$this->session->set_flashdata('alert_type', 'success');
		redirect('users/user_management');
	}

/*	function deactivate_user($user_id) {
		$this->logs_m->save_log($this->session->userdata('user_id'), $this->log_types['user']['deactivate'], NULL);
		$user_data = array('status' => 'inactive');
		$this->save($user_data, $user_id);

		$this->session->set_flashdata('alert', $this->alert_messages['user']['deactivate']['success']);
		$this->session->set_flashdata('alert_type', 'success');

		redirect('users/view_users');
	}	

	function activate_user($user_id) {
		$this->logs_m->save_log($this->session->userdata('user_id'), $this->log_types['user']['activate'], NULL);
		$user_data = array('status' => 'active');
		$this->save($user_data, $user_id);

		$this->session->set_flashdata('alert', $this->alert_messages['user']['activate']['success']);
		$this->session->set_flashdata('alert_type', 'success');

		redirect('users/view_users');
	}*/	

	function change_password($user_id) {
		$user_data = array('user_password' => md5($this->input->post('user_password')));
		$this->save($user_data, $user_id);

		$this->session->set_flashdata('alert', 'Password successfully changed.');
		$this->session->set_flashdata('alert_type', 'success');

		redirect('users/user_management');
	}
}