<?php 

class Theme_M extends MY_Model {

	protected $_table_name = 'srn_themes';
	protected $_primary_key = 'theme_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'theme_id';
	protected $_order = 'DESC';
	protected $_timestamps = TRUE;
	public $rules = array();

	function __construct() {
        parent::__construct();

        $this->load->model('theme_meta_m');
    }

 	function get_new() {
 		$theme = new stdClass();

 		$theme->theme_title = 'New Theme';
 		$theme->theme_name = '';
 		$theme->theme_description = '';
 		$theme->theme_tags = '';

 		return $theme;
 	}   

 	function save_theme($id = NULL) {
 		//add validation if theme name already exists
 		if(isset($_POST['redirect'])) {
 			$redirect = $_POST['redirect'];
 			unset($_POST['redirect']);
 		}

 		if($id) {
 			$this->save($this->input->post(), $id);
 			$this->session->set_flashdata('alert', 'Theme successfully updated.');
 		} else {
	 		//create dir
	 		$extract_path = APPPATH.'../themes/'.$this->input->post('theme_name');
			file_exists($extract_path) || mkdir($extract_path,0777);

			//construct name then upload
			$filepath = $extract_path.'/'.$this->input->post('theme_name').'.zip';
			move_uploaded_file($_FILES['theme_file']['tmp_name'], $filepath);

			//unzip the file
			$zip = new ZipArchive;
			$open_zip = $zip->open($filepath);
			if ($open_zip === TRUE) {
				$zip->extractTo($extract_path);
				$zip->close();
			} else {
				$this->session->set_flashdata('alert', 'Problem unzipping the file.');
				$this->session->set_flashdata('alert_type','danger');
		 		if(isset($redirect)) {
		 			redirect($redirect);
		 		} else {
		 			redirect('themes/themes_management');
		 		}
			}
 			
 			$this->save($this->input->post());
 			$this->session->set_flashdata('alert', 'Theme successfully added.');
 		}	

		$this->session->set_flashdata('alert_type','success');
		//save log
		$this->logs_m->save_log('Add Theme', serialize($this->input->post()));

 		if(isset($redirect)) {
 			redirect($redirect);
 		} else {
 			redirect('themes/themes_management');
 		}
 	}

 	function update_theme($theme_id) {
 		//add validation if theme name already exists
 		if(isset($_POST['redirect'])) {
 			$redirect = $_POST['redirect'];
 			unset($_POST['redirect']);
 		}
 		
 		$theme = $this->get($theme_id);

 		$meta = array('version_number' => $this->input->post('theme_version'), 'version_remarks' => $this->input->post('version_remarks'));

 		unset($_POST['version_remarks']);

 		//save meta
 		$meta_array = array(
 			'theme_id'  => $theme_id,
 			'thm_key'   => 'version',
 			'thm_value' => serialize($meta)
 		);

 		$this->theme_meta_m->save($meta_array);

 		//create dir
 		$extract_path = APPPATH.'../themes/'.$theme->theme_name;
		file_exists($extract_path) || mkdir($extract_path,0777);

		//construct name then upload
		$filepath = $extract_path.'/'.$theme->theme_name.'.zip';
		move_uploaded_file($_FILES['theme_file']['tmp_name'], $filepath);

		//unzip the file
		$zip = new ZipArchive;
		$open_zip = $zip->open($filepath);
		if ($open_zip === TRUE) {
			$zip->extractTo($extract_path);
			$zip->close();
		} else {
			$this->session->set_flashdata('alert', 'Problem unzipping the file.');
			$this->session->set_flashdata('alert_type','danger');

	 		if(isset($redirect)) {
	 			redirect($redirect);
	 		} else {
	 			redirect('themes/themes_management');
	 		}
		}

		$this->save($this->input->post(), $theme_id);
		$this->session->set_flashdata('alert_type','success');
		$this->session->set_flashdata('alert', 'Theme successfully updated.');

		//save log
		$this->logs_m->save_log('Update Theme', serialize($this->input->post()));

 		if(isset($redirect)) {
 			redirect($redirect);
 		} else {
 			redirect('themes/themes_management');
 		}
 	}

 	function get_tool_categories() {
 		$this->db->select('tool_category');
		$this->db->distinct();
		$result = $this->db->get('srn_tools')->result();
		return $result;
 	}
}