<?php 

class Jmeter_Testrunners_M extends MY_Model {

	protected $_table_name = 'srn_jmeter_runner';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'id';
	protected $_order = 'DESC';
	protected $_timestamps = TRUE;
	public $rules = array();

	function __construct() {
        parent::__construct();

        //$this->load->model('theme_meta_m');
        $this->load->model("db_write_m");
    	$this->db_write_m->create("srn_jmeter_runner");

    	$this->db_write_m->create("srn_jmeter_runner_history");
    }


 	function create_runner(){
 		$date = date("Y-m-d H:i:s");

 		$arr = $this->input->post();

 		$arr["datecreated"] = $date;
 		$arr["datemodified"] = $date;
 		$arr["createdby"] = $this->session->userdata('user_fullname');
 		$arr["schedule_type"] = $arr["schedule"]["recur"];
 		switch($arr["schedule_type"]){
 			case "Daily":
 				$arr["schedule_value"] = $arr["schedule"]["daily"];
 			break;
 			case "Weekly":
 				$arr["schedule_value"] = $arr["schedule"]["weekly"];
 			break;
 			case "Monthly":
 				$arr["schedule_value"] = $arr["schedule"]["monthly"];
 			break;
 		}
 		unset($arr["schedule"]);
		//var_dump($arr);
 		$this->db->insert('srn_jmeter_runner', $arr);
 		$id = $this->db->insert_id();
		//$this->session->set_flashdata('alert', 'Template successfully added.');
		//save log
		$this->logs_m->save_log('Create Jmeter Test Runner', serialize($arr));

		return $id;

 	}

 	function get_runners(){

 		$this->db->select('r.id,r.name,r.schedule_type,r.schedule_value,r.status,r.run_count,r.last_run_date,r.createdby,r.datecreated,t.name as test_script');
		$this->db->from("srn_jmeter_runner r");
		$this->db->join("srn_jmeter_testscripts t","r.test_script = t.id","left");
		$this->db->order_by("r.id desc");
		return $this->db->get()->result();
 	}

 	function get_runner_api($hour = 0, $week = 0, $month = 0){

 		$this->db->select('r.id,r.name,r.schedule_type,r.schedule_value,r.status,r.run_count,r.last_run_date,r.createdby,r.datecreated,t.script as test_script');
		$this->db->from("srn_jmeter_runner r");
		$this->db->join("srn_jmeter_testscripts t","r.test_script = t.id","left");
		$this->db->order_by("r.id desc");
		$this->db->where("(r.schedule_type='Daily' and r.schedule_value=".$hour.") or (r.schedule_type='Weekly' and r.schedule_value=".$week." and ".($hour == 0 ? "true" : "false").") or (r.schedule_type='Monthly' and r.schedule_value=".$month." and ".($hour == 0 ? "true" : "false").")");
		return $this->db->get()->result();
 	}

 	function refresh_runner($id){
 		$this->db->select("run_count");
 		$this->db->from("srn_jmeter_runner");
		$this->db->where('id', $id);
		$row = $this->db->get()->row();
		
		$arr = array(
			"run_count"=>$row->run_count+1,
			"last_run_date"=>date("Y-m-d H:i:s")
		);
 		$this->db->where('id', $id);
 		$this->db->update('srn_jmeter_runner', $arr);
 	}

 	function create_runner_history($id){
 		$p = $this->input->get();

		$arr = array(
			"runner_id"=>$id,
			"datecreated"=>date("Y-m-d H:i:s"),
			"status"=>$p["status"]
		);
 		
 		//$this->db->where('id', $id);
 		//$this->db->update('srn_jmeter_runner_history', $arr);

 		$this->db->insert('srn_jmeter_runner_history', $arr);
 	}

 	function get_history($id){
 		$this->db->select("*");
 		$this->db->from("srn_jmeter_runner_history");
		$this->db->where('runner_id', $id);
		$this->db->order_by("id desc");
		$row = $this->db->get()->result();
		
		return $row;
 	}
}