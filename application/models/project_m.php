<?php 

class Project_M extends MY_Model {

	protected $_table_name = 'srn_projects';
	protected $_primary_key = 'project_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'project_id';
	protected $_order = 'DESC';
	protected $_timestamps = TRUE;
	public $rules = array();

	function __construct() {
        parent::__construct();

        //$this->load->model('theme_meta_m');
    }

 	function get_new() {
 		$project = new stdClass();

 		$project->project_title = '';
 		$project->project_name = '';
 		$project->project_description = '';
 		$project->themes = '';
 		$project->tools = '';
 		$project->config_meta = '';
 		$project->client_name = '';

 		return $project;
 	}   

 	function get_inprogress_projects(){
 		$this->db->select('*');
 		$this->db->from('srn_projects');
 		$this->db->where('project_status','In Progress');
		$result = $this->db->get()->result();
		return $result;
 	}

 	function save_project($id = NULL) {
 		//add validation if project name already exists
 		if(isset($_POST['redirect'])) {
 			$redirect = $_POST['redirect'];
 			unset($_POST['redirect']);
 		}

 		$post_arrays = array('themes','tools','config_meta');
 		foreach ($post_arrays as $post_array) {
	 		if(isset($_POST[$post_array])) {
	 			if($post_array == 'themes' || $post_array == 'tools') {
	 				$_POST[$post_array] = implode(',',$_POST[$post_array]);
	 			} else {
	 				$_POST[$post_array] = serialize($_POST[$post_array]);
	 			}
	 		}
 		}

 		if($id) {
 			$this->save($this->input->post(), $id);
 			$this->session->set_flashdata('alert', 'Project successfully updated.');
 			//save log
			$this->logs_m->save_log('Update Project', serialize($this->input->post()));
 		} else {
 			$this->save($this->input->post());
 			$this->session->set_flashdata('alert', 'Project successfully added.');
 			//save log
			$this->logs_m->save_log('Add Project', serialize($this->input->post()));
 		}	

		$this->session->set_flashdata('alert_type','success');
		

 		if(isset($redirect)) {
 			redirect($redirect);
 		} else {
 			redirect('projects/project_management');
 		}
 	}

 	function update_theme($theme_id) {
 		//add validation if theme name already exists
 		if(isset($_POST['redirect'])) {
 			$redirect = $_POST['redirect'];
 			unset($_POST['redirect']);
 		}
 		
 		$theme = $this->get($theme_id);

 		$meta = array('version_number' => $this->input->post('theme_version'), 'version_remarks' => $this->input->post('version_remarks'));

 		unset($_POST['version_remarks']);

 		//save meta
 		$meta_array = array(
 			'theme_id'  => $theme_id,
 			'thm_key'   => 'version',
 			'thm_value' => serialize($meta)
 		);

 		$this->theme_meta_m->save($meta_array);

 		//create dir
 		$extract_path = APPPATH.'../themes/'.$theme->theme_name;
		file_exists($extract_path) || mkdir($extract_path,0777);

		//construct name then upload
		$filepath = $extract_path.'/'.$theme->theme_name.'.zip';
		move_uploaded_file($_FILES['theme_file']['tmp_name'], $filepath);

		//unzip the file
		$zip = new ZipArchive;
		$open_zip = $zip->open($filepath);
		if ($open_zip === TRUE) {
			$zip->extractTo($extract_path);
			$zip->close();
		} else {
			$this->session->set_flashdata('alert', 'Problem unzipping the file.');
			$this->session->set_flashdata('alert_type','danger');

	 		if(isset($redirect)) {
	 			redirect($redirect);
	 		} else {
	 			redirect('themes/themes_management');
	 		}
		}

		$this->save($this->input->post(), $theme_id);
		$this->session->set_flashdata('alert_type','success');
		$this->session->set_flashdata('alert', 'Theme successfully updated.');

		//save log
		$this->logs_m->save_log('Update Theme', serialize($this->input->post()));

 		if(isset($redirect)) {
 			redirect($redirect);
 		} else {
 			redirect('themes/themes_management');
 		}
 	}

 	function get_tool_categories() {
 		$this->db->select('tool_category');
		$this->db->distinct();
		$result = $this->db->get('srn_tools')->result();
		return $result;
 	}

 	function projectArtifacts($project_id){
 		$this->db->select('*');
		$this->db->from("srn_project_artifacts p");
		$this->db->where('project_id', $project_id);
		$result = $this->db->get()->result();

		
		foreach($result  as $key=>$value){
			$this->db->select('*');
			$this->db->from("srn_artifact_files");
			$this->db->order_by("file_version","desc");
			$this->db->where('artifact_id', $result[$key]->id);
			$res = $this->db->get()->result();
			
			$result[$key]->files = $res;
		}

		return $result;
 	}

 	function get_artifactFile($file_id){
 		$this->db->select('*');
		$this->db->from("srn_artifact_files");
		$this->db->where('id', $file_id);
		$res = $this->db->get()->row();

		return $res;
 	}

 	function createArtifact($query) {
 		$date = date("Y-m-d H:i:s");
 		$query["artifact_datemodified"] = $date;
 		$query["artifact_datecreated"] = $date;
 		$query["artifact_status"] = "Active";
 		$query["artifact_createdby"] = $this->session->userdata('user_fullname');

 		$artifact = array(
 			"file_url"=>$query["url"],
 			"file_datecreated"=>$date,
 			"file_datemodified"=>$date,
 			"file_status"=>"Active",
 			"file_version"=>"1.0.0",
 			"file_security"=>serialize(array()),
 			"file_createdby"=>$this->session->userdata('user_fullname')
 		);

 		//save log
		$this->logs_m->save_log('Create Artifact', serialize($query));

 		unset($query["url"]);

 		$this->db->insert('srn_project_artifacts', $query);

 		$artifact["artifact_id"] = $this->db->insert_id();

 		$this->db->insert('srn_artifact_files', $artifact);

 		
 	}

 	function addArtifactFile($query) {
 		$date = date("Y-m-d H:i:s");
 		$query["file_datecreated"] = $date;
 		$query["file_datemodified"] = $date;
 		$query["file_createdby"] = $this->session->userdata('user_fullname');

 		
 		$this->db->insert('srn_artifact_files', $query);

 		
 	}
}