<?php 

class Db_Write_M extends MY_Model {

	
	function __construct() {
        parent::__construct();

        
    }

 	function create($db_name) {
 		if(!$this->db->table_exists($db_name)){
        	
        	$db_file = FCPATH."application/models/db/".$db_name.".sql";
        	if(file_exists($db_file)){
	        	$sql = file_get_contents($db_file);
				/*
				Assuming you have an SQL file (or string) you want to run as part of the migration, which has a number of statements...
				CI migration only allows you to run one statement at a time. If the SQL is generated, it's annoying to split it up and create separate statements.
				This small script splits the statements allowing you to run them all in one go.
				*/

				$sqls = explode(';', $sql);
				array_pop($sqls);

				foreach($sqls as $statement){
				    $statment = $statement . ";";
				    $this->db->query($statement);   
				}
	        }
        }
 		
 	}   
}