<?php 

class Logs_M extends MY_Model {

	protected $_table_name = 'srn_logs';
	protected $_primary_key = 'log_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'log_id';
	protected $_order = 'DESC';
	protected $_timestamps = TRUE;
	public $rules = array();

	function __construct() {
        parent::__construct();	
    }

    function save_log($type, $meta) {

    	$data = array(
    		'user_id' => $this->session->userdata('user_id'),
    		'log_type' => $type,
    		'log_meta' => $meta
    	);

    	$this->save($data);
    }
}