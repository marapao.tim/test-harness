<?php 

class Release_M extends MY_Model {

	protected $_table_name = 'srn_master_release';
	protected $_primary_key = 'release_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'release_id';
	protected $_order = 'DESC';
	protected $_timestamps = TRUE;
	public $rules = array();

	function __construct() {
        parent::__construct();

        $this->load->helper('array');
    }
}