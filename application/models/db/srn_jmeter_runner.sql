
CREATE TABLE IF NOT EXISTS `srn_jmeter_runner` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `test_script` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `run_count` int(11) NOT NULL,
  `last_run_date` datetime DEFAULT NULL,
  `createdby` varchar(255) NOT NULL,
  `datecreated` datetime NOT NULL,
  `datemodified` datetime NOT NULL,
  `schedule_type` text NOT NULL,
  `schedule_value` int(11) NOT NULL
);

ALTER TABLE `srn_jmeter_runner`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `srn_jmeter_runner`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;