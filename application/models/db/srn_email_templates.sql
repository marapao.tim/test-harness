
CREATE TABLE IF NOT EXISTS `srn_email_templates` (
`template_id` int(11) NOT NULL,
  `template_name` varchar(255) NOT NULL,
  `template_description` text NOT NULL,
  `template_module` varchar(255) NOT NULL,
  `template_tags` text NOT NULL,
  `template_datecreated` datetime NOT NULL,
  `template_datemodified` datetime NOT NULL,
  `createdby` varchar(255) NOT NULL,
  `template_body` longtext NOT NULL
);


ALTER TABLE `srn_email_templates`
 ADD PRIMARY KEY (`template_id`);


ALTER TABLE `srn_email_templates`
MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT;
