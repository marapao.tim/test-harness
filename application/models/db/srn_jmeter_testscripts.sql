
CREATE TABLE IF NOT EXISTS `srn_jmeter_testscripts` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `project` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tags` text NOT NULL,
  `script` longtext NOT NULL,
  `createdby` varchar(255) NOT NULL,
  `datecreated` datetime NOT NULL,
  `datemodified` datetime NOT NULL
);

ALTER TABLE `srn_jmeter_testscripts`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `srn_jmeter_testscripts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;