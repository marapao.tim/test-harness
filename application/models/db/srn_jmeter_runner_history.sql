
CREATE TABLE IF NOT EXISTS `srn_jmeter_runner_history` (
`id` int(11) NOT NULL,
  `runner_id` int(11) NOT NULL,
  `datecreated` datetime NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `srn_jmeter_runner_history`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `srn_jmeter_runner_history`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
