<?php 

class Theme_Meta_M extends MY_Model {

	protected $_table_name = 'srn_thememeta';
	protected $_primary_key = 'thm_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'thm_id';
	protected $_order = 'DESC';
	protected $_timestamps = TRUE;
	public $rules = array();

	function __construct() {
        parent::__construct();
    }
}