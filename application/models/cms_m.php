<?php 

class Cms_M extends MY_Model {

	protected $_table_name = 'srn_cms';
	protected $_primary_key = 'cms_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'cms_id';
	protected $_order = 'DESC';
	protected $_timestamps = TRUE;
	public $rules = array();

	function __construct() {
        parent::__construct();

        $this->load->model('tool_meta_m');
    }

 	function get_new() {
 		$tool = new stdClass();

 		$tool->codename = 'Codename';
 		$tool->version = '';
 		$tool->release_notes = '';

 		return $tool;
 	}   
}