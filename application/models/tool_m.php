<?php 

class Tool_M extends MY_Model {

	protected $_table_name = 'srn_tools';
	protected $_primary_key = 'tool_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'tool_id';
	protected $_order = 'DESC';
	protected $_timestamps = TRUE;
	public $rules = array();

	function __construct() {
        parent::__construct();

        $this->load->model('tool_meta_m');
    }

 	function get_new() {
 		$tool = new stdClass();

 		$tool->tool_name = '';
 		$tool->tool_title = 'New Tool';
 		$tool->tool_description = '';
 		$tool->tool_category = '';

 		return $tool;
 	}   
}