<div class="row">
    <div class="col-md-3 col-md-offset-7">
       
    </div>

    <div class="col-md-2">
        <a class="btn btn-primary btn-default" href="<?php echo base_url('jmeter/test_script/add'); ?>"><i class="ion-plus"></i> New Test Script</a>
    </div>
</div>
<hr>
<div class="row projects-container">
    <div class="col-md-12">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Module</th>
                    <th>Description</th>
                    <th>Version</th>
                    <th>Created By</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
