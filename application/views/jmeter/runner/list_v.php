<style>
     .project-status-circle {
        width: 10px;
        height: 10px;
        position: absolute;
        left: -8px;
        top: 14px;
        border-radius: 100%;
        border: 1px solid;
    }
</style>
<div class="panel panel-default master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            Test Scripts
            <div class="pull-right">
                <!--<a class="btn btn-primary btn-default" href="<?php echo base_url('jmeter/test_script/add'); ?>"><i class="ion-plus"></i> New Scheduled Runner</a>-->
                <a class="btn btn-primary btn-default" href="javascript:;" id="add-runner"><i class="ion-plus"></i> New Scheduled Runner</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <div class="panel-body">
        
        <div class="row projects-container">
            <div class="col-md-12">
                <table id="testrunner-table" class="table datatable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Test Script</th>
                            <th>Schedule</th>
                            <th># of Runs</th>
                            <th>Lastest Run</th>
                            <th>Status</th>
                            <th>Created By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($test_runners as $key=>$value){ ?>
                        <tr>
                            <td><?=$value->name;?></td>
                            <td><?=$value->test_script;?></td>
                            <td>
                                <?=$value->schedule_type;?>
                                <?php
                                    $s_type = "";
                                    switch($value->schedule_type){
                                        case "Daily":
                                            $s_type = "@ ".$value->schedule_value . ":00";
                                        break;
                                        case "Weekly":
                                            $s_type = " every ";
                                            switch($value->schedule_value){
                                                case 0:
                                                    $s_type .= "Sunday";
                                                break;
                                                case 1:
                                                    $s_type .= "Monday";
                                                break;
                                                case 2:
                                                    $s_type .= "Tuesday";
                                                break;
                                                case 3:
                                                    $s_type .= "Wednesday";
                                                break;
                                                case 4:
                                                    $s_type .= "Thursday";
                                                break;
                                                case 5:
                                                    $s_type .= "Friday";
                                                break;
                                                case 6:
                                                    $s_type .= "Saturday";
                                                break;
                                            }
                                            
                                        break;
                                        case "Monthly":
                                            $s_type = "@ day ".($value->schedule_value);
                                        break;
                                    }
                                    echo $s_type;
                                ?>
                            </td>
                            <td><?=$value->run_count;?></td>
                            <td><?=$value->last_run_date;?></td>
                            <td><?=$value->status;?></td>
                            <td><?=$value->createdby;?></td>
                            <td>
                                <!--<a href="<?php echo base_url('jmeter/test_script/edit/'.$value->id); ?>" rel="tooltip" title="Update" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>-->
                                <a href="javascript:;" data-url="<?= base_url('jmeter/view_runner_results/'.$value->id); ?>" rel="tooltip" title="View Results" class="view-runner-results btn btn-default btn-xs" data-id="<?=$value->id;?>" data-name="<?=$value->name;?>"><i class="fa fa-eye"></i></a>
                                <a href="javascript:;" data-url="<?= base_url('jmeter/view_runner_history/'.$value->id); ?>" rel="tooltip" title="View History" class="view-runner-history btn btn-default btn-xs" data-id="<?=$value->id;?>" data-name="<?=$value->name;?>"><i class="fa fa-list-alt"></i></a>
                                <a href="javascript:;" data-url="<?= base_url('jmeter/runner/delete/'.$value->id); ?>" rel="tooltip" title="Delete" data-runner-id="<?=$value->id;?>" class="btn btn-default btn-xs delete-runner-btn" data-action-modal-message="Are you sure?"><i class="fa fa-remove"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>



<script id="modal-add-runner" type="text/x-handlebars-template">
    <div id="fetch-modal-alerts" class="alert alert-danger hidden">
      Indicates a neutral informative change or action.
    </div>
    <form id="form-add-runner" class="form-horizontal" method="POST" data-target="<?= base_url('jmeter/runner/add'); ?>">
        
        <div class="form-group">
            <label for="" class="col-md-3 control-label">Name</label>
            <div class="col-md-9">
                <input type="text" class="form-control" name="name" id="" placeholder="Name" value="">
            </div>
        </div>

        <div class="form-group">
            <label for="" class="col-md-3 control-label">Test Script</label>
            <div class="col-md-9">
                <select class="form-control" name="test_script" >
                    <?php foreach($test_scripts as $key=>$value){ ?>
                    <option value="<?=$value->id;?>"><?=$value->name;?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="" class="col-md-3 control-label">Schedule</label>
            <div class="col-md-9">
                <select class="form-control" name="schedule[recur]" >
                    <option>Daily</option>
                    <option>Weekly</option>
                    <option>Monthly</option>
                </select>
                <br/>

                <div id="tab-Daily" class="tabs-sched">
                    <div class="form-group">
                        <label for="" class="col-md-2 control-label">Hour</label>
                        <div class="col-md-10">
                            <select class="form-control" name="schedule[daily]" >
                                <option value="0">12mn</option>
                                <option value="1">1am</option>
                                <option value="2">2am</option>
                                <option value="3">3am</option>
                                <option value="4">4am</option>
                                <option value="5">5am</option>
                                <option value="6">6am</option>
                                <option value="7">7am</option>
                                <option value="8">8am</option>
                                <option value="9">9am</option>
                                <option value="10">10am</option>
                                <option value="11">11am</option>
                                <option value="12">12nn</option>
                                <option value="13">1pm</option>
                                <option value="14">2pm</option>
                                <option value="15">3pm</option>
                                <option value="16">4pm</option>
                                <option value="17">5pm</option>
                                <option value="18">6pm</option>
                                <option value="19">7pm</option>
                                <option value="20">8pm</option>
                                <option value="21">9pm</option>
                                <option value="22">10pm</option>
                                <option value="23">11pm</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div id="tab-Weekly" class="tabs-sched hidden">
                    <div class="form-group">
                        <label for="" class="col-md-2 control-label">Day</label>
                        <div class="col-md-10">
                            <select class="form-control" name="schedule[weekly]" >
                                <option value="0">Sunday</option>
                                <option value="1">Monday</option>
                                <option value="2">Tuesday</option>
                                <option value="3">Wednesday</option>
                                <option value="4">Thursday</option>
                                <option value="5">Friday</option>
                                <option value="6">Saturday</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div id="tab-Monthly" class="tabs-sched hidden">
                    <div class="form-group">
                        <label for="" class="col-md-2 control-label">Day</label>
                        <div class="col-md-10">
                            <select class="form-control" name="schedule[monthly]" >
                                <?php for($x=1;$x<=31;$x++){ ?>
                                <option value="<?=$x;?>"><?=$x;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="" class="col-md-3 control-label">Status</label>
            <div class="col-md-9">
                <select class="form-control" name="status" >
                    <option>Active</option>
                    <option>Inactive</option>
                </select>
            </div>
        </div>

    </form>
</script>