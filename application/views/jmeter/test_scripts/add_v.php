<div class="panel panel-default master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            Add a Test Script
            <div class="pull-right">
                <button class="btn btn-box-tool btn-xs btn-link" rel="tooltip" title="Collapse" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <input type="hidden" value="<?= ($this->uri->segment(3)) ? 'edit' : 'add'; ?>" data-tool-id="<?= $this->uri->segment(3); ?>" id="form-mode">

    <!-- form start -->
    <?php echo form_open_multipart('test_scripts/add', array('role' => 'form', 'class' => 'form-horizontal', 'id' => 'add-template-form')); 
    ?>

        <div class="panel-body">
            <div class="form-group">
                <label for="tool_title" class="control-label label-left col-md-2">Name</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="name" id="template_name" placeholder="" value="">
                </div>
            </div>      

            <div class="form-group hidden">
                <label for="tool_category" class="control-label label-left col-md-2">Project</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="project" id="template_module" placeholder="" value="" list="tool-categories">
                    <datalist id="tool-categories">
                        <?php 
                            foreach ($projects as $key => $value) {
                                ?>
                                    <option value="<?= $value->project; ?>"><?= $value->project; ?></option>
                                <?php
                            }
                        ?> 
                    </datalist>
                </div>
            </div>

            <div class="form-group">
                <label for="tool_description" class="control-label label-left col-md-2">Description</label>
                <div class="col-md-8">
                    <textarea rows="5" type="text" class="form-control" name="description" id="template_description" placeholder="" rows="3"></textarea>
                </div>
            </div>       

            <div class="form-group">
                <label for="tool_tags" class="control-label label-left col-md-2">Tags</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="tags" id="template_tags" placeholder="" value="">
                </div>
            </div>     


            <div class="form-group">
                <label for="tool_description" class="control-label label-left col-md-2">
                    Script 
                </label>
                <div class="col-md-8">
                    <textarea rows="25" type="text" class="form-control" name="script" id="template_body" placeholder="" ></textarea>
                </div>
            </div>    

        </div>

        <div class="panel-footer">
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <a href="<?= base_url('jmeter/test_script'); ?>" class="btn btn-default btn-sm">Back</a>
                    
                    <button class="btn btn-primary btn-sm" id="save-tool-btn">Save</button>
                </div>
            </div>
        </div>

    <?php echo form_close(); ?>

</div>