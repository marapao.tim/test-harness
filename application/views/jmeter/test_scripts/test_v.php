<div class="panel panel-default master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            Test API Script: <?=$test_script->name;?>
            
            <div class="pull-right">
                <button class="btn btn-primary btn-sm" id="run-script-btn" data-id="<?=$test_script->id;?>">Run</button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <input type="hidden" value="<?= ($this->uri->segment(3)) ? 'edit' : 'add'; ?>" data-tool-id="<?= $this->uri->segment(3); ?>" id="form-mode">

    <!-- form start -->
    <?php echo form_open_multipart('jmeter/test_script/edit/'.$this->uri->segment(4), array('role' => 'form', 'class' => 'form-horizontal', 'id' => 'edit-template-form')); 
    ?>

    <div class="row">
        <div class="col-md-12 script-text-test"> 
                <textarea rows="25" type="text" class="form-control" name="script" id="template_body" placeholder="" ><?=$test_script->script;?></textarea>
        </div> 
    </div> 
    <div class="output ui-widget-content">
        <div class="output-header">
            <span>Output:</span> 
            <div class="pull-right">
                <a data-toggle="collapse" href="#result_panel" title="minimize" aria-expanded="true" class=""><i style="color:#fff" class="fa fa-minus-square-o"></i></a> 
                <a data-toggle="collapse" href="#result_panel" title="maximize" aria-expanded="true" class=""><i style="color:#fff" class="fa fa-square-o"></i></a>
            </div>
        </div>

            <ul class="list-inline text-center list-format">
                <li><a href="#"><i class="fa fa-spinner fa-spin"></i> Origin</a></li>
                <li><a href="#"><i class="fa fa-spinner fa-spin"></i> Headers</a></li> 
                <li><a href="#" data-toggle="modal" data-target="#modal-responsed"><i class="fa fa-spinner fa-spin"></i> Response</a></li> 
            </ul>
        <textarea rows="25" type="text" class="form-control" name="script" id="test_result" placeholder="" ></textarea>
    </div>

        <!-- <div class="panel-body">
            <div class="col-md-12">
                <div class="panel panel-default master-cms-panel">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Script
                            <div class="pull-right">
                                <a data-toggle="collapse" href="#template_panel"><i class="fa fa-minus"></i></a>
                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div><!-- /.box-header
                    <div id="template_panel" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <textarea rows="25" type="text" class="form-control" name="script" id="template_body" placeholder="" ><?=$test_script->script;?></textarea>
                        </div>
                    </div>
                </div>
                
            </div>  

            <div class="col-md-12">
                <div class="panel panel-default master-cms-panel">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Result
                            <div class="pull-right">
                                <a data-toggle="collapse" href="#result_panel"><i class="fa fa-minus"></i></a>
                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div><!-- /.box-header -->
              <!--       <div id="result_panel" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="col-md-6">
                                <textarea rows="25" type="text" class="form-control" name="script" id="test_result" placeholder="" ></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  

        </div> -->

        <!--<div class="panel-footer">
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <a href="<?= base_url('jmeter/test_script'); ?>" class="btn btn-default btn-sm">Back</a>
                    
                    <button class="btn btn-primary btn-sm" id="save-tool-btn">Save</button>
                </div>
            </div>
        </div>-->

    <?php echo form_close(); ?>

</div> 

<div id="modal-responsed" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content"> 
        <ul class="nav nav-tabs reponse-tabs">
<!--           <li><a href="#">Home</a></li>
          <li><a href="#">Menu 1</a></li>
          <li><a href="#">Menu 2</a></li>
          <li><a href="#">Menu 3</a></li> -->
        </ul>
      <div class="modal-body">
         <iframe id="iframe" style="width:100%;height:500px;"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
 