<div class="panel panel-default master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            Test Scripts
            <div class="pull-right">
                <a class="btn btn-primary btn-default" href="<?php echo base_url('test_scripts/add'); ?>"><i class="ion-plus"></i> New Test Script</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <div class="panel-body">
        
        <div class="row projects-container">
            <div class="col-md-12">
                <table id="testscript-table" class="table datatable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Tags</th>
                            <th>Created By</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($test_scripts as $key=>$value){ ?>
                        <tr>
                            <td><?=$value->name;?></td>
                            <td><?=$value->description;?></td>
                            <td><?=$value->tags;?></td>
                            <td><?=$value->createdby;?></td>
                            <td class="text-center">
                                <a href="<?php echo base_url('test_scripts/test/'.$value->id); ?>" rel="tooltip" title="Test" class="btn btn-default btn-xs"><i class="fa fa-line-chart"></i></a>
                                <a href="<?php echo base_url('test_scripts/edit/'.$value->id); ?>" rel="tooltip" title="Update" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>
                                <a href="javascript:;" rel="tooltip" title="Delete" data-testscript-id="<?=$value->id;?>" class="btn btn-default btn-xs delete-testscript-btn" data-action-modal-message="Are you sure?"><i class="fa fa-remove"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

