<p class="text-center" style="margin-top: 25%;">
    <img src="<?= base_url('assets/img/srn_logo.png'); ?>" alt="Serino">
    <br>
    <?php $this->user_m->display_alert(); ?>
</p>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title">Welcome, please login.</div>
    </div>
    <div class="panel-body">
        <form role="form" method="POST">
            <fieldset>
                <div class="form-group">
                    <input class="form-control" placeholder="Username" id="user_name" name="user_name" type="text" autofocus>
                </div>
                <div class="form-group">
                    <input class="form-control" placeholder="Password" id="user_password" name="user_password" type="password" value="">
                </div>
                <div class="checkbox hidden">
                    <label>
                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                    </label>
                </div>

                <input type="submit" value="Login" class="btn btn-success btn-lg btn-default btn-block">

            </fieldset>
        </form>
    </div>
</div>

<p class="text-muted text-center">
    &copy; Copyright <?= date('Y'); ?> Test Harness
</p>