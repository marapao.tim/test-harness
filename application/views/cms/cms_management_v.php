<div class="panel panel-default master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            CMS Table
            <div class="pull-right">
                <a class="btn btn-sm btn-primary btn-default" href="<?php echo base_url('cms/save_cms'); ?>"><i class="ion-plus"></i> Add New Version</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->
    <div class="panel-body">
        <table class="table table-hover datatable" id="cms-table">
            <thead>
                <tr>
                    <th>Version</th>
                    <th>Codename</th>
                    <th>Date Released</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->

<script id="cms-row-template" type="x-tmpl-mustache">
    <tr>
        <td>{{version}}</td>
        <td>{{codename}}</td>
        <td>{{date_created}}</td>
        <td class="text-center">
            <a href="<?php echo base_url('cms/view_cms/{{cms_id}}'); ?>" rel="tooltip" title="View" class="btn btn-default btn-xs"><i class="fa fa-search"></i></a>
            <a href="<?php echo base_url('cms/delete_cms/{{cms_id}}'); ?>" rel="tooltip" title="Delete" data-cms-id="{{cms_id}}" class="btn btn-default btn-xs delete-cms-btn"><i class="fa fa-remove"></i></a>
        </td>
    </tr>
</script>