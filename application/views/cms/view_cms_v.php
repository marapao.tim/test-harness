<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title">
            
            <div class="pull-right">
                <button class="btn btn-box-tool btn-xs btn-link" rel="tooltip" title="Collapse" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <div class="panel-body">
        <table class="table table-hover">
            <tr>
                <th style="width: 20%;">Codename</th>
                <td><?= $cms->codename; ?></td>
            </tr>
            <tr>
                <th>Version</th>
                <td><?= $cms->version; ?></td>
            </tr>
            <tr>
                <th>Released Date</th>
                <td>
                    <abbr class="timeago" rel="tooltip" title="<?= date($this->master_config['date_time_format'], strtotime($cms->date_created)); ?>"></abbr>
                </td>
            </tr>
            <tr>
                <th>No. of Downloads</th>
                <td>
                    0
                </td>
            </tr>
            <tr>
                <th colspan="2">Release Notes</th>
            </tr>
            <tr>
                <td colspan="2">
                    <?= htmlspecialchars_decode($cms->release_notes); ?>
                </td>
            </tr>
        </table>
    </div>
</div>