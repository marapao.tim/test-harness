<div class="panel panel-default master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            <?= ($this->uri->segment(3)) ? 'Edit ' : 'Add '; ?>CMS Form
            <div class="pull-right">
                <button class="btn btn-box-tool btn-xs btn-link" rel="tooltip" title="Collapse" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <input type="hidden" value="<?= ($this->uri->segment(3)) ? 'edit' : 'add'; ?>" data-tool-id="<?= $this->uri->segment(3); ?>" id="form-mode">

    <!-- form start -->
    <?php echo form_open_multipart('v1/cms/', array('role' => 'form', 'class' => 'form-horizontal', 'id' => 'save-cms-form', 'data-parsley-validate' => true)); 
    ?>

        <div class="panel-body">
            <div class="form-group">
                <label for="codename" class="control-label label-left col-md-2">Codename</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="codename" id="codename" placeholder="" value="" data-parsley-required>
                </div>
            </div>              

            <div class="form-group">
                <label for="cms_file" class="control-label col-md-2 label-left">ZIP Package</label>
                <div class="col-md-4" id="cms_file_form_group">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-default btn-sm btn-file">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" class="" name="cms_file" accept=".zip">
                        </span>
                        <span class="fileinput-filename"></span>
                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none; font-size: 13px;"><i class="fa fa-remove"></i></a>
                    </div>                   
                </div>
            </div>   

            <div class="form-group">
                <label for="version" class="control-label label-left col-md-2">Version</label>
                <div class="col-md-2">
                    <input type="number" class="form-control" name="version" id="version" placeholder="" value="" step="0.01">
                </div>
            </div>                       

            <div class="form-group">
                <label for="release_notes" class="control-label label-left col-md-2">Release Notes</label>
                <div class="col-md-10">
                    <textarea type="text" class="form-control" name="release_notes" id="release_notes" placeholder="" rows="10"></textarea>
                </div>
            </div>  
        </div>

        <div class="panel-footer">
            <div class="form-group">
                <div class="col-md-4 col-md-offset-2">
                    <a href="<?= base_url('cms/cms_management'); ?>" class="btn btn-default btn-sm">Back</a>
                    <button class="btn btn-primary btn-sm" id="save-cms-btn">Save</button>
                </div>
            </div>
        </div>

    <?php echo form_close(); ?>

</div>