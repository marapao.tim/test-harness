<div class="panel panel-default master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            <?= ($this->uri->segment(3)) ? 'Edit ' : 'Add '; ?>Permission Form
            <div class="pull-right">
                <button class="btn btn-box-permission btn-xs btn-link" rel="tooltip" title="Collapse" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <input type="hidden" value="<?= ($this->uri->segment(3)) ? 'edit' : 'add'; ?>" data-permission-id="<?= $this->uri->segment(3); ?>" id="form-mode">

    <?php echo form_open_multipart('permissions/save_permission/'.$this->uri->segment(3), array('role' => 'form', 'class' => 'form-horizontal', 'id' => 'save-permission-form')); 
    ?>
        <div class="panel-body">
            <div class="form-group">
                <label for="permission_title" class="control-label label-left col-md-2">Title</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="permission_title" id="permission_title" placeholder="" value="">
                </div>
            </div>  

            <div class="form-group">
                <label for="permission_description" class="control-label label-left col-md-2">Description</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="permission_description" id="permission_description" placeholder="" value="">
                </div>
            </div>  

            <div class="form-group">
                <label for="permission_group" class="control-label label-left col-md-2">Group</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="permission_group" id="permission_group" placeholder="" value="" list="permission-group">
                    <datalist id="permission-group">
                        <?php 
                            foreach ($groups as $key => $value) {
                                ?>
                                    <option value="<?= $value->permission_group; ?>"><?= $value->permission_group; ?></option>
                                <?php
                            }
                        ?> 
                    </datalist>
                </div>
            </div>    

            <div class="form-group">
                <label for="permission_remarks" class="control-label label-left col-md-2">Remarks</label>
                <div class="col-md-10">
                    <textarea type="text" class="form-control" name="permission_remarks" id="permission_remarks" placeholder="" rows="3"></textarea>
                </div>
            </div>   
        </div>

        <div class="panel-footer">
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <a href="<?= base_url('permissions/permissions_management'); ?>" class="btn btn-default btn-sm">Back</a>
                    <button class="btn btn-primary btn-sm" id="save-permission-btn">Save</button>
                </div>
            </div>
        </div>
     <?php echo form_close(); ?>

</div>