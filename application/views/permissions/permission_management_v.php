<div class="panel master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            Permissions Table
            <div class="pull-right">
                <a class="btn btn-sm btn-primary btn-default" href="<?php echo base_url('permissions/save_permission'); ?>"><i class="ion-plus"></i> Add Permission</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->
    <div class="panel-body">
        <table class="table table-hover datatable" id="permissions-table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Group</th>
                    <th>Date Created</th>
                    <th>Created By</th>
                    <th>Date Modified</th>
                    <th>Modified By</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->

<script id="permission-row-template" type="x-tmpl-mustache">
    <tr>
        <td>{{permission_title}}</td>
        <td>{{permission_group}}</td>
        <td>{{date_created}}</td>
        <td>{{created_by}}</td>
        <td>{{date_modified}}</td>
        <td>{{modified_by}}</td>
        <td class="text-center">
            <a href="<?php echo base_url('permissions/save_permission/{{permission_id}}'); ?>" rel="tooltip" title="Edit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></a>
            <a href="<?php echo base_url('permissions/delete_permission/{{permission_id}}'); ?>" rel="tooltip" title="Delete" data-permission-id="{{permission_id}}" class="btn btn-default btn-xs delete-permission-btn" data-action-modal-message="Are you sure?"><i class="fa fa-remove"></i></a>
        </td>
    </tr>
</script>