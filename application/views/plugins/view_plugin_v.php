<p>
    <a href="<?= base_url('plugin/save_plugin/'.$plugin->plugin_id.'/?redirect=').base_url('plugin/view_plugin/'.$this->uri->segment(3)); ?>" class="btn btn-warning btn-sm">Edit</a>
    <a href="<?= base_url('plugin/update_plugin/'.$plugin->plugin_id.'/?redirect=').base_url('plugin/view_plugin/'.$this->uri->segment(3)); ?>" class="btn btn-success btn-sm">Update</a>
    <a href="#" class="btn btn-danger btn-sm delete-plugin-btn" data-plugin-id="<?= $this->uri->segment(3); ?>">Delete</a>
</p>

<div class="panel panel-default master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            Details
            <div class="pull-right">
                <button class="btn btn-box-tool btn-xs btn-link" rel="tooltip" title="Collapse" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <div class="panel-body">
        
        <table class="table table-hover table bordered">
            <tr>
                <th style="width: 15%;">ID</th>
                <td class="data-plugin-id">
                    
                </td>
            </tr>
            <tr>
                <th>CMS ID</th>
                <td class="data-plugin-cms-id"></td>
            </tr>            
            <tr>
                <th>Title</th>
                <td class="data-plugin-title"></td>
            </tr>                     
            <tr>
                <th>Author</th>
                <td class="data-plugin-author"></td>
            </tr>            
            <tr>
                <th>Current Version</th>
                <td class="data-plugin-current-version"></td>
            </tr>            
            <tr>
                <th>Category</th>
                <td class="data-plugin-category"></td>
            </tr>            
            <tr>
                <th>Tags</th>
                <td class="data-plugin-tags"></td>
            </tr>            
            <tr>
                <th>Description</th>
                <td class="data-plugin-description"></td>
            </tr>
            <tr>
                <th>Creation Details</th>
                <td>
                    Created by <strong>User ID <span class="data-plugin-created-by"></span></strong> &middot; <span class="text-muted"><span class="data-plugin-date-created"></span></span>
                </td>
            </tr>
            <tr class="modification-details-container hidden">
                <th>Modification Details</th>
                <td>
                    Last Modified by <strong>User ID <span class="data-plugin-modified-by"></span></strong> &middot; <span class="text-muted"><span class="data-plugin-date-modified"></span></span>
                </td>
            </tr>
        </table>

    </div><!-- /.box-body -->
</div><!-- /.box -->

<div class="panel panel-default plugin-versions-panel">
    <div class="panel-heading">
        <div class="panel-title">
            Versions Timeline
            <div class="pull-right">
                <button class="btn btn-box-tool btn-xs btn-link" rel="tooltip" title="Collapse" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <div class="panel-body">
        
        <ul class="milestones plugin-version-timeline">

        </ul>        
    </div><!-- /.box-body -->
</div><!-- /.box -->

<script id="plugin-version-template" type="x-tmpl-mustache">
    <li>
        <i class="{{icon_class}} fa fa-file-zip-o"></i>
        <h6><strong>Version {{version}}</strong></h6> <br>
        {{{remarks}}}
        <span class="text-muted">
            <i class="fa fa-user"></i> &nbsp; {{updated_by}} &middot; &nbsp;
            <i class="fa fa-clock-o"></i> &nbsp; {{{release_date}}} &middot; &nbsp;
            <i class="ion-soup-can-outline"></i> &nbsp; {{{cms_support}}}
        </span>
        <br><br>
        <strong>Screenshot: </strong> <br><br>
        <a href="{{path_to_screenshot}}" data-toggle="lightcase" title="screenshot" style="cursor: -webkit-zoom-in; cursor: -moz-zoom-in;">
            <img src="{{path_to_screenshot}}" class="img-responsive img-thumbnail" style="width: 250px;" alt="{{plugin_cms_id}}">
        </a>
        <br>
        <br>
        <a href="{{path_to_file}}" class="btn btn-sm btn-primary" download="{{plugin_cms_id}}v{{version}}.zip"><i class="fa fa-download"></i> Download</a>
    </li>
</script>