<div class="panel master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            Plugins Table
            <div class="pull-right">
                <a class="btn btn-sm btn-primary btn-default" href="<?php echo base_url('plugin/save_plugin'); ?>"><i class="ion-plus"></i> Add Plugin</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->
    <div class="panel-body">
        <table class="table table-hover datatable" id="plugins-table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Current Version</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->

<script id="plugin-row-template" type="x-tmpl-mustache">
    <tr>
        <td>{{title}}</td>
        <td>{{category}}</td>
        <td>{{version}}</td>
        <td class="text-center">
            <a href="<?php echo base_url('plugin/view_plugin/{{plugin_id}}'); ?>" rel="tooltip" title="View" class="btn btn-default btn-xs"><i class="fa fa-search"></i></a>
            <a href="<?php echo base_url('plugin/save_plugin/{{plugin_id}}'); ?>" rel="tooltip" title="Edit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></a>
            <a href="<?php echo base_url('plugin/update_plugin/{{plugin_id}}'); ?>" rel="tooltip" title="Update" class="btn btn-default btn-xs"><i class="fa fa-upload"></i></a>
            <a href="<?php echo base_url('plugin/delete_plugin/{{plugin_id}}'); ?>" rel="tooltip" title="Delete" data-plugin-id="{{plugin_id}}" class="btn btn-default btn-xs delete-plugin-btn" data-action-modal-message="Are you sure?"><i class="fa fa-remove"></i></a>
        </td>
    </tr>
</script>