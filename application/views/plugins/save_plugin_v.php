<div class="panel panel-default master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            <?= ($this->uri->segment(3)) ? 'Edit ' : 'Add '; ?>Plugin Form
            <div class="pull-right">
                <button class="btn btn-box-tool btn-xs btn-link" rel="tooltip" title="Collapse" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <input type="hidden" value="<?= ($this->uri->segment(3)) ? 'edit' : 'add'; ?>" data-plugin-id="<?= $this->uri->segment(3); ?>" id="form-mode">

    <!-- form start -->
    <?php echo form_open_multipart('plugin/save_plugin/'.$this->uri->segment(3), array('role' => 'form', 'class' => 'form-horizontal', 'id' => 'save-plugin-form')); 
    ?>

        <div class="panel-body">
            <div class="form-group">
                <label for="title" class="control-label label-left col-md-2">Title</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="title" id="title" placeholder="" value="">
                </div>
            </div>                                 

            <div class="form-group">
                <label for="author" class="control-label label-left col-md-2">Author</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="author" id="author" placeholder="" value="">
                </div>
            </div>   

            <div class="form-group">
                <label for="category" class="control-label label-left col-md-2">Category</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="category" id="category" placeholder="" value="" list="plugin-categories">
                    <datalist id="plugin-categories">
                        <?php 
                            foreach ($categories as $key => $value) {
                                ?>
                                    <option value="<?= $value->category; ?>"><?= $value->category; ?></option>
                                <?php
                            }
                        ?> 
                    </datalist>
                </div>
            </div>              

            <div class="form-group">
                <label for="tags" class="control-label label-left col-md-2">Tags</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="tags" id="tags" placeholder="" value="">
                </div>
            </div>                      

            <div class="form-group">
                <label for="description" class="control-label label-left col-md-2">Description</label>
                <div class="col-md-10">
                    <textarea type="text" class="form-control" name="description" id="description" placeholder="" rows="3"></textarea>
                </div>
            </div>        

            <?php 
                if(!$this->uri->segment(3)) {
                   ?>
                        <hr>
                        <h6>Version Details</h6>

                        <div class="form-group">
                            <label for="plugin_file" class="control-label col-md-2 label-left">ZIP Package</label>
                            <div class="col-md-4" id="theme_file_form_group">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <span class="btn btn-default btn-sm btn-file">
                                        <span class="fileinput-new">Select file</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" class="" data-parsley-errors-container="#theme_file_form_group" data-parsley-class-handler="#theme_file_form_group" data-parsley-required name="plugin_file" accept=".zip">
                                    </span>
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none; font-size: 13px;"><i class="fa fa-remove"></i></a>
                                </div>                   
                            </div>
                        </div>   

                        <div class="form-group">
                            <label for="plugin_version" class="control-label label-left col-md-2">Version</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="plugin_version" id="plugin_version" placeholder="" value="1" readonly="">
                            </div>
                        </div>   

                        <div class="form-group">
                            <label for="cms_support" class="control-label label-left col-md-2">CMS Support</label>
                            <div class="col-md-4">
                                <select name="cms_support[]" id="cms_support" class="form-control" multiple>
                                    <?php 
                                        foreach ($cms as $key => $value) {
                                            ?>
                                                <option value="<?= $value->version; ?>"><?= $value->codename.' '.$value->version; ?></option>
                                            <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>   

                        <div class="form-group">
                            <label for="version_remarks" class="control-label label-left col-md-2">Version Remarks</label>
                            <div class="col-md-10">
                                <textarea type="text" class="form-control" name="version_remarks" id="version_remarks" placeholder="" rows="3"></textarea>
                            </div>
                        </div>        
                   <?php 
                }
            ?>
        </div>

        <div class="panel-footer">
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <?php 
                        if(isset($_GET['redirect'])) {
                            ?>
                                <a href="<?= $_GET['redirect']; ?>" class="btn btn-default btn-sm">Back</a>
                            <?php
                        } else {
                            ?>
                                <a href="<?= base_url('plugin/plugin_management/'); ?>" class="btn btn-default btn-sm">Back</a>
                            <?php
                        }
                    ?>
                    <button class="btn btn-primary btn-sm" id="save-plugin-btn">Save</button>
                </div>
            </div>
        </div>

    <?php echo form_close(); ?>

</div>