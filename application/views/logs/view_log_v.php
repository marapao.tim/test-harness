
<div class="col-xs-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
                View Log
                <div class="pull-right">
                    <button class="btn btn-box-tool btn-xs btn-link" rel="tooltip" title="Collapse" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div><!-- /.box-header -->
        <div class="panel-body">
            <table class="table table-hover table-bordered">
                <tr>
                    <th style="width: 20%;">Log ID</th>
                    <td><?= $log->log_id; ?></td>
                </tr>                
                <tr>
                    <th>User ID</th>
                    <td><?= $this->user_m->get($log->user_id)->user_fullname; ?></td>
                </tr>                
                <tr>
                    <th>Log Type</th>
                    <td><?= $log->log_type; ?></td>
                </tr>
                <tr>
                    <th>Log Meta</th>
                    <td>
                        <?php 
                            $meta = unserialize($log->log_meta);
                            print_r($meta);
                        ?>
                    </td>
                </tr>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col -->
