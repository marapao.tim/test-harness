<div class="panel">
    <div class="panel-heading">
        <div class="panel-title">
            Logs Table
        </div>
    </div><!-- /.box-header -->
    <div class="panel-body">
        <table class="table table-hover datatable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Date</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    foreach ($logs as $log) {
                        ?>
                            <tr>
                                <td><?php echo $log->log_id; ?></td>
                                <td><?php echo $log->log_type; ?></td>
                                <td><?php echo $this->user_m->get($log->user_id)->user_fullname; ?></td>
                                <td><?php echo date('F d, Y h:i:s A', strtotime($log->date_created)); ?></td>
                                <td class="text-center">
                                    <a href="<?php echo base_url('logs/view_log/'.$log->log_id); ?>" rel="tooltip" title="View" class="btn btn-default btn-xs"><i class="fa fa-search"></i></a>
                                </td>
                            </tr>
                        <?php
                    }
                ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->