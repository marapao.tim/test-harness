
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <script type="text/javascript">
        var base_url = '<?= base_url(); ?>';
    </script>

    <!-- jQuery -->
    <script src="<?= base_url('assets/js/jquery.2.1.1.min.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?= base_url('assets/plugins/metisMenu/metisMenu.min.js'); ?>"></script>       

    <!-- Pace Plugin JavaScript -->
    <script src="<?= base_url('assets/plugins/pace/pace.js'); ?>"></script>    

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?= base_url('assets/plugins/bootbox/bootbox.min.js'); ?>"></script>    

    <!-- Time ago Plugin JavaScript -->
    <script src="<?= base_url('assets/plugins/timeago/timeago.js'); ?>"></script>

    <!-- Dotdotdot -->
    <script src="<?= base_url('assets/plugins/dotdotdot/jquery.dotdotdot.min.js'); ?>"></script>    

    <!-- Scroll Reveal -->
    <script src="<?= base_url('assets/plugins/scrollreveal/scrollreveal.min.js'); ?>"></script>    

    <!-- Custom Scripts -->
    <?php 
        if(isset($scripts) && !empty($scripts)) {
            foreach ($scripts as $script) {
                ?>
                    <script src="<?php echo base_url('assets/'.$script.'.js'); ?>"></script>
                <?php
            }
        }
    ?>
    
    <!-- Serino Main JS -->
    <script src="<?php echo base_url('assets/custom/main.js'); ?>"></script> 

    <!-- Store JS --> 
    <script src="<?php echo base_url('assets/custom/store.js'); ?>"></script>  
</body>

</html>
