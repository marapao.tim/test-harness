<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?= $tool->tool_name; ?></title>

	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css'); ?>">

	<?php 
		if(file_exists(APPPATH.'../tools/'.$tool->tool_name.'/css/index.php')) {
			include(APPPATH.'../tools/'.$tool->tool_name.'/css/index.php');
		} else {
			$css = array();
		}		

		if(file_exists(APPPATH.'../tools/'.$tool->tool_name.'/js/index.php')) {
			include(APPPATH.'../tools/'.$tool->tool_name.'/js/index.php');
		} else {
			$js = array();
		}

		/***** LOAD TOOL CSS ******/
		if(count($css)) {
			foreach ($css as $key => $value) {
				?>
					<link rel="stylesheet" href="<?= base_url('tools/'.$tool->tool_name.'/css/'.$value.'.css'); ?>">
				<?php
			}
		}
	?>
</head>
<body>
	<?php 

		/***** For Master CMS Preview *****/
		if(!isset($_GET['preview'])) {
			require_once("../srn.config.php");
			require_once($config["root_url"]."lib/system/index.php");
		}
	
		include(APPPATH.'../tools/'.$tool->tool_name.'/index.php');
	?>
</body>

	<?php 

		if(count($js)) {
			/***** LOAD TOOL CSS ******/
			foreach ($js as $key => $value) {
				?>
					<script src="<?= base_url('tools/'.$tool->tool_name.'/js/'.$value.'.js'); ?>"></script>
				<?php
			}
		}
	?>

</html>