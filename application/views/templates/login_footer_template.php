                
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="<?= base_url('assets/js/jquery.2.1.1.min.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?= base_url('assets/plugins/metisMenu/metisMenu.min.js'); ?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?= base_url('assets/js/sb-admin-2.js'); ?>"></script>

    <!-- Custom Scripts -->
    <?php 
        if(isset($scripts) && !empty($scripts)) {
            foreach ($scripts as $script) {
                ?>
                    <script src="<?php echo base_url('assets/'.$script.'.js'); ?>"></script>
                <?php
            }
        }
    ?>
    
    <!-- Serino Main JS -->
    <script src="<?php echo base_url('assets/custom/main.js'); ?>"></script>
</body>

</html>
