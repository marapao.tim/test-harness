        

    </div>
    <!-- /#wrapper -->

    <script type="text/javascript">
        var base_url = '<?= base_url(); ?>';
    </script>

    <!-- jQuery -->
    <script src="<?= base_url('assets/js/jquery.2.1.1.min.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?= base_url('assets/plugins/metisMenu/metisMenu.min.js'); ?>"></script>       

    <!-- Pace Plugin JavaScript -->
    <script src="<?= base_url('assets/plugins/pace/pace.js'); ?>"></script>   

    <!-- Moment --> 
    <script src="<?= base_url('assets/plugins/moment/moment.js'); ?>"></script>    

    <!-- Bootbox JavaScript -->
    <script src="<?= base_url('assets/plugins/bootbox/bootbox.min.js'); ?>"></script> 

    <!-- Notify Master -->  
    <script src="<?= base_url('assets/plugins/notify-master/js/notify.js'); ?>"></script>    
    <script src="<?= base_url('assets/plugins/notify-master/js/prettify.js'); ?>"></script>    

    <!-- Time ago Plugin JavaScript -->
    <script src="<?= base_url('assets/plugins/timeago/timeago.js'); ?>"></script>

    <!-- Mustache Javascript Templating -->
    <script src="<?= base_url('assets/plugins/mustache/mustache.min.js'); ?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?= base_url('assets/js/sb-admin-2.js'); ?>"></script>    
  
    <script type="text/javascript">
    $(document).ready(function() {
        $("body").tooltip({ selector: '[data-toggle=tooltip]' });
    });
    </script>    
    
    <!-- Custom Scripts -->
    <?php  
        if(isset($scripts) && !empty($scripts)) {
            foreach ($scripts as $script) {
                if(isset($script) && !empty($script)) {
                    ?>
                        <script src="<?php echo base_url('assets/'.$script.'.js'); ?>"></script>
                    <?php
                }
            }
        }
    ?>
    
    <!-- Serino Main JS -->
    <script src="<?php echo base_url('assets/custom/main.js'); ?>"></script>  

    <?php $this->user_m->display_alert(); ?>
</body>

</html>
