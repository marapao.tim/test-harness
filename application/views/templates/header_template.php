<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= 'Test Harness | '.$title; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">    

    <!-- Bootstrap Paper Custom Theme CSS -->
    <link href="<?= base_url('assets/css/bootstrap.paper.min.css'); ?>" rel="stylesheet">    

    <!-- MetisMenu CSS -->
    <link href="<?= base_url('assets/plugins/metisMenu/metisMenu.min.css'); ?>" rel="stylesheet">    

    <!-- PACE CSS -->
    <link href="<?= base_url('assets/plugins/pace/pace.css'); ?>" rel="stylesheet">

    <!-- Notify Master -->
    <link href="<?= base_url('assets/plugins/notify-master/css/notify.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/plugins/notify-master/css/prettify.css'); ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= base_url('assets/css/sb-admin-2.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/timeline.css'); ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/ionicons.min.css'); ?>" rel="stylesheet" type="text/css">

    <!-- Custom Scripts -->
    <?php 
        if(isset($styles) && !empty($styles)) {
            foreach ($styles as $style) {
                ?>
                    <link rel="stylesheet" href="<?php echo base_url('assets/'.$style.'.css'); ?>"></link>
                <?php
            }
        }
    ?>

    <!-- Serino Master CMS CSS -->
    <link href="<?= base_url('assets/css/serino.master.css'); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script language="javascript" type="text/javascript">
      function resizeIframe(obj) {
        obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
      }
    </script>

    <script type="text/javascript">
        var base_url = "<?php echo base_url();?>";
    </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= base_url(); ?>">
                    <p>
                       <strong>Test Harness</strong>
                    </p>
                </a>
            </div>
            <!-- /.navbar-header -->
            
            <ul class="nav navbar-top-links navbar-right">
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?= $this->session->userdata('user_fullname'); ?> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?= base_url('logout'); ?>"><i class="ion-log-out"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="<?= base_url('main'); ?>"><i class="ion-podium"></i> Dashboard</a>
                        </li>


                       
                        <li class="header">Management</li>
                        

                        
                        <li>
                            <a href="#"><i class="ion-easel"></i> Test Scripts<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?= base_url('test_scripts/add'); ?>">Create Test Script</a>
                                </li>
                                <li>
                                    <a href="<?= base_url('test_scripts'); ?>">View Test Scripts</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>    
                        

                        <?php 
                            if( $this->user_m->checkPermissions(array("test-users","view-users","view-roles","view-permissions","view-logs") ) ) {
                        ?>
                        <li class="header">System</li>
                        <?php } ?>

                        <?php 
                            if( $this->user_m->checkPermissions("view-users") ) {
                                ?>
                                    <li>
                                        <a href="#"><i class="ion-person-stalker"></i> Users<span class="fa arrow"></span></a>
                                        <ul class="nav nav-second-level">
                                            <li>
                                                <a href="<?= base_url('users/save_user'); ?>">Add User</a>
                                            </li>
                                            <li>
                                                <a href="<?= base_url('users/user_management'); ?>">User Management</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav-second-level -->
                                    </li>    
                                <?php
                            }
                        ?>  

                        <?php 
                            if( $this->user_m->checkPermissions("view-roles") ) {
                                ?>
                                    <li>
                                        <a href="#"><i class="ion-person-add"></i> Roles<span class="fa arrow"></span></a>
                                        <ul class="nav nav-second-level">
                                            <li>
                                                <a href="<?= base_url('user_roles/save_user_role'); ?>">Add Role</a>
                                            </li>
                                            <li>
                                                <a href="<?= base_url('user_roles/user_roles_management'); ?>">Role Management</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav-second-level -->
                                    </li>  
                                <?php
                            }
                        ?>  

                        <?php 
                            if( $this->user_m->checkPermissions("view-permissions") ) {
                                ?>
                                    <li>
                                        <a href="#"><i class="ion-key"></i> Permissions<span class="fa arrow"></span></a>
                                        <ul class="nav nav-second-level">
                                            <li>
                                                <a href="<?= base_url('permissions/save_permission'); ?>">Add Permission</a>
                                            </li>
                                            <li>
                                                <a href="<?= base_url('permissions/permissions_management'); ?>">Permission Management</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav-second-level -->
                                    </li>    
                                <?php
                            }
                        ?>                    

                        
                        <?php /*

                        <li>
                            <a href="tables.html"><i class="fa fa-table fa-fw"></i> Tables</a>
                        </li>
                        <li>
                            <a href="forms.html"><i class="fa fa-edit fa-fw"></i> Forms</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="panels-wells.html">Panels and Wells</a>
                                </li>
                                <li>
                                    <a href="buttons.html">Buttons</a>
                                </li>
                                <li>
                                    <a href="notifications.html">Notifications</a>
                                </li>
                                <li>
                                    <a href="typography.html">Typography</a>
                                </li>
                                <li>
                                    <a href="icons.html"> Icons</a>
                                </li>
                                <li>
                                    <a href="grid.html">Grid</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Third Level <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li class="active">
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="active" href="blank.html">Blank Page</a>
                                </li>
                                <li>
                                    <a href="login.html">Login Page</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li> */?>
                    </ul>

                    <ul class="nav">
                        <?php 
                            if( $this->user_m->checkPermissions("view-logs") ) {
                                ?>
                                    <li>
                                        <a href="<?= base_url('logs'); ?>"><i class="ion-clipboard"></i>&nbsp; Logs</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('test'); ?>"><i class="ion-clipboard"></i>&nbsp; Test</a>
                                    </li>
                                <?php
                            }
                        ?> 

                        <!--<li>
                            <a href="<?= base_url('logout'); ?>"><i class="ion-log-out"></i> Logout</a>
                        </li>-->
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <?php if (isset($page_header_data['title']) && isset($page_header_data['description'])): ?>

                <h4 class="page-header"><?= $page_header_data['title']; ?>
                    <small><?= $page_header_data['description']; ?></small>
                    <div class="clearfix"></div>
                </h4>
                    
                <?php endif ?>