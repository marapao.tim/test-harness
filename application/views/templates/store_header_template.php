<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= 'Serino MC | '.$title; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">    

    <!-- Bootstrap Paper Custom Theme CSS -->
    <link href="<?= base_url('assets/css/bootstrap.paper.min.css'); ?>" rel="stylesheet">    

    <!-- MetisMenu CSS -->
    <link href="<?= base_url('assets/plugins/metisMenu/metisMenu.min.css'); ?>" rel="stylesheet">    

    <!-- PACE CSS -->
    <link href="<?= base_url('assets/plugins/pace/pace.css'); ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= base_url('assets/css/sb-admin-2.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/timeline.css'); ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/ionicons.min.css'); ?>" rel="stylesheet" type="text/css">

    <!-- Custom Scripts -->
    <?php 
        if(isset($styles) && !empty($styles)) {
            foreach ($styles as $style) {
                ?>
                    <link rel="stylesheet" href="<?php echo base_url('assets/'.$style.'.css'); ?>"></link>
                <?php
            }
        }
    ?>

    <!-- Serino Master CMS CSS -->
    <link href="<?= base_url('assets/css/serino.master.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/serino.store.css'); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script language="javascript" type="text/javascript">
      function resizeIframe(obj) {
        obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
      }
    </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= base_url(); ?>">
                    <p>
                        <img src="<?= base_url('assets/img/serino-logo.png'); ?>" alt=""> <strong>Serino</strong> MC
                    </p>
                </a>
            </div>
            <!-- /.navbar-header -->
        </nav>