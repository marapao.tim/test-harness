<div class="panel master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            User Roles Table
            <div class="pull-right">
                <a class="btn btn-sm btn-primary btn-default" href="<?php echo base_url('user_roles/save_user_role'); ?>"><i class="ion-plus"></i> Add User Role</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->
    <div class="panel-body">
        <table class="table table-hover datatable" id="user-roles-table">
            <thead>
                <tr>
                    <th></th>
                    <th>Title</th>
                    <th>Date Created</th>
                    <th>Created By</th>
                    <th>Date Modified</th>
                    <th>Modified By</th>
                    <th class="text-center">Actions</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->


<script id="user-role-row-template" type="x-tmpl-mustache">
    <tr>
        <td class="details-control"></td>
        <td>{{user_role_title}}</td>
        <td>{{date_created}}</td>
        <td>{{created_by}}</td>
        <td>{{date_modified}}</td>
        <td>{{modified_by}}</td>
        <td class="text-center">
            <a href="<?php echo base_url('user_roles/save_user_role/{{user_role_id}}'); ?>" rel="tooltip" title="Edit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></a>
            <a href="<?php echo base_url('user_roles/delete_user_role/{{user_role_id}}'); ?>" rel="tooltip" title="Delete" data-user-role-id="{{user_role_id}}" class="btn btn-default btn-xs delete-user-role-btn" data-action-modal-message="Are you sure?"><i class="fa fa-remove"></i></a>
        </td>
        <td>{{user_role_permissions}}</td>
    </tr>
</script>