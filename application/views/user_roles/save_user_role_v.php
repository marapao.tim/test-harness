<div class="panel panel-default master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            <?= ($this->uri->segment(3)) ? 'Edit ' : 'Add '; ?>User Role Form
            <div class="pull-right">
                <button class="btn btn-box-user-role btn-xs btn-link" rel="tooltip" title="Collapse" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <input type="hidden" value="<?= ($this->uri->segment(3)) ? 'edit' : 'add'; ?>" data-user-role-id="<?= $this->uri->segment(3); ?>" id="form-mode">

    <?php echo form_open_multipart('user_roles/save_user_role/'.$this->uri->segment(3), array('role' => 'form', 'class' => 'form-horizontal', 'id' => 'save-user-role-form')); 
    ?>
        <div class="panel-body">
            <div class="form-group">
                <label for="user_role_title" class="control-label label-left col-md-2">Title</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="user_role_title" id="user_role_title" placeholder="" value="">
                </div>
            </div>  

            <div class="form-group">
                <label for="user_role_description" class="control-label label-left col-md-2">Description</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="user_role_description" id="user_role_description" placeholder="" value="">
                </div>
            </div>  

            <div class="form-group">
                <label for="user_role_remarks" class="control-label label-left col-md-2">Remarks</label>
                <div class="col-md-10">
                    <textarea type="text" class="form-control" name="user_role_remarks" id="user_role_remarks" placeholder="" rows="3"></textarea>
                </div>
            </div>   

            <h6>Site Permissions</h6>
            <?php
                $ctr = 1;
                $ind = 0;
                $this->config->load("permissions");
                $site_permissions = $this->config->item("site_permissions");
                foreach ($site_permissions as $key => $value) {
                    if($ctr == 1){
            ?>
            <div class="col-md-12">
            <?php
                    }
             ?>
                <div class="col-sm-3">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <strong><?=$key;?></strong>
                            <span>
                                <input type="checkbox" class="select-all pull-right">
                            </span>
                        </li>

                        <?php
                            foreach($value as $e=>$vel){
                                $checked = "";
                                if($this->uri->segment(3) && in_array($vel,$user_role->user_role_permissions)){
                                    $checked = "checked";
                                }
                        ?>
                        <li data-toggle="tooltip" title="" class="list-group-item" data-original-title="<?=$e;?>">
                            <?=$e;?>
                            <span>
                                <input name="user_role_permissions[]" type="checkbox" class="select-permission pull-right" value="<?=$vel;?>" <?=$checked;?>>
                            </span>
                        </li>
                        <?php
                            }
                        ?>
                    </ul>
                </div>
            <?php
                    $ctr++;
                    if($ctr > 4 || $ind == count($site_permissions) - 1){
                        $ctr = 1;
            ?>
            </div>
            <?php            
                    }          
                    $ind++;
                }
            ?>

            <h6>Dynamic Permissions</h6>
            <div class="col-md-12">
            <?php 


                $temp = "";
                $totalwidth = 0;
                foreach ($permissions as $key => $value) {
                        $isSelected = "";
                        if($this->uri->segment(3)) {
                            if(array_search($value->permission_code, $user_role->user_role_permissions) !== false) {
                               $isSelected = 'checked="checked"';                               
                            }
                        }
                        if($value->permission_group != $temp) {
                            if($temp != "") {
                            ?>
                            </ul></div>

                            <?php }
                            if($totalwidth == 12) {
                            ?>
                                </div>
                                <div class="col-md-12">
                            <?php
                            }
                            $totalwidth += 3;
                            ?>
                            <div class="col-sm-3"><ul class="list-group">
                                <li class="list-group-item"><strong><?= $value->permission_group; ?></strong><span><input type="checkbox" class="select-all pull-right"></span></li>
                            <?php
                        }

                    ?>
                          <li data-toggle="tooltip" title="<?= $value->permission_description; ?>" class="list-group-item"><?= $value->permission_title; ?><span><input name="user_role_permissions[]" <?= $isSelected ?> type="checkbox" class="select-permission pull-right" value="<?= $value->permission_code; ?>"></span></li>
                    <?php
                    $temp = $value->permission_group;
                }
            ?>
                </div>
            </div>
        </div>

        <div class="panel-footer">
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <a href="<?= base_url('user_roles/user_roles_management'); ?>" class="btn btn-default btn-sm">Back</a>
                    <button class="btn btn-primary btn-sm" id="save-user-role-btn">Save</button>
                </div>
            </div>
        </div>
     <?php echo form_close(); ?>

</div>