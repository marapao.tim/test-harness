<div class="col-md-12">
    <div class="row">
        <div class="col-md-3 text-center">
                


            <div class="thumbnail project-logo-container" style="max-height: 170px; background: transparent; ">
                <img src="<?= ($project->project_logo != '' && $project->project_logo != NULL) ? base_url('store/projects/'.$project->project_name.'/images/150x150/'.$project->project_logo) : base_url('assets/img/default-app.png'); ?>" class="">
                <div class='loader4 hidden'>
                  <div>
                    <div>
                      <div>
                        <div>
                          <div>
                            <div>
                              <div>
                                <div>
                                  <div>
                                    <div></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <?php if($this->user_m->checkPermissions("edit-projects")){ ?>
            <form action="<?= base_url('projects/upload_project_logo/'.$this->uri->segment(3)); ?>" id="" enctype="multipart/form-data">
                <input type="file" id="project-logo-uploader" name="project_logo" class="form-control project-logo-uploader">
                <label for="project-logo-uploader" class="btn btn-primary btn-sm">Change</label>
                <br>
                <br>
            </form>
            <?php } ?>

            <table class="table">
                <tr>
                    <th><i class="fa fa-lightbulb-o"></i> Status</th>
                    <td>
                        <?= $project->project_status; ?>
                    </td>
                </tr> 
                <tr>
                    <th><i class="fa fa-edit"></i> Created by</th>
                    <td>
                        <?= $this->user_m->get($project->created_by)->user_fullname; ?>
                    </td>
                </tr>                
                <tr>
                    <th><i class="fa fa-calendar"></i> Date created</th>
                    <td>
                        <abbr class="timeago" rel="tooltip" title="<?= date($this->master_config['date_time_format'], strtotime($project->date_created)); ?>"></abbr>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-9">

            <div role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-paper" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#project_details" aria-controls="project_details" role="tab" data-toggle="tab">Project Details</a>
                    </li>                            
                    <!--<li role="presentation">
                        <a href="#theme_and_tools" aria-controls="theme_and_tools" role="tab" data-toggle="tab">Themes and Tools</a>
                    </li>-->
                    <!--<li role="presentation">
                        <a href="#configurations" aria-controls="tab" role="tab" data-toggle="tab">Configurations</a>
                    </li>-->
                    <li role="presentation">
                        <a href="#environment_instances" aria-controls="tab" role="tab" data-toggle="tab">Environment Instances</a>
                    </li>
                    <li role="presentation">
                        <a href="#project_artifacts" aria-controls="tab" role="tab" data-toggle="tab">Artifacts</a>
                    </li>
                </ul>
            
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="project_details">
                        <div class="panel panel-default">
                            <br>
                            <div class="panel-body">
                                <!-- form start -->
                                <?php echo form_open_multipart('projects/save_project/'.$this->uri->segment(3), array('role' => 'form', 'data-parsley-validate' => 'true', 'class' => 'form-horizontal')); 
                                ?>
                                    <input type="hidden" name="redirect" value="<?= base_url('projects/view_project/'.$project->project_id); ?>">
                                    <div class="form-group">
                                        <label for="project_title" class="control-label col-md-2">Title</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="project_title" id="project_title" placeholder="Project Title" value="<?php echo $project->project_title; ?>" data-parsley-required>
                                        </div>
                                    </div>                          

                                    <div class="form-group">
                                        <label for="project_name" class="control-label col-md-2">Name</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="project_name" id="project_name" placeholder="Project Name" value="<?php echo $project->project_name; ?>" 
                                            data-parsley-required
                                            data-parsley-projectnameexists="<?php echo base_url('projects/project_name_exists/'.$this->uri->segment(3)); ?>"
                                            readonly>
                                        </div>
                                    </div>  

                                    <div class="form-group">
                                        <label for="client_name" class="control-label col-md-2">Client</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="client_name" id="client_name" placeholder="Client Name" value="<?php echo $project->client_name; ?>" data-parsley-required>
                                        </div>
                                    </div>                          

                                    <div class="form-group">
                                        <label for="project_description" class="control-label col-md-2">Description</label>
                                        <div class="col-md-8">
                                            <textarea class="form-control" name="project_description" id="project_description" placeholder="Project Description"><?php echo $project->project_description; ?></textarea>
                                        </div>
                                    </div>  

                                    <div class="form-group">
                                        <label for="client_name" class="control-label col-md-2">Status</label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="project_status" id="project_status">
                                                <option <?=($project->project_status == "Not Started" ? "selected" : "" );?>>Not Started</option>
                                                <option <?=($project->project_status == "In Progress" ? "selected" : "" );?>>In Progress</option>
                                                <option <?=($project->project_status == "Discontinued" ? "selected" : "" );?>>Discontinued</option>
                                                <option <?=($project->project_status == "Cancelled" ? "selected" : "" );?>>Cancelled</option>
                                                <option <?=($project->project_status == "Completed" ? "selected" : "" );?>>Completed</option>
                                            </select>
                                            
                                        </div>
                                    </div>    
                                    <?php if($this->user_m->checkPermissions("edit-project")){ ?>
                                    <div class="form-group">
                                        <div class="col-md-2 col-md-offset-8">
                                            <button type="submit" class="btn btn-primary btn-sm pull-right">Save</button>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="theme_and_tools">
                        <div class="panel panel-default">
                            <br>
                            <div class="panel-body">
                                <!-- form start -->
                                <?php echo form_open_multipart('projects/save_project/'.$this->uri->segment(3), array('role' => 'form', 'data-parsley-validate' => 'true', 'class' => 'form-horizontal')); 
                                ?>
                                    <input type="hidden" name="redirect" value="<?= base_url('projects/view_project/'.$project->project_id); ?>">

                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Themes</label>

                                        <div class="col-md-10">
                                            <select multiple="multiple" class="form-control multiselect" id="themes" name="themes[]">
                                                <?php 
                                                    $preselected_themes = explode(',',$project->themes);
                                                    if(count($themes) > 0) {
                                                        foreach ($themes as $key => $value) {
                                                            ?>
                                                                <option value="<?= $value->theme_id; ?>" <?= (in_array($value->theme_id, $preselected_themes)) ? 'selected' : ''; ?>><?= $value->theme_title; ?></option>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                            <option value="" disabled>No themes available</option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>   

                                            <p class="text-muted">Select the themes you want to include in the package.</p>       
                                        </div>
                                    </div>      

                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Tools</label>

                                        <div class="col-md-10">
                                            <select multiple="multiple" class="form-control multiselect" id="tools" name="tools[]">
                                                <?php 
                                                    if(count($tools) > 0) {
                                                        $preselected_tools = explode(',',$project->tools);
                                                        foreach ($tools as $key => $value) {
                                                            ?>
                                                                <option value="<?= $value->tool_id; ?>" <?= (in_array($value->tool_id, $preselected_tools)) ? 'selected' : ''; ?>><?= $value->tool_title; ?></option>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                            <option value="" disabled>No tools available</option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>   

                                            <p class="text-muted">Select the tools you want to include in the package.</p>       
                                        </div>
                                    </div>  

                                    <div class="form-group">
                                        <div class="col-md-offset-2 col-md-2">
                                            <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="configurations">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <!-- form start -->
                                <?php echo form_open_multipart('projects/save_project/'.$this->uri->segment(3), array('role' => 'form', 'data-parsley-validate' => 'true', 'class' => 'form-horizontal')); 
                                ?>
                                    <input type="hidden" name="redirect" value="<?= base_url('projects/view_project/'.$project->project_id); ?>">
                                    <h6>General</h6>


                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Site Title</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="config_meta[site_title]" id="site_title" placeholder="Site Title" value="<?php echo element('site_title',$project->config_meta); ?>" data-parsley-required>
                                        </div>
                                    </div>                       

                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Base URL</label>
                                        <div class="col-md-6" id="base_url_form_group">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="base_url_addon">https://</span>
                                                <input type="text" class="form-control" name="config_meta[base_url]" id="base_url" placeholder="www.my-domain.com" value="<?php echo element('base_url',$project->config_meta); ?>" aria-describedby="base_url_addon" data-parsley-errors-container="#base_url_form_group" data-parsley-class-handler="#base_url_form_group" data-parsley-required>
                                            </div>
                                        </div>
                                    </div>                        

                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Root URL</label>
                                        <div class="col-md-6" id="root_url_form_group">
                                            <div class="input-group">
                                            <span class="input-group-addon" id="root_url_addon">Document Root</span>
                                                <input type="text" class="form-control" name="config_meta[root_url]" id="root_url" placeholder="my_project" value="<?php echo element('root_url',$project->config_meta); ?>" aria-describedby="root_url_addon" data-parsley-errors-container="#root_url_form_group" data-parsley-class-handler="#root_url_form_group" data-parsley-required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="timezone" class="for control-label col-md-2">Timezone</label>
                                        <div class="col-md-6">
                                            <select name="config_meta[timezone]" id="timezone" class="select2" style="width: 100%;" data-parsley-required>
                                                <option value="">--Choose One--</option>
                                                <?php 
                                                    foreach ($timezones as $timezone) {
                                                        ?>
                                                            <option value="<?php print $timezone['zone'] ?>" <?= ($timezone['zone'] == element('timezone', $project->config_meta)) ? 'selected="selected"' : ''; ?>><?php print $timezone['diff_from_GMT'] . ' - ' . $timezone['zone'] ?></option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <h6>Database Settings</h6>

                                    <div class="form-group">
                                        <label for="server_name" class="control-label col-md-2">Server Name</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="config_meta[server_name]" id="server_name" placeholder="www.my-domain.com" value="<?php echo element('server_name',$project->config_meta); ?>" data-parsley-required>
                                        </div>
                                    </div>                        

                                    <div class="form-group">
                                        <label for="db_name" class="control-label col-md-2">Name</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="config_meta[db_name]" id="db_name" placeholder="Database Name" value="<?php echo element('db_name',$project->config_meta); ?>" data-parsley-required>
                                        </div>
                                    </div>                        

                                    <div class="form-group">
                                        <label for="db_user" class="control-label col-md-2">User</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="config_meta[db_user]" id="db_user" placeholder="Database User" value="<?php echo element('db_user',$project->config_meta); ?>" data-parsley-required>
                                        </div>
                                    </div>                        

                                    <div class="form-group">
                                        <label for="db_password" class="control-label col-md-2">Password</label>
                                        <div class="col-md-6">
                                            <input type="password" class="form-control" name="config_meta[db_password]" id="db_password" placeholder="Database Password" value="" data-parsley-required>
                                        </div>
                                    </div>

                                    <h6>Email Settings</h6>

                                    <div class="form-group">
                                        <label for="smtp_security" class="control-label col-md-2">Security</label>
                                        <div class="col-md-6">
                                            <select name="config_meta[smtp_security]" id="smtp_security" class="select2" style="width: 100%;" data-parsley-required>
                                            <?php 
                                                $security_protocols = array('' => '--Choose One--','tls' => 'TLS', 'ssl' => 'SSL');

                                                foreach ($security_protocols as $key => $value) {
                                                    ?>
                                                        <option value="<?= $key; ?>" <?= ($key == element('smtp_security',$project->config_meta)) ? 'selected' : ''; ?>><?= $value; ?></option>
                                                    <?php
                                                }
                                            ?>
                                            </select>
                                        </div>
                                    </div>                      

                                    <div class="form-group">
                                        <label for="smtp_host" class="control-label col-md-2">SMTP Host</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="config_meta[smtp_host]" id="smtp_host" placeholder="SMTP Host" value="<?php echo element('smtp_host',$project->config_meta); ?>" data-parsley-required>
                                        </div>
                                    </div>                         

                                    <div class="form-group">
                                        <label for="smtp_port" class="control-label col-md-2">SMTP Port</label>
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="config_meta[smtp_port]" id="smtp_port" placeholder="SMTP Port" value="<?php echo element('smtp_port',$project->config_meta); ?>" data-parsley-required>
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <label for="smtp_username" class="control-label col-md-2">SMTP Username</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="config_meta[smtp_username]" id="smtp_username" placeholder="SMTP Username" value="<?php echo element('smtp_username',$project->config_meta); ?>" data-parsley-required>
                                        </div>
                                    </div>                         

                                    <div class="form-group">
                                        <label for="smtp_password" class="control-label col-md-2">SMTP Password</label>
                                        <div class="col-md-6">
                                            <input type="password" class="form-control" name="config_meta[smtp_password]" id="smtp_password" placeholder="SMTP Password" value="" data-parsley-required>
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <div class="col-md-2 col-md-offset-6">
                                            <button type="submit" class="btn btn-primary btn-sm pull-right">Save</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="environment_instances">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <!-- form start -->
                                <?php echo form_open_multipart('projects/save_project/'.$this->uri->segment(3), array('role' => 'form', 'data-parsley-validate' => 'true', 'class' => 'form-horizontal')); 
                                ?>
                                    <input type="hidden" name="redirect" value="<?= base_url('projects/view_project/'.$project->project_id); ?>">

                                    <div role="tabpanel">
                                        <!-- Nav tabs -->
                                        <ul class="side-nav-tabs nav nav-tabs nav-paper" role="tablist">
                                            <li role="presentation" class="active">
                                                <a href="#instance_dev" aria-controls="project_details" role="tab" data-toggle="tab">Dev</a>
                                            </li>                            
                                            <li role="presentation">
                                                <a href="#instance_qa" aria-controls="theme_and_tools" role="tab" data-toggle="tab">QA</a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#instance_uat" aria-controls="tab" role="tab" data-toggle="tab">UAT</a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#instance_prod" aria-controls="tab" role="tab" data-toggle="tab">Prod</a>
                                            </li>
                                        </ul>
                                    
                                        <!-- Tab panes -->
                                        <div class="side-tab-content tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="instance_dev">
                                                <div class="panel panel-default">
                                                    <br>
                                                    <div class="panel-body">
                                                        <h6>Site</h6>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Public Site URL</label>
                                                            <a target="_blank" href="<?php echo element('dev_public_site_url',$project->config_meta); ?>"><i class="fa fa-eye"></i></a>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="config_meta[dev_public_site_url]" id="dev_public_site_url" placeholder="Dev Public Site URL" value="<?php echo element('dev_public_site_url',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>                       

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Admin Site URL</label>
                                                            <a target="_blank" href="<?php echo element('dev_admin_site_url',$project->config_meta); ?>"><i class="fa fa-eye"></i></a>
                                                            <div class="col-md-6" id="base_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[dev_admin_site_url]" id="dev_admin_site_url" placeholder="Dev Admin Site URL" value="<?php echo element('dev_admin_site_url',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>                        

                                                        <hr> 

                                                        <h6>Account</h6>
                                                        <div class="form-group <?=($this->user_m->checkPermissions("view-projects-root-creds") ? "" : "hidden");?>">
                                                            <label for="" class="col-md-2 control-label">Root Username</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[dev_root_username]" id="dev_root_username" placeholder="Dev Root Username" value="<?php echo element('dev_root_username',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group <?=($this->user_m->checkPermissions("view-projects-root-creds") ? "" : "hidden");?>">
                                                            <label for="" class="col-md-2 control-label">Root Password</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[dev_root_password]" id="dev_root_password" placeholder="Dev Root Password" value="<?php echo element('dev_root_password',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Account ID</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[dev_account_id]" id="dev_account_id" placeholder="Dev Account ID" value="<?php echo element('dev_account_id',$project->config_meta); ?>">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Site ID</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[dev_site_id]" id="dev_site_id" placeholder="Dev Site ID" value="<?php echo element('dev_site_id',$project->config_meta); ?>">
                                                            </div>
                                                        </div>

                                                        <hr> 
                                                        
                                                        <h6>Config 
                                                            <?php if($this->user_m->checkPermissions("edit-projects")){ ?>
                                                            <a ins-env="dev" data-instance="<?=site_url("v1/api/fetch");?>" class="fetch-config" href="javascript:;" title="Fetch Config"  token="<?=$token;?>"><i class="fa fa-cloud-download"></i></a>
                                                            <?php } ?>
                                                        </h6>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Context</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[dev_context]" id="dev_context" placeholder="Dev Context" value="<?php echo element('dev_context',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Integration Key</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[dev_integration_key]" id="dev_integration_key" placeholder="Dev Integration Key" value="<?php echo element('dev_integration_key',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Database Name</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[dev_db_name]" id="dev_db_name" placeholder="Dev Database Name" value="<?php echo element('dev_db_name',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <hr> 
                                                        
                                                        <h6>Assets 
                                                            <?php if($this->user_m->checkPermissions("edit-projects")){ ?>
                                                            <a ins-env="dev" data-instance="<?=site_url("v1/api/fetch");?>" class="fetch-assets" href="javascript:;" title="Fetch Assets"  token="<?=$token;?>"><i class="fa fa-cloud-download"></i></a>
                                                            <?php } ?>
                                                        </h6>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Themes</label>
                                                            <div class="col-md-4" id="root_url_form_group">
                                                                <textarea rows="10" class="form-control " name="config_meta[dev_assets_themes]" id="dev_assets_themes" readonly=""><?php echo element('dev_assets_themes',$project->config_meta); ?></textarea>
                                                                
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Plugins</label>
                                                            <div class="col-md-4" id="root_url_form_group">
                                                                <textarea rows="10" class="form-control " name="config_meta[dev_assets_plugins]" id="dev_assets_plugins" readonly=""><?php echo element('dev_assets_plugins',$project->config_meta); ?></textarea>
                                                                
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Tools</label>
                                                            <div class="col-md-4" id="root_url_form_group">
                                                                <textarea rows="10" class="form-control " name="config_meta[dev_assets_tools]" id="dev_assets_tools" readonly=""><?php echo element('dev_assets_tools',$project->config_meta); ?></textarea>
                                                                
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                            <div role="tabpanel" class="tab-pane" id="instance_qa">
                                                <div class="panel panel-default">
                                                    <br>
                                                    <div class="panel-body">
                                                        <h6>Site</h6>


                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Public Site URL</label>
                                                            <a target="_blank" href="<?php echo element('qa_public_site_url',$project->config_meta); ?>"><i class="fa fa-eye"></i></a>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="config_meta[qa_public_site_url]" id="qa_public_site_url" placeholder="QA Public Site URL" value="<?php echo element('qa_public_site_url',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>                       

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Admin Site URL</label>
                                                            <a target="_blank" href="<?php echo element('qa_admin_site_url',$project->config_meta); ?>"><i class="fa fa-eye"></i></a>
                                                            <div class="col-md-6" id="base_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[qa_admin_site_url]" id="qa_admin_site_url" placeholder="QA Admin Site URL" value="<?php echo element('qa_admin_site_url',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>                        

                                                        <hr> 
                                                        
                                                        <h6>Account</h6>

                                                        <div class="form-group <?=($this->user_m->checkPermissions("view-projects-root-creds") ? "" : "hidden");?>">
                                                            <label for="" class="col-md-2 control-label">Root Username</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[qa_root_username]" id="qa_root_username" placeholder="QA Root Username" value="<?php echo element('qa_root_username',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group <?=($this->user_m->checkPermissions("view-projects-root-creds") ? "" : "hidden");?>">
                                                            <label for="" class="col-md-2 control-label">Root Password</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[qa_root_password]" id="qa_root_password" placeholder="QA Root Password" value="<?php echo element('qa_root_password',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Account ID</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[qa_account_id]" id="qa_account_id" placeholder="QA Account ID" value="<?php echo element('qa_account_id',$project->config_meta); ?>" >
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Site ID</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[qa_site_id]" id="qa_site_id" placeholder="QA Site ID" value="<?php echo element('qa_site_id',$project->config_meta); ?>" >
                                                            </div>
                                                        </div>

                                                        <hr> 
                                                        
                                                        <h6>Config 
                                                            <?php if($this->user_m->checkPermissions("edit-projects")){ ?>
                                                            <a ins-env="qa" data-instance="<?=site_url("v1/api/fetch");?>" class="fetch-config" href="javascript:;" title="Fetch Config"  token="<?=$token;?>"><i class="fa fa-cloud-download"></i></a>
                                                            <?php } ?>
                                                        </h6>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Context</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[qa_context]" id="qa_context" placeholder="QA Context" value="<?php echo element('qa_context',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Integration Key</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[qa_integration_key]" id="qa_integration_key" placeholder="QA Integration Key" value="<?php echo element('qa_integration_key',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Database Name</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[qa_db_name]" id="qa_db_name" placeholder="QA Database Name" value="<?php echo element('qa_db_name',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <hr> 
                                                        
                                                        <h6>Assets 
                                                            <?php if($this->user_m->checkPermissions("edit-projects")){ ?>
                                                            <a ins-env="qa" data-instance="<?=site_url("v1/api/fetch");?>" class="fetch-assets" href="javascript:;" title="Fetch Assets"  token="<?=$token;?>"><i class="fa fa-cloud-download"></i></a>
                                                            <?php } ?>
                                                        </h6>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Themes</label>
                                                            <div class="col-md-4" id="root_url_form_group">
                                                                <textarea rows="10" class="form-control " name="config_meta[qa_assets_themes]" id="qa_assets_themes" readonly=""><?php echo element('qa_assets_themes',$project->config_meta); ?></textarea>
                                                                
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Plugins</label>
                                                            <div class="col-md-4" id="root_url_form_group">
                                                                <textarea rows="10" class="form-control " name="config_meta[qa_assets_plugins]" id="qa_assets_plugins" readonly=""><?php echo element('qa_assets_plugins',$project->config_meta); ?></textarea>
                                                                
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Tools</label>
                                                            <div class="col-md-4" id="root_url_form_group">
                                                                <textarea rows="10" class="form-control " name="config_meta[qa_assets_tools]" id="qa_assets_tools" readonly=""><?php echo element('qa_assets_tools',$project->config_meta); ?></textarea>
                                                                
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div role="tabpanel" class="tab-pane" id="instance_uat">
                                                <div class="panel panel-default">
                                                    <br>
                                                    <div class="panel-body">
                                                        <h6>Site</h6>


                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Public Site URL</label>
                                                            <a target="_blank" href="<?php echo element('uat_public_site_url',$project->config_meta); ?>"><i class="fa fa-eye"></i></a>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="config_meta[uat_public_site_url]" id="uat_public_site_url" placeholder="UAT Public Site URL" value="<?php echo element('uat_public_site_url',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>                       

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Admin Site URL</label>
                                                            <a target="_blank" href="<?php echo element('uat_admin_site_url',$project->config_meta); ?>"><i class="fa fa-eye"></i></a>
                                                            <div class="col-md-6" id="base_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[uat_admin_site_url]" id="uat_admin_site_url" placeholder="UAT Admin Site URL" value="<?php echo element('uat_admin_site_url',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>                        

                                                        <hr> 
                                                        
                                                        <h6>Account</h6>

                                                        <div class="form-group <?=($this->user_m->checkPermissions("view-projects-root-creds") ? "" : "hidden");?>">
                                                            <label for="" class="col-md-2 control-label">Root Username</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[uat_root_username]" id="uat_root_username" placeholder="UAT Root Username" value="<?php echo element('uat_root_username',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group <?=($this->user_m->checkPermissions("view-projects-root-creds") ? "" : "hidden");?>">
                                                            <label for="" class="col-md-2 control-label">Root Password</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[uat_root_password]" id="uat_root_password" placeholder="UAT Root Password" value="<?php echo element('uat_root_password',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Account ID</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[uat_account_id]" id="uat_account_id" placeholder="UAT Account ID" value="<?php echo element('uat_account_id',$project->config_meta); ?>">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Site ID</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[uat_site_id]" id="uat_site_id" placeholder="UAT Site ID" value="<?php echo element('uat_site_id',$project->config_meta); ?>">
                                                            </div>
                                                        </div>

                                                        <hr> 
                                                        
                                                        <h6>Config 
                                                            <?php if($this->user_m->checkPermissions("edit-projects")){ ?>
                                                            <a ins-env="uat" data-instance="<?=site_url("v1/api/fetch");?>" class="fetch-config" href="javascript:;" title="Fetch Config"  token="<?=$token;?>"><i class="fa fa-cloud-download"></i></a>
                                                            <?php } ?>
                                                        </h6>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Context</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[uat_context]" id="uat_context" placeholder="UAT Context" value="<?php echo element('uat_context',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Integration Key</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[uat_integration_key]" id="uat_integration_key" placeholder="UAT Integration Key" value="<?php echo element('uat_integration_key',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Database Name</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[uat_db_name]" id="uat_db_name" placeholder="UAT Database Name" value="<?php echo element('uat_db_name',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <hr> 
                                                        
                                                        <h6>Assets 
                                                            <?php if($this->user_m->checkPermissions("edit-projects")){ ?>
                                                            <a ins-env="uat" data-instance="<?=site_url("v1/api/fetch");?>" class="fetch-assets" href="javascript:;" title="Fetch Assets"  token="<?=$token;?>"><i class="fa fa-cloud-download"></i></a>
                                                            <?php } ?>
                                                        </h6>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Themes</label>
                                                            <div class="col-md-4" id="root_url_form_group">
                                                                <textarea rows="10" class="form-control " name="config_meta[uat_assets_themes]" id="uat_assets_themes" readonly=""><?php echo element('uat_assets_themes',$project->config_meta); ?></textarea>
                                                                
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Plugins</label>
                                                            <div class="col-md-4" id="root_url_form_group">
                                                                <textarea rows="10" class="form-control " name="config_meta[uat_assets_plugins]" id="uat_assets_plugins" readonly=""><?php echo element('uat_assets_plugins',$project->config_meta); ?></textarea>
                                                                
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Tools</label>
                                                            <div class="col-md-4" id="root_url_form_group">
                                                                <textarea rows="10" class="form-control " name="config_meta[uat_assets_tools]" id="uat_assets_tools" readonly=""><?php echo element('uat_assets_tools',$project->config_meta); ?></textarea>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div role="tabpanel" class="tab-pane" id="instance_prod">
                                                <div class="panel panel-default">
                                                    <br>
                                                    <div class="panel-body">

                                                        <h6>Site</h6>


                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Public Site URL</label>
                                                            <a target="_blank" href="<?php echo element('prod_public_site_url',$project->config_meta); ?>"><i class="fa fa-eye"></i></a>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="config_meta[prod_public_site_url]" id="prod_public_site_url" placeholder="Prod Public Site URL" value="<?php echo element('prod_public_site_url',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>                       

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Admin Site URL</label>
                                                            <a target="_blank" href="<?php echo element('prod_admin_site_url',$project->config_meta); ?>"><i class="fa fa-eye"></i></a>
                                                            <div class="col-md-6" id="base_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[prod_admin_site_url]" id="prod_admin_site_url" placeholder="Prod Admin Site URL" value="<?php echo element('prod_admin_site_url',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>                        

                                                        <hr> 
                                                        
                                                        <h6>Account</h6>

                                                        <div class="form-group <?=($this->user_m->checkPermissions("view-projects-root-creds") ? "" : "hidden");?>">
                                                            <label for="" class="col-md-2 control-label">Root Username</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[prod_root_username]" id="prod_root_username" placeholder="Prod Root Username" value="<?php echo element('prod_root_username',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group <?=($this->user_m->checkPermissions("view-projects-root-creds") ? "" : "hidden");?>">
                                                            <label for="" class="col-md-2 control-label">Root Password</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[prod_root_password]" id="prod_root_password" placeholder="Prod Root Password" value="<?php echo element('prod_root_password',$project->config_meta); ?>" data-parsley-required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Account ID</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[prod_account_id]" id="prod_account_id" placeholder="PROD Account ID" value="<?php echo element('prod_account_id',$project->config_meta); ?>">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Site ID</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[prod_site_id]" id="prod_site_id" placeholder="PROD Site ID" value="<?php echo element('prod_site_id',$project->config_meta); ?>">
                                                            </div>
                                                        </div>

                                                        <hr> 
                                                        
                                                        <h6>Config 
                                                            <?php if($this->user_m->checkPermissions("edit-projects")){ ?>
                                                            <a ins-env="prod" data-instance="<?=site_url("v1/api/fetch");?>" class="fetch-config" href="javascript:;" title="Fetch Config"  token="<?=$token;?>"><i class="fa fa-cloud-download"></i></a>
                                                            <?php } ?>
                                                        </h6>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Context</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[prod_context]" id="prod_context" placeholder="Prod Context" value="<?php echo element('prod_context',$project->config_meta); ?>">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Integration Key</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[prod_integration_key]" id="prod_integration_key" placeholder="Prod Integration Key" value="<?php echo element('prod_integration_key',$project->config_meta); ?>">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Database Name</label>
                                                            <div class="col-md-6" id="root_url_form_group">
                                                                <input type="text" class="form-control" name="config_meta[prod_db_name]" id="prod_db_name" placeholder="Prod Database Name" value="<?php echo element('prod_db_name',$project->config_meta); ?>">
                                                            </div>
                                                        </div>

                                                        <hr> 
                                                        
                                                        <h6>Assets 
                                                            <?php if($this->user_m->checkPermissions("edit-projects")){ ?>
                                                            <a ins-env="prod" data-instance="<?=site_url("v1/api/fetch");?>" class="fetch-assets" href="javascript:;" title="Fetch Assets"  token="<?=$token;?>"><i class="fa fa-cloud-download"></i></a>
                                                            <?php } ?>
                                                        </h6>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Themes</label>
                                                            <div class="col-md-4" id="root_url_form_group">
                                                                <textarea rows="10" class="form-control " name="config_meta[prod_assets_themes]" id="prod_assets_themes" readonly=""><?php echo element('prod_assets_themes',$project->config_meta); ?></textarea>
                                                                
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Plugins</label>
                                                            <div class="col-md-4" id="root_url_form_group">
                                                                <textarea rows="10" class="form-control " name="config_meta[prod_assets_plugins]" id="prod_assets_plugins" readonly=""><?php echo element('prod_assets_plugins',$project->config_meta); ?></textarea>
                                                                
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="" class="col-md-2 control-label">Tools</label>
                                                            <div class="col-md-4" id="root_url_form_group">
                                                                <textarea rows="10" class="form-control " name="config_meta[prod_assets_tools]" id="prod_assets_tools" readonly=""><?php echo element('prod_assets_tools',$project->config_meta); ?></textarea>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php if($this->user_m->checkPermissions("edit-projects")){ ?>
                                        <div class="col-md-2 col-md-offset-6">
                                            <button type="submit" class="btn btn-primary btn-sm pull-right">Save</button>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="project_artifacts">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-md-12" style="margin-bottom:15px;">
                                    <button id="upload-artifact" type="submit" class="btn btn-primary btn-sm pull-right">Upload New</button>
                                </div>
                                <div class="col-md-12">
                                    <table class="table table-hover datatable dataTable no-footer dtr-inline" id="artifacts_list" role="grid">
                                        <thead>
                                            <tr>
                                                <th>Last Modified</th>
                                                <th>Name</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th>Latest Version</th>
                                                <th>Created By</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach($artifacts as $index=>$artifact){
                                            ?>
                                            <tr>
                                                <td><?=$artifact->artifact_datemodified;?></td>
                                                <td><?=$artifact->artifact_name;?></td>
                                                <td><?=$artifact->artifact_description;?></td>
                                                <td><?=$artifact->artifact_status;?></td>
                                                <td>
                                                    <?=$artifact->files[0]->file_version;?>
                                                    <div class="files-list-container hidden">
                                                        <div class="col-md-12" style="margin-bottom:15px;">
                                                            <button type="button" class="btn-artifact-files btn btn-primary btn-sm pull-right" 
                                                                version-major="<?=explode(".",$artifact->files[0]->file_version)[0];?>"
                                                                version-minor="<?=explode(".",$artifact->files[0]->file_version)[1];?>"
                                                                version-patch="<?=explode(".",$artifact->files[0]->file_version)[2];?>"
                                                                aid="<?=$artifact->files[0]->artifact_id;?>"
                                                                a-name="<?=$artifact->artifact_name;?>"
                                                                url="<?= base_url('projects/upload_artifact_files/'.$this->uri->segment(3).'/'.$artifact->files[0]->artifact_id); ?>"
                                                                >
                                                                Upload New
                                                            </button>
                                                        </div>

                                                        <table class="table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>Version</th>
                                                                    <th>Notes</th>
                                                                    <th>Created By</th>
                                                                    <th>Link</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                        <?php
                                                            foreach($artifact->files as $f=>$file){
                                                        ?>
                                                            <tr>
                                                                <td><?=$file->file_version;?></td>
                                                                <td><?=$file->file_description;?></td>
                                                                <td><?=$file->file_createdby;?></td>
                                                                <td>
                                                                    <a href="<?=base_url("/projects/view_artifacts/".$artifact->id."/".$file->id);?>" target="_blank" rel="tooltip" title="View" class="btn-view-files btn btn-default btn-xs" data-original-title="View">
                                                                        <i class="fa fa-eye"></i>
                                                                    </a>
                                                                </td>            
                                                            </tr>                                                                
                                                        <?php
                                                            }
                                                        ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                                <td><?=$artifact->artifact_createdby;?></td>
                                                <td class="text-center">
                                                    <a href="javascript:;" rel="tooltip" title="View" class="btn-view-files btn btn-default btn-xs" data-original-title="View">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<script id="fetch-config-template" type="text/x-handlebars-template">
    <div id="fetch-modal-alerts" class="alert alert-danger hidden">
      Indicates a neutral informative change or action.
    </div>
    <form class="form-horizontal">
        <div class="form-group">
            <label for="" class="col-md-2 control-label">Account ID</label>
            <div class="col-md-10">
                <input type="text" class="form-control" name="fetch-accountid" placeholder="Account ID">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-2 control-label">Site ID</label>
            <div class="col-md-10">
                <input type="text" class="form-control" name="fetch-siteid" placeholder="Site ID">
            </div>
        </div>
    </form>
</script>

<script id="modal-artifact-template" type="text/x-handlebars-template">
    <div id="fetch-modal-alerts" class="alert alert-danger hidden">
      Indicates a neutral informative change or action.
    </div>
    <form class="form-horizontal" method="POST" enctype="multipart/form-data" data-target="<?= base_url('projects/upload_new_project_artifact/'.$this->uri->segment(3)); ?>">
        <div class="form-group">
            <label for="" class="col-md-2 control-label">Name</label>
            <div class="col-md-10">
                <input type="text" class="form-control" name="artifact_name" placeholder="Name">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-2 control-label">Description</label>
            <div class="col-md-10">
                <textarea class="form-control" name="artifact_description"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-2 control-label">Tags</label>
            <div class="col-md-10">
                <textarea class="form-control" name="artifact_tags"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-2 control-label">File</label>
            <div class="col-md-10">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <span class="btn btn-default btn-sm btn-file">
                        <span class="fileinput-new">Select file</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" class="" data-parsley-required="" name="artifact_file" accept="">
                    </span>
                    <span class="fileinput-filename"></span>
                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none; font-size: 13px;"><i class="fa fa-remove"></i></a>
                </div>
            </div>
        </div>
    </form>
</script>


<script id="modal-files-uploader-template" type="text/x-handlebars-template">
    <div id="fetch-modal-alerts" class="alert alert-danger hidden">
      Indicates a neutral informative change or action.
    </div>
    <form class="form-horizontal" method="POST" enctype="multipart/form-data" data-target="<?= base_url('projects/upload_artifact_files/'.$this->uri->segment(3)); ?>">
        
        <div class="form-group">
            <label for="" class="col-md-3 control-label">Artifact Name</label>
            <div class="col-md-9">
                <label class="artifact_name"></label>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 control-label">Latest Version</label>
            <div class="col-md-9">
                <label class="latest_version"></label>
            </div>
        </div>

        <div class="form-group">
            <label for="" class="col-md-3 control-label">File</label>
            <div class="col-md-9">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <span class="btn btn-default btn-sm btn-file">
                        <span class="fileinput-new">Select file</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" class="" data-parsley-required="" name="artifact_file" accept="">
                    </span>
                    <span class="fileinput-filename"></span>
                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none; font-size: 13px;"><i class="fa fa-remove"></i></a>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 control-label">Notes</label>
            <div class="col-md-9">
                <textarea class="form-control" name="file_description"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 control-label">Tags</label>
            <div class="col-md-9">
                <textarea class="form-control" name="file_tags"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 control-label">Update Version</label>
            <div class="col-md-9">
                <select class="form-control" name="update_version" >
                    <option>Major</option>
                    <option>Minor</option>
                    <option>Patch</option>
                </select>
            </div>
            <div class="hidden">
                <div class="col-md-3">
                    <input type="number" class="form-control" min="1" step="1" name="file_major_version" placeholder="Major">
                </div>
                <div class="col-md-3">
                    <input type="number" class="form-control" min="1" step="1" name="file_minor_version" placeholder="Minor">
                </div>
                <div class="col-md-3">
                    <input type="number" class="form-control" min="1" step="1" name="file_patch_version" placeholder="Patch">
                </div>
            </div>
        </div>

    </form>
</script>