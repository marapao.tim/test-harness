<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title">
            <?= ($this->uri->segment(3)) ? 'Edit ' : 'Add '; ?>Project Form
            <div class="pull-right">
                <button class="btn btn-box-tool btn-xs btn-link" rel="tooltip" title="Collapse" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <!-- form start -->
                <?php echo form_open_multipart('projects/save_project/'.$this->uri->segment(3), array('role' => 'form', 'data-parsley-validate' => 'true', 'class' => 'form-horizontal')); 
                
                    if(isset($_GET['redirect'])) {
                        ?>
                            <input type="hidden" name="redirect" value="<?= $_GET['redirect']; ?>">
                        <?php
                    }
                ?>
                  
                    <div class="form-group">
                        <label for="" class="col-md-2 control-label"><h5>Project Details</h5></label>
                    </div>

                    <div class="form-group">
                        <label for="project_title" class="control-label col-md-2">Title</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="project_title" id="project_title" placeholder="Project Title" value="<?php echo $project->project_title; ?>" data-parsley-required>
                        </div>
                    </div>                          

                    <div class="form-group">
                        <label for="project_name" class="control-label col-md-2">Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="project_name" id="project_name" placeholder="Project Name" value="<?php echo $project->project_name; ?>" 
                            data-parsley-required
                            data-parsley-projectnameexists="<?php echo base_url('projects/project_name_exists/'.$this->uri->segment(3)); ?>"
                            readonly>
                        </div>
                    </div>  

                    <div class="form-group">
                        <label for="client_name" class="control-label col-md-2">Client</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="client_name" id="client_name" placeholder="Client Name" value="<?php echo $project->client_name; ?>" data-parsley-required>
                        </div>
                    </div>                          

                    <div class="form-group">
                        <label for="project_description" class="control-label col-md-2">Description</label>
                        <div class="col-md-8">
                            <textarea class="form-control" name="project_description" id="project_description" placeholder="Project Description"><?php echo $project->project_description; ?></textarea>
                        </div>
                    </div>  

                    <div class="form-group">
                        <div class="col-md-2 col-md-offset-8">
                            <button type="submit" class="btn btn-primary btn-sm pull-right">Save</button>
                        </div>
                    </div>
                
                </form>
            </div>
        </div>
    </div><!-- /.panel-body -->
</div>