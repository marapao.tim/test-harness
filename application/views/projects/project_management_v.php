<div class="row">
    <div class="col-md-3 col-md-offset-7">
        <div class="input-group stylish-input-group input-append">
            <input type="text" class="form-control search-project" placeholder="Search" >
            <span class="input-group-addon">
                <button type="submit">
                    <span class="glyphicon glyphicon-search"></span>
                </button>  
            </span>
        </div>
    </div>

    <div class="col-md-2">
        <a class="btn btn-primary btn-default" href="<?php echo base_url('projects/save_project'); ?>"><i class="ion-plus"></i> New Project</a>
    </div>
</div>
<hr>
<div class="row projects-container">
    <?php 
        foreach($projects as $project) {
            ?>
                <div class="col-md-4 project-container">
                    <div class="panel panel-default project">
                        <div class="panel-body text-center">
                            <a href="<?= base_url('projects/view_project/'.$project->project_id); ?>" class="btn btn-default btn-xs btn-link" rel="tooltip" title="View">
                                <div class="project-icon">
                                    <img src="<?= ($project->project_logo != '' && $project->project_logo != NULL) ? base_url('store/projects/'.$project->project_name.'/images/150x150/'.$project->project_logo) : base_url('assets/img/default-app.png'); ?>" alt="<?= $project->project_title; ?>" class="img-thumbnail">
                                </div>
                                <span style="font-size: 14px;">
                                    <?= $project->project_title; ?>
                                </span>
                            </a>
                        </div>

                        <div class="panel-footer project-footer">
                            <div class="row">
                                <div class="col-md-6">
                                    <small>
                                        <?= $project->client_name; ?>
                                    </small>
                                    <br>
                                    <span class="project-status">
                                        <a class="project-status-circle <?= str_replace(" ","-",strtolower($project->project_status)); ?>" href="javascript:;"></a>
                                        (<?= $project->project_status; ?>)
                                    </span>
                                </div>
                                <div class="col-md-6">
                                    <div class="project-actions">
                                        <a href="<?= base_url('projects/view_project/'.$project->project_id); ?>" class="btn btn-default btn-xs btn-link" rel="tooltip" title="View">
                                            <i class="fa fa-search"></i>
                                        </a>                                              

                                        <!--<a href="<?= base_url('projects/project_tasks_management/'.$project->project_id); ?>" class="btn btn-default btn-xs btn-link" rel="tooltip" title="Tasks">
                                            <i class="fa fa-bar-chart"></i>
                                        </a> -->
                                        <!--
                                        <a href="<?= base_url('projects/download_project/'.$project->project_id); ?>" target="_blank" class="btn btn-default btn-xs btn-link" rel="tooltip" title="Download Package">
                                            <i class="fa fa-download"></i>
                                        </a> 
                                        -->
                                        <?php if($this->user_m->checkPermissions("delete-project")){ ?>
                                        <a href="<?= base_url('projects/delete_project/'.$project->project_id); ?>" class="btn btn-default btn-xs btn-link action-modal-trigger" data-action-modal-message="Are you sure you want to delete this item?" rel="tooltip" title="Delete">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
        }
    ?>
</div>
