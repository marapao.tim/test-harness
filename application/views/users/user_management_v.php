<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title">
            Users Table
            <div class="pull-right">
                <a class="btn btn-sm btn-primary btn-default" href="<?php echo base_url('users/save_user'); ?>"><i class="ion-plus"></i> Add User</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->
    <div class="panel-body">
        <table class="table table-hover datatable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Username</th>
                    <th>User Role</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    foreach ($users as $user) {
                        ?>
                            <tr>
                                <td><?php echo $user->user_id; ?></td>
                                <td><?php echo $user->user_name; ?></td>
                                <td><?php echo $user->user_role_title; ?></td>
                                <td><?php echo $user->user_fullname; ?></td>
                                <td><?php echo $user->user_email; ?></td>
                                <td class="text-center">
                                    <a href="<?php echo base_url('users/save_user/'.$user->user_id); ?>" rel="tooltip" title="Edit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></a>
                                    <?= ($this->session->userdata('user_id') == $user->user_id) ? '' : '<a href="'.base_url('users/delete_user/'.$user->user_id).'" rel="tooltip" title="Delete" class="btn btn-default btn-xs action-modal-trigger" data-action-modal-message="Are you sure you want to delete this item?"><i class="fa fa-remove"></i></a>'?>
                                    <a href="<?php echo base_url('users/reset_password/'.$user->user_id); ?>" rel="tooltip" title="Reset Password" class="btn btn-default btn-xs"><i class="fa fa-unlock"></i></a>
                                </td>
                            </tr>
                        <?php
                    }
                ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->