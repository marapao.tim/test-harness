<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title">
            <?= ($this->uri->segment(3)) ? 'Edit ' : 'Add '; ?>User Form
            <div class="pull-right">
                <button class="btn btn-box-tool btn-xs btn-link" rel="tooltip" title="Collapse" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <div class="panel-body">
        <div class="row">

            <!-- form start -->
            <?php echo form_open('users/save_user/'.$this->uri->segment(3), array('role' => 'form', 'data-parsley-validate' => 'true', 'class' => 'form-horizontal')); ?>

                <div class="col-md-5">
                    <div class="form-group">
                        <label for="user_fullname" class="control-label col-md-3">Full Name</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="user_fullname" id="user_fullname" placeholder="Full Name" value="<?php echo $user->user_fullname; ?>" 
                            data-parsley-required 
                            >
                        </div>
                    </div>    

                    <div class="form-group">
                        <label for="user_name" class="control-label col-md-3">Username</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="user_name" id="user_name" placeholder="User Name" value="<?php echo $user->user_name; ?>" 
                            data-parsley-type="alphanum"
                            data-parsley-minlength="6"
                            data-parsley-usernameexists="<?php echo base_url('users/username_exists/'.$this->uri->segment(3)); ?>"
                            data-parsley-required>
                        </div>
                    </div>        

                    <?php
                        if(!$this->uri->segment(3)) {
                    ?>
                    <div class="form-group">
                        <label for="user_role_id" class="control-label col-md-3">User Role</label>
                        <div class="col-md-9">
                            <select name="user_role_id" id="user_role_id" class="form-control" data-parsley-required>
                                <option value="">--Select--</option>
                                <?php 
                                    foreach ($user_roles as $key => $value) {
                                        ?>
                                            <option value="<?= $value->user_role_id; ?>"><?= $value->user_role_title; ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div> 
                    <?php
                        }
                    ?>
                </div>

                <div class="col-md-6">
                    <?php 
                        if(!$this->uri->segment(3)) {
                            ?>
                                <div class="form-group">
                                    <label for="user_password" class="control-label col-md-3">Password</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="user_password" id="user_password" placeholder="Password" value="" 
                                    data-parsley-minlength="6"
                                    data-parsley-required>
                                    </div>
                                </div>                                              

                                <div class="form-group">
                                    <label for="user_cpassword" class="control-label col-md-3">Confirm Password</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="user_password" id="user_cpassword" placeholder="Confirm Password" value="" 
                                        data-parsley-equalto="#user_password"
                                        data-parsley-required>
                                    </div>
                                </div>        
                            <?php
                        }
                    ?> 
                    
                    <div class="form-group">
                        <label for="user_email" class="control-label col-md-3">Email</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="user_email" id="user_email" placeholder="Email" value="<?php echo $user->user_email; ?>" 
                            data-parsley-type="email"
                            data-parsley-emailexists="<?php echo base_url('users/email_exists/'.$this->uri->segment(3)); ?>"
                            data-parsley-required>
                        </div>
                    </div> 
                    <?php
                        if($this->uri->segment(3)) {
                    ?>
                    <div class="form-group">
                        <label for="user_role_id" class="control-label col-md-3">User Role</label>
                        <div class="col-md-9">
                            <select name="user_role_id" id="user_role_id" class="form-control" data-parsley-required>
                                <option value="">--Select--</option>
                                <?php 
                                    foreach ($user_roles as $key => $value) {
                                        $isSelected = "";
                                        if($value->user_role_id == $user->user_role_id) {
                                            $isSelected = "selected='selected'";
                                        }
                                        ?>
                                            <option <?= $isSelected ?> value="<?= $value->user_role_id; ?>"><?= $value->user_role_title; ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div> 
                    <?php
                        }
                    ?>
                </div>   

                <div class="col-md-6 col-md-offset-5">
                    <button type="submit" class="btn btn-primary pull-right btn-sm">Save</button>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->