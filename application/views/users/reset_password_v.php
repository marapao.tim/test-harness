<div class="col-xs-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
                Reset Password Form
                <div class="pull-right">
                    <button class="btn btn-box-tool btn-xs btn-link" rel="tooltip" title="Collapse" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div><!-- /.box-header -->

        <div class="panel-body">
            <div class="row">

                <!-- form start -->
                <?php echo form_open('users/reset_password/'.$this->uri->segment(3), array('role' => 'form', 'data-parsley-validate' => 'true', 'class' => 'form-horizontal')); ?>

                        <div class="form-group">
                            <label for="user_password" class="control-label col-md-3">Password</label>
                            <div class="col-md-5">
                                <input type="password" class="form-control" name="user_password" id="user_password" placeholder="Password" value="" 
                            data-parsley-minlength="6"
                            data-parsley-required>
                            </div>
                        </div>                                              

                        <div class="form-group">
                            <label for="user_cpassword" class="control-label col-md-3">Confirm Password</label>
                            <div class="col-md-5">
                                <input type="password" class="form-control" name="user_password" id="user_cpassword" placeholder="Confirm Password" value="" 
                                data-parsley-equalto="#user_password"
                                data-parsley-required>
                            </div>
                        </div>     

                        <div class="row">
                            <div class="col-md-5 col-md-offset-3">
                                <button type="submit" class="btn btn-primary btn-sm">Save</button>
                            </div>
                        </div>
                <?php echo form_close(); ?>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col -->