<div class="store-topbar-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-7">
				<ul class="list-inline store-topbar">
					<li class="active"><a href="#">Installed</a></li>
					<li><a href="#">Tools</a></li>
					<li><a href="#">Themes</a></li>
					<li><a href="#">Plugins</a></li>
				</ul>
			</div>
			<div class="col-md-5">
				<form class="search-form">
					<input type="text" class="search-input input-block" placeholder="Search..." name="search_query">
				</form>
			</div>
		</div>
	</div>
</div>

<div class="store-wrapper">
	<div class="container-fluid">
		<h4 class="page-header clearfix">
			Installed on my CMS
			<!-- Button trigger modal -->
			<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
			  Launch demo modal
			</button>
			<a href="#" class="text-muted pull-right md-trigger md-setperspective" data-modal="modal-1"><small>Show All</small></a>
		</h4>

		<div class="row"> 
			<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 scroll-reveal">
				<div class="store-item-container">
					<div class="store-item-image">
						<div class="image-center-wrapper" style="width: 100px; height: 100px;">
							<img class="" src="<?= base_url('assets/img/default-plugin.png'); ?>" alt="Default">
						</div>
					</div>

					<div class="store-item-details">
						<a href="#" rel="tooltip" title="sdfdsfds" class="store-item-title"><strong>APP TITLE sdfdsfdsfsdfdsfdsfsdfsdfsdfsdfsdf</strong></a>
						<a href="#" rel="tooltip" title="sdfdsfdsf" class="store-item-title" style="font-size: 12px;"><i><span style="color: #333; ">By</span> Category</i></a>
						<div class="store-item-description overflow-ellipsis">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem quos labore in ipsum repudiandae id, sapiente harum asperiores illum! At iusto accusantium delectus porro, et dicta animi doloribus quisquam magnam. dlkfjdfjdsfjdsfjsdfjsdfkjsdlfjsdlfjsdlfkjdfjsdlfjksdf
						</div>
					</div>
					<div class="store-item-footer clearfix">
						<span class="text-muted">Ver. 1.00</span>
						<!-- <a href="#" rel="tooltip" class="btn btn-sm btn-link" title="Installed and Updated"><i class="fa fa-check text-success"></i></a> -->
						<a href="#" rel="tooltip" class="btn btn-sm btn-link pull-right" title="Uninstall"><i class="fa fa-remove text-danger"></i></a>
						<a href="#" rel="tooltip" class="btn btn-sm btn-link pull-right" title="Update to version 2.0"><i class="fa fa-refresh text-info"></i></a>
					</div>
				</div>
			</div>				

			<div class="col-md-12 scroll-reveal">
				<hr>
				<button class="btn btn-primary center-block">Load More</button>
				<hr>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">A Very Nice Plugin by <a href="#"><small>Kel Sarabia</small></a></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3">
						<div class="store-modal-item-info">
							<div class="store-item-image">
								<div class="image-center-wrapper" style="width: 100px; height: 100px;">
									<img class="" src="<?= base_url('assets/img/default-plugin.png'); ?>" alt="Default">
								</div>
							</div>

							<br>
					        <table class="table table-condensed"> 
					            <tr>
					                <td colspan="2">
				                        <strong>Description</strong> <br>
				                        <p class="text-justify">
				                        	<?= $plugin->description; ?>
				                        </p>
					                </td>
					            </tr>   
					            <tr>
					                <td colspan="2">
					                	<strong><i class="fa fa-tag"></i> Category</strong> <br>
					                    <?= $plugin->category; ?>
					                </td>
					            </tr>                       
					            <tr>
					                <td colspan="2">
					                	<strong><i class="fa fa-calendar"></i> Submitted</strong> <br>
					                    <abbr class="timeago" rel="tooltip" title="<?= date($this->master_config['date_time_format'], strtotime($plugin->date_created)); ?>"></abbr>
					                </td>
					            </tr>        

					            <tr>
					                <td colspan="2">
					                	<strong><i class="fa fa-calendar"></i> Updated</strong> <br>
					                    <abbr class="timeago" rel="tooltip" title="<?= date($this->master_config['date_time_format'], strtotime($plugin->date_modified)); ?>"></abbr>
					                </td>
					            </tr> 
					        </table>
						</div>
					</div>
					<div class="col-md-9">
				        <div role="tabpanel">
				            <!-- Nav tabs -->
				            <ul class="nav nav-tabs nav-paper" role="tablist">                         
				                <li role="presentation" class="active">
				                    <a href="#documentation" aria-controls="documentation" role="tab" data-toggle="tab">Documentation</a>
				                </li>
				                <li role="presentation">
				                    <a href="#screenshots" aria-controls="tab" role="tab" data-toggle="tab">Screenshots</a>
				                </li>            
				                <li role="presentation">
				                    <a href="#versions" aria-controls="tab" role="tab" data-toggle="tab">Versions &middot; Releases</a>
				                </li>
				            </ul>
				        </div>

			            <!-- Tab panes -->
			            <div class="tab-content">
			                <div role="tabpanel" class="tab-pane active" id="documentation">
			                    <div class="panel panel-default">
			                        <div class="panel-body">
			                            Docu here
			                        </div>
			                    </div>
			                </div>                

			                <div role="tabpanel" class="tab-pane" id="screenshots">
			                    <div class="panel panel-default">
			                        <div class="panel-body">
			                            Under Construction
			                        </div>
			                    </div>
			                </div>            

			                <div role="tabpanel" class="tab-pane" id="versions">
			                    <div class="panel panel-default">
			                        <div class="panel-body">
			                            
			                            <ul class="timeline">
			                                <?php 
			                                	$versions = unserialize($plugin->version_metas);
			                                    sort($versions, SORT_NUMERIC);

			                                    if(count($versions)) {
			                                        $counter = 0;
			                                        foreach ($versions as $key => $version) {

			                                            ?>
			                                                <li class="timeline-inverted">
			                                                    <div class="timeline-badge <?= ($version['version'] == $plugin->current_version) ? 'success' : ''; ?>"><i class="fa fa-check"></i>
			                                                    </div>
			                                                    <div class="timeline-panel">
			                                                        <div class="timeline-heading">
			                                                            <h4 class="timeline-title">Version <?= number_format($version['version'], 2); ?><small class="pull-right"><a href="<?= base_url('plugins/'.$plugin->plugin_id.'/versions/'.$version['version'].'/'.$version['filename']); ?>" download="<?= $plugin->name.'v'.$version['version'].'.zip'; ?>"><i class="fa fa-download"></i></a></small>
			                                                                <div class="clearfix"></div>
			                                                            </h4>
			                                                            <p><small class="text-muted"><i class="fa fa-clock-o"></i> <abbr class="timeago" rel="tooltip" title="<?= date($this->master_config['date_time_format'], strtotime($version['release_date'])); ?>"></abbr> &middot; <i class="fa fa-user"></i> <?= $this->user_m->get($version['updated_by'])->user_fullname; ?></small>
			                                                            </p>
			                                                        </div>
			                                                        <div class="timeline-body">
			                                                            <p>
			                                                                <?= $version['remarks']; ?>
			                                                            </p>
			                                                        </div>
			                                                    </div>
			                                                </li>
			                                            <?php
			                                            $counter++;
			                                        }
			                                    }
			                                ?>
			                            </ul>

			                        </div>
			                    </div>
			                </div>
			            </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary btn-sm">Install</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->