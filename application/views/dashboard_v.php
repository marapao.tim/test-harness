<!-- /.row -->
<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary dashboard-widget">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="ion-easel ion-lg"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <div class="huge">
                            <?= count($this->project_m->get()); ?>
                        </div>
                        <div>Test Scripts</div>
                    </div>
                </div>
            </div>
            <a href="<?= base_url('projects/project_management'); ?>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    
</div>
<!-- /.row -->
