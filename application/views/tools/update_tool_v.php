<div class="panel panel-default master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            Update Tool Form
            <div class="pull-right">
                <button class="btn btn-box-tool btn-xs btn-link" rel="tooltip" title="Collapse" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <input type="hidden" value="<?= ($this->uri->segment(3)) ? 'edit' : 'add'; ?>" data-tool-id="<?= $this->uri->segment(3); ?>" id="form-mode">

    <div class="panel-body">
        <!-- form start -->
        <?php echo form_open_multipart('tools/update_tool/'.$this->uri->segment(3), array('role' => 'form', 'data-parsley-validate' => 'true', 'class' => 'form-horizontal', 'id' => 'update-tool-form')); ?>

            <div class="form-group">
                <label for="" class="control-label label-left col-md-2">Current Version</label>
                <div class="col-md-3">
                    <p class="form-control-static">
                        <?= $tool->tool_version; ?>
                    </p>
                </div>
            </div>

            <div class="form-group">
                <label for="version" class="control-label label-left col-md-2">New Version</label>
                <div class="col-md-2">
                    <input type="text" class="form-control" name="tool_version" value="">
                </div>
            </div>

            <div class="form-group">
                <label for="tool_file" class="control-label label-left col-md-2">ZIP Package</label>
                <div class="col-md-10" id="theme_file_form_group">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-default btn-sm btn-file">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" class="" name="tool_file" accept=".zip">
                        </span>
                        <span class="fileinput-filename"></span>
                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none; font-size: 13px;"><i class="fa fa-remove"></i></a>
                    </div>                   
                </div>
            </div>  

            <div class="form-group">
                <label for="cms_support" class="control-label label-left col-md-2">CMS Support</label>
                <div class="col-md-4">
                    <select name="cms_support[]" id="cms_support" class="form-control" multiple>
                        <?php 
                            foreach ($cms as $key => $value) {
                                ?>
                                    <option value="<?= $value->version; ?>"><?= $value->codename.' '.$value->version; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </div>
            </div>   

            <div class="form-group">
                <label for="version_remarks" class="control-label label-left col-md-2">Version Remarks</label>
                <div class="col-md-10">
                    <textarea name="version_remarks" class="form-control" id="version_remarks" rows="3"></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-2">
                    <?php 
                        if(isset($_GET['redirect'])) {
                            ?>
                                <a href="<?= $_GET['redirect']; ?>" class="btn btn-default btn-sm">Back</a>
                            <?php
                        } else {
                            ?>
                                <a href="<?= base_url('tools/tools_management/'); ?>" class="btn btn-default btn-sm">Back</a>
                            <?php
                        }
                    ?>
                    
                    <button type="submit" class="btn btn-primary btn-sm update-tool-btn">Update</button>
                </div>
            </div>
        <?php echo form_close(); ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->