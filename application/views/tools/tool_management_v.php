<div class="panel master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            Tools Table
            <div class="pull-right">
                <a class="btn btn-sm btn-primary btn-default" href="<?php echo base_url('tools/save_tool'); ?>"><i class="ion-plus"></i> Add Tool</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->
    <div class="panel-body">
        <table class="table table-hover datatable" id="tools-table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Type</th>
                    <th>Category</th>
                    <th>Current Version</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->

<script id="tool-row-template" type="x-tmpl-mustache">
    <tr>
        <td>{{tool_title}}</td>
        <td>{{tool_type}}</td>
        <td>{{tool_category}}</td>
        <td>{{tool_version}}</td>
        <td class="text-center">
            <a href="<?php echo base_url('tools/view_tool/{{tool_id}}'); ?>" rel="tooltip" title="View" class="btn btn-default btn-xs"><i class="fa fa-search"></i></a>
            <a href="<?php echo base_url('tools/save_tool/{{tool_id}}'); ?>" rel="tooltip" title="Edit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></a>
            <a href="<?php echo base_url('tools/update_tool/{{tool_id}}'); ?>" rel="tooltip" title="Update" class="btn btn-default btn-xs"><i class="fa fa-upload"></i></a>
            <a href="<?php echo base_url('tools/delete_tool/{{tool_id}}'); ?>" rel="tooltip" title="Delete" data-tool-id="{{tool_id}}" class="btn btn-default btn-xs delete-tool-btn" data-action-modal-message="Are you sure?"><i class="fa fa-remove"></i></a>
        </td>
    </tr>
</script>