<div class="panel panel-default master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            <?= ($this->uri->segment(3)) ? 'Edit ' : 'Add '; ?>Tool Form
            <div class="pull-right">
                <button class="btn btn-box-tool btn-xs btn-link" rel="tooltip" title="Collapse" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <input type="hidden" value="<?= ($this->uri->segment(3)) ? 'edit' : 'add'; ?>" data-tool-id="<?= $this->uri->segment(3); ?>" id="form-mode">

    <!-- form start -->
    <?php echo form_open_multipart('tools/save_tool/'.$this->uri->segment(3), array('role' => 'form', 'class' => 'form-horizontal', 'id' => 'save-tool-form')); 
    ?>

        <div class="panel-body">
            <div class="form-group">
                <label for="tool_title" class="control-label label-left col-md-2">Title</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="tool_title" id="tool_title" placeholder="" value="">
                </div>
            </div>                
            
            <div class="form-group">
                <label for="tool_type" class="control-label label-left col-md-2">Type</label>
                <div class="col-md-4">
                    <select name="tool_type" id="tool_type" class="form-control">
                        <option value="">--Select--</option>
                        <?php 
                            $tool_types = array('Menu', 'Static', 'Dynamic');

                            foreach ($tool_types as $key => $value) {
                                ?>
                                    <option value="<?= $value; ?>"><?= $value; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </div>
            </div>                    

            <div class="form-group">
                <label for="tool_author" class="control-label label-left col-md-2">Author</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="tool_author" id="tool_author" placeholder="" value="">
                </div>
            </div>   

            <div class="form-group">
                <label for="tool_category" class="control-label label-left col-md-2">Category</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="tool_category" id="tool_category" placeholder="" value="" list="tool-categories">
                    <datalist id="tool-categories">
                        <?php 
                            foreach ($categories as $key => $value) {
                                ?>
                                    <option value="<?= $value->tool_category; ?>"><?= $value->tool_category; ?></option>
                                <?php
                            }
                        ?> 
                    </datalist>
                </div>
            </div>              

            <div class="form-group">
                <label for="tool_tags" class="control-label label-left col-md-2">Tags</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="tool_tags" id="tool_tags" placeholder="" value="">
                </div>
            </div>                      

            <div class="form-group">
                <label for="tool_description" class="control-label label-left col-md-2">Description</label>
                <div class="col-md-10">
                    <textarea type="text" class="form-control" name="tool_description" id="tool_description" placeholder="" rows="3"></textarea>
                </div>
            </div>        

            <?php 
                if(!$this->uri->segment(3)) {
                   ?>
                        <hr>
                        <h6>Version Details</h6>

                        <div class="form-group">
                            <label for="tool_file" class="control-label col-md-2 label-left">ZIP Package</label>
                            <div class="col-md-4" id="theme_file_form_group">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <span class="btn btn-default btn-sm btn-file">
                                        <span class="fileinput-new">Select file</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" class="" data-parsley-errors-container="#theme_file_form_group" data-parsley-class-handler="#theme_file_form_group" data-parsley-required name="tool_file" accept=".zip">
                                    </span>
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none; font-size: 13px;"><i class="fa fa-remove"></i></a>
                                </div>                   
                            </div>
                        </div>   

                        <div class="form-group">
                            <label for="tool_version" class="control-label label-left col-md-2">Version</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="tool_version" id="tool_version" placeholder="" value="1" readonly="">
                            </div>
                        </div>   

                        <div class="form-group">
                            <label for="cms_support" class="control-label label-left col-md-2">CMS Support</label>
                            <div class="col-md-4">
                                <select name="cms_support[]" id="cms_support" class="form-control" multiple>
                                    <?php 
                                        foreach ($cms as $key => $value) {
                                            ?>
                                                <option value="<?= $value->version; ?>"><?= $value->codename.' '.$value->version; ?></option>
                                            <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>   

                        <div class="form-group">
                            <label for="version_remarks" class="control-label label-left col-md-2">Version Remarks</label>
                            <div class="col-md-10">
                                <textarea type="text" class="form-control" name="version_remarks" id="version_remarks" placeholder="" rows="3"></textarea>
                            </div>
                        </div>        
                   <?php 
                }
            ?>
        </div>

        <div class="panel-footer">
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <?php 
                        if(isset($_GET['redirect'])) {
                            ?>
                                <a href="<?= $_GET['redirect']; ?>" class="btn btn-default btn-sm">Back</a>
                            <?php
                        } else {
                            ?>
                                <a href="<?= base_url('tools/tools_management/'); ?>" class="btn btn-default btn-sm">Back</a>
                            <?php
                        }
                    ?>
                    <button class="btn btn-primary btn-sm" id="save-tool-btn">Save</button>
                </div>
            </div>
        </div>

    <?php echo form_close(); ?>

</div>