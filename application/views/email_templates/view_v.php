<div class="panel master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            Email Templates Table
            <div class="pull-right">
                <a class="btn btn-sm btn-primary btn-default" href="<?php echo base_url('email_templates/add'); ?>"><i class="ion-plus"></i> Add Template</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->
    <div class="panel-body">
        <table class="table table-hover datatable" id="tools-table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Module</th>
                    <th>Description</th>
                    <th>Tags</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach($templates as $key=>$template){
                ?>
                    <tr>
                        <td><?=$template->template_name;?></td>
                        <td><?=$template->template_module;?></td>
                        <td><?=$template->template_description;?></td>
                        <td><?=$template->template_tags;?></td>
                        <td class="text-center">
                            <a href="<?php echo base_url('email_templates/edit/'.$template->template_id); ?>" rel="tooltip" title="Update" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>
                            <a href="javascript:;" rel="tooltip" title="Delete" data-template-id="<?=$template->template_id;?>" class="btn btn-default btn-xs delete-template-btn" data-action-modal-message="Are you sure?"><i class="fa fa-remove"></i></a>
                        </td>
                    </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->
