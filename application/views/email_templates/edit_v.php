<div class="panel panel-default master-cms-panel">
    <div class="panel-heading">
        <div class="panel-title">
            Edit Template Form
            <div class="pull-right">
                <button class="btn btn-box-tool btn-xs btn-link" rel="tooltip" title="Collapse" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-header -->

    <input type="hidden" value="<?= ($this->uri->segment(3)) ? 'edit' : 'add'; ?>" data-tool-id="<?= $this->uri->segment(3); ?>" id="form-mode">

    <!-- form start -->
    <?php echo form_open_multipart('email_templates/update_template/'.$this->uri->segment(3), array('role' => 'form', 'class' => 'form-horizontal', 'id' => 'save-template-form')); 
    ?>

        <div class="panel-body">
            <div class="form-group">
                <label for="tool_title" class="control-label label-left col-md-2">Name</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="template_name" id="template_name" placeholder="" value="<?=$template->template_name;?>">
                </div>
            </div>      



            <div class="form-group">
                <label for="tool_description" class="control-label label-left col-md-2">Description</label>
                <div class="col-md-8">
                    <textarea rows="5" type="text" class="form-control" name="template_description" id="template_description" placeholder="" rows="3"><?=$template->template_description;?></textarea>
                </div>
            </div>       

            <div class="form-group">
                <label for="tool_category" class="control-label label-left col-md-2">Module</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="template_module" id="template_module" placeholder="" value="<?=$template->template_module;?>" list="template-categories">
                    <datalist id="template-categories">
                        <?php 
                            foreach ($modules as $key => $value) {
                                $selected = "";
                                if($value->template_module == $template->template_module){
                                    $selected = "selected";
                                }
                                ?>
                                    <option value="<?= $value->template_module; ?>" <?=$selected;?>><?= $value->template_module; ?></option>
                                <?php
                            }
                        ?> 
                    </datalist>
                </div>
            </div>              

            <div class="form-group">
                <label for="tool_tags" class="control-label label-left col-md-2">Tags</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="template_tags" id="template_tags" placeholder="" value="<?=$template->template_tags;?>">
                </div>
            </div>     


            <div class="form-group">
                <label for="tool_description" class="control-label label-left col-md-2">
                    Body <a class="preview-template" href="javascript:;"><i class="fa fa-eye"></i></a>
                </label>
                <div class="col-md-8">
                    <textarea rows="15" type="text" class="form-control" name="template_body" id="template_body" placeholder="" rows="3"><?=$template->template_body;?></textarea>
                </div>
            </div>    

        </div>

        <div class="panel-footer">
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <a href="<?= base_url('email_templates'); ?>" class="btn btn-default btn-sm">Back</a>
                    
                    <button class="btn btn-primary btn-sm" id="save-tool-btn">Save</button>
                </div>
            </div>
        </div>

    <?php echo form_close(); ?>

</div>