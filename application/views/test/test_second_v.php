<div class="row">
	<h4 class="page-header"><i class="ion-settings"></i> Test Scripts                    <small><a class="btn btn-primary btn-sm pull-right" href="http://localhost/TestHarness/test_scripts"><i class="ion-ios-arrow-thin-left"></i> Back to Test Scripts List</a></small>
                    <div class="clearfix"></div>
                </h4>
<!-- 	<div class="col-md-12 text-center" style="background: #455669;padding: 10px 0px;margin-bottom:20px">
		<img src="<?= base_url('assets/img/srn_logo.png'); ?>">
	</div>
	<h3 class="text-center">Hello World</h3>
	<div class="col-md-4"> 
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> 
	</div>
	<div class="col-md-4">
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> 
	</div>
	<div class="col-md-4">
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> 
	</div> 
	<div class="col-md-12">
		<div class="panel">
			<div class="panel-heading">
				<div class="panel-title">
					Test Table
					<div class="pull-right">
	                	<a class="btn btn-sm btn-primary btn-default" href="#"><i class="ion-plus"></i> Add User</a>
	            	</div> 
				</div>
			</div>
			<div class="panel-body">
				<table class="table table-hover test-table">
		            <thead>
		                <tr>
		                    <th></th>
		                    <th>ID</th>
		                    <th>FULL NAME</th>
		                    <th>AGE</th>
		                    <th>GENDER</th> 
		                    <th class="text-center">Actions</th>
		                    <th></th>
		                </tr>
		            </thead>
		            <tbody> 
		            </tbody>
		        </table> 

		        <nav aria-label="..." class="test-paginate text-center">
				  <ul class="pagination"> 
				  </ul>
				</nav>

			</div> 
		</div>
	</div>  -->
	
			<div class="panel panel-body">
	<div class="col-md-12 row script-text-test">
		<div class="col-md-4 row" style="margin-bottom: 10px;">
			<form>
			    <div class="input-group">
			      <input id="txtsearch" type="text" class="form-control" name="email" placeholder="Search">
			      <span class="input-group-addon"><i class="fa fa-search"></i></span>
			    </div>
			</form> 
		</div>
		<div class="col-md-8 total-count" style="padding-top:15px">
			<label></label>
		</div><table class="table table-hover table-bordered table-striped test-table">
		            <thead>
		                <tr style="background: #2C3E50;color: #fff">
		                    <th>ID</th>
		                    <th>FULL NAME</th>
		                    <th>AGE</th>
		                    <th>GENDER</th> 
		                    <th class="text-center">Actions</th> 
		                </tr>

		            <div class="loader" style="">
		            	<h6><i class="fa fa-spinner fa-spin"></i> Processing</h6>
		            </div>
		            </thead>
		            <tbody> 
		            </tbody>
		        </table> 

		        <nav aria-label="..." class="test-paginate text-left">
				  <ul class="pagination"> 
				  </ul>
				</nav>
				</div>	
<!-- 		<div class="panel panel-default master-cms-panel" style="margin-bottom: 1px !important;">
		    <div class="panel-heading">
		        <div class="panel-title">
		            Test API Script:  
		            
		            <div class="pull-right">
		                <button class="btn btn-primary btn-sm" id="run-script-btn" >Run</button>
		            </div>
		            <div class="clearfix"></div>
		        </div>
		    </div>
		</div>  -->
			<!-- <textarea rows="25" type="text" class="form-control" name="script" id="template_body" placeholder="" ></textarea> -->  
	</div>

	<!-- <div class="output ui-widget-content">
		<span>Output:</span>
		<div class="pull-right">
			<a data-toggle="collapse" href="#result_panel" title="minimize" aria-expanded="true" class=""><i style="color:#fff" class="fa fa-minus-square-o"></i></a> 
			<a data-toggle="collapse" href="#result_panel" title="maximize" aria-expanded="true" class=""><i style="color:#fff" class="fa fa-square-o"></i></a>
        </div>
		<br>
 		<textarea rows="25" type="text" class="form-control" name="script" id="test_result" placeholder="" ></textarea>
 	</div>  -->
</div> 