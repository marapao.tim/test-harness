<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index() {
		//add log
		//$this->logs_m->save_log($this->session->userdata('user_id'), 'Logout', NULL);
		$this->session->sess_destroy();
		redirect('login');
	}
}