<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);
	}

	public function index() {
		$this->user_management();
	}

	/*********************************************/ 

	#TOOLS MANAGEMENT

	/*********************************************/

	public function user_management() {
		$page_data['title'] = 'User Management';
		$page_data['subview'] = 'users/user_management_v';
		$page_data['page_header_data'] = array('title' => 'User Management', 'description' => 'You can manage your Serino Master CMS Users here.');
		$page_data['users'] = $this->user_m->getUsers();
		$page_data['styles'] = array('plugins/datatables/datatables.min');
		$page_data['scripts'] = array('plugins/datatables/datatables.min','custom/user.management');
		$this->load->view('layouts/main_layout', $page_data);
	}

	public function save_user($user_id = NULL) {
		$this->load->model('user_role_m');
		if(empty($_POST)) {
			$page_data['title'] = ($user_id) ? 'Edit User' : 'Add User';
			$page_data['subview'] = 'users/save_user_v';
			$page_data['page_header_data'] = array('description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('users/user_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to Users Listings</a>');
			$page_data['scripts'] = array('plugins/parsley/parsley.config','plugins/parsley/parsley.min','custom/save.user');
			$page_data['user_roles'] = $this->user_role_m->get();

			if($user_id) {
				$page_data['page_header_data']['title'] = 'Edit User';
				$page_data['user'] = $this->user_m->get($user_id);
			} else {
				$page_data['page_header_data']['title'] = 'Add User';
				
				$page_data['user'] = $this->user_m->get_new();
			}

			$this->load->view('layouts/main_layout', $page_data);
		} else {
			$this->user_m->save_user($user_id);
		}
	}

    public function reset_password($user_id) {
        if(empty($_POST)) {
            $page_data['title'] = 'Reset Password';
            $page_data['subview'] = 'users/reset_password_v';
            $page_data['page_header_data'] = array('title' => 'Reset Password', 'description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('users/user_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to Users Listings</a>');
            $page_data['scripts'] = array('plugins/parsley/parsley.config','plugins/parsley/parsley.min','custom/reset.password');
            $page_data['user'] = $this->user_m->get($user_id);

            $this->load->view('layouts/main_layout', $page_data);
        } else {
            $this->user_m->change_password($user_id);
        }
    }

	public function delete_user($user_id) {
		$this->user_m->delete($user_id);
		$this->session->set_flashdata('alert', 'User successfully deleted.');
		$this->session->set_flashdata('alert_type','success');

		redirect('users/user_management','refresh');
	}

	/*********************************************/ 

	#AJAX CALLS

	/*********************************************/

	function username_exists($id = false) {
		if($id) {
			$this->db->where('user_id != ', $id);
		}

		$query = $this->user_m->get_by(array('user_name' => $this->input->post('user_name')));

		if(count($query)) {
			echo 0;
		} else {
			echo 1;
		}
		die();
	}	

	function email_exists($id = false) {
		if($id) {
			$this->db->where('user_id != ', $id);
		}

		$query = $this->user_m->get_by(array('user_email' => $this->input->post('user_email')));

		if(count($query)) {
			echo 0;
		} else {
			echo 1;
		}
		die();
	}
}