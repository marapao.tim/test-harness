<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Themes extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);

		$this->load->model('user_m');
		$this->load->model('theme_m');
		$this->load->model('theme_meta_m');
	}

	public function index() {
		$this->themes_management();
	}

	/*********************************************/ 

	#THEMES MANAGEMENT

	/*********************************************/

	public function themes_management() {
		$this->user_m->checkPermissions("view-themes",true);
		$page_data['title'] = 'Themes Management';
		$page_data['subview'] = 'themes/theme_management_v';
		$page_data['page_header_data'] = array('title' => '<i class="ion-paintbucket"></i> Themes Management', 'description' => 'You can manage your Serino CMS themes here.');
		//$page_data['themes'] = $this->theme_m->get();
		$page_data['styles'] = array('plugins/datatables/datatables.min');
		$page_data['scripts'] = array('plugins/datatables/datatables.min','custom/theme.functions','custom/theme.management');
		$this->load->view('layouts/main_layout', $page_data);
	}

	public function view_theme($theme_id) {
		//$this->user_m->checkPermissions("view-themes",true);
		$theme = $this->theme_m->get($theme_id);

		if(!$this->user_m->checkIfAdmin($this->session->userdata('user_id'))) {
			if($theme->created_by !== $this->session->userdata('user_id')) {
				$this->session->set_flashdata('alert_type', 'danger');
				$this->session->set_flashdata('alert_message', 'Not Allowed.');
				redirect('themes/themes_management');
			}
		}

		$page_data['title'] = 'View Theme - '.$theme->theme_title;
		$page_data['subview'] = 'themes/view_theme_v';
		$page_data['page_header_data'] = array('title' => 'View Theme - '.$theme->theme_title, 'description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('themes/themes_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to Themes Management</a>');
		$page_data['theme'] = $theme;

		$page_data['styles'] = array('plugins/bootstrap-milestones/bootstrap-milestones.min','plugins/lightcase/css/lightcase');
		$page_data['scripts'] = array('custom/theme.functions','plugins/lightcase/js/lightcase','custom/preview.theme');

		$this->load->view('layouts/main_layout', $page_data);
	}

	public function save_theme($theme_id = NULL) {
		$this->user_m->checkPermissions("add-themes",true);
		if(empty($_POST)) {
			$this->load->model('cms_m');

			$page_data['title'] = ($theme_id) ? 'Edit Theme' : 'Add Theme';
			$page_data['subview'] = 'themes/save_theme_v';
			$page_data['page_header_data'] = array('description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('themes/themes_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to Themes Management</a>');
			$page_data['styles'] = array('plugins/jasny-bootstrap/css/jasny-bootstrap.min','plugins/bootstrap-tags/bootstrap-tagsinput','plugins/bootstrap-multiselect/css/bootstrap-multiselect','plugins/summernote/summernote');
			$page_data['scripts'] = array('plugins/jasny-bootstrap/js/jasny-bootstrap.min','plugins/bootstrap-tags/bootstrap-tagsinput.min','plugins/bootstrap-multiselect/js/bootstrap-multiselect','plugins/summernote/summernote.min','custom/theme.functions','custom/save.theme');
			$page_data['categories'] = $this->theme_m->get_distinct('theme_category');
			$page_data['cms'] = $this->cms_m->get();
			
			if($theme_id) {
				$page_data['theme'] = $this->theme_m->get($theme_id);

				if(!$this->user_m->checkIfAdmin($this->session->userdata('user_id'))) {
					if($page_data['theme']->created_by !== $this->session->userdata('user_id')) {
						$this->session->set_flashdata('alert_type', 'danger');
						$this->session->set_flashdata('alert_message', 'Not Allowed.');
						redirect('themes/themes_management');
					}
				}

				//$page_data['page_header_data']['title'] = 'Edit Theme - '.$page_data['theme']->theme_name;
				$page_data['page_header_data']['title'] = 'Edit Theme - <span class="theme-title">'.$page_data['theme']->theme_title.'</span>';	
			} else {
				$page_data['theme'] = $this->theme_m->get_new();
				//$page_data['page_header_data']['title'] = 'Add Theme - New Theme';
				$page_data['page_header_data']['title'] = 'Add Theme - <span class="theme-title">'.$page_data['theme']->theme_title.'</span>';
				
			}

			$this->load->view('layouts/main_layout', $page_data);
		} else {
			$this->theme_m->save_theme($theme_id);
		}
	}

	public function update_theme($theme_id) {
		$this->user_m->checkPermissions("update-themes",true);
		if(empty($_POST)) {
			$this->load->model('cms_m');

			$page_data['title'] = 'Update Theme';
			$page_data['subview'] = 'themes/update_theme_v';
			$page_data['page_header_data'] = array('description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('themes/themes_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to Themes Management</a>');
			$page_data['styles'] = array('plugins/jasny-bootstrap/css/jasny-bootstrap.min','plugins/bootstrap-multiselect/css/bootstrap-multiselect','plugins/summernote/summernote');
			$page_data['scripts'] = array('plugins/jasny-bootstrap/js/jasny-bootstrap.min','plugins/summernote/summernote.min','plugins/bootstrap-multiselect/js/bootstrap-multiselect','custom/update.theme');

			$page_data['cms'] = $this->cms_m->get();
			$page_data['theme'] = $this->theme_m->get($theme_id);

			if(!$this->user_m->checkIfAdmin($this->session->userdata('user_id'))) {
				if($page_data['theme']->created_by !== $this->session->userdata('user_id')) {
					$this->session->set_flashdata('alert_type', 'danger');
					$this->session->set_flashdata('alert_message', 'Not Allowed.');
					redirect('themes/themes_management');
				}
			}

			$page_data['page_header_data']['title'] = 'Update Theme - '.$page_data['theme']->theme_title;

			$this->load->view('layouts/main_layout', $page_data);
		} else {
			$theme = $this->theme_m->get($theme_id);
			$delete_path = APPPATH.'../themes/'.$theme->theme_name;
			$this->_delete_directory($delete_path);

			$this->theme_m->update_theme($theme_id);
		}
	}

	public function delete_theme($theme_id) {
		$this->user_m->checkPermissions("delete-themes",true);
		$theme = $this->theme_m->get($theme_id);

		if(!$this->user_m->checkIfAdmin($this->session->userdata('user_id'))) {
			if($theme->created_by !== $this->session->userdata('user_id')) {
				$this->session->set_flashdata('alert_type', 'danger');
				$this->session->set_flashdata('alert_message', 'Not Allowed.');
				redirect('themes/themes_management');
			}
		}

		$delete_path = APPPATH.'../themes/'.$theme->theme_name;

		$this->_delete_directory($delete_path);

		$this->theme_m->delete($theme_id);
		$this->session->set_flashdata('alert', 'Theme successfully deleted.');
		$this->session->set_flashdata('alert_type','success');

		$this->logs_m->save_log('Delete Theme', serialize($theme));

		redirect('themes/themes_management','refresh');
	}

	function _delete_directory($directory, $empty = false) { 
	    if(substr($directory,-1) == "/") { 
	        $directory = substr($directory,0,-1); 
	    } 

	    if(!file_exists($directory) || !is_dir($directory)) { 
	        return false; 
	    } elseif(!is_readable($directory)) { 
	        return false; 
	    } else { 
	        $directoryHandle = opendir($directory); 
	        
	        while ($contents = readdir($directoryHandle)) { 
	            if($contents != '.' && $contents != '..') { 
	                $path = $directory . "/" . $contents; 
	                
	                if(is_dir($path)) { 
	                    $this->_delete_directory($path); 
	                } else { 
	                    unlink($path); 
	                } 
	            } 
	        } 
	        
	        closedir($directoryHandle); 

	        if($empty == false) { 
	            if(!rmdir($directory)) { 
	                return false; 
	            } 
	        } 
	        
	        return true; 
	    } 
	}

	/*********************************************/ 

	#AJAX CALLS

	/*********************************************/

	function theme_name_exists($id = false) {
		if($id) {
			$this->db->where('theme_id != ', $id);
		}

		$query = $this->theme_m->get_by(array('theme_name' => $this->input->post('theme_name')));

		if(count($query)) {
			echo 0;
		} else {
			echo 1;
		}
		die();
	}

	/*********************************************/

	#API CALLS

	/*********************************************/

/*	function tools_api() {
		header("Access-Control-Allow-Origin: *");
		header('Content-Type: application/json');

		$tools = $this->tool_m->get();

		foreach ($tools as $tool) {
			$tool_sets[] = array(
				'tool_id' => $tool->tool_id,
				'tool_name' => $tool->tool_name,
				'tool_title' => $tool->tool_title,
				'tool_description' => $tool->tool_description,
				'tool_category' => $tool->tool_category,
				'tool_image' => 'http://176.66.3.246/serino_master/tools/'.$tool->tool_name.'/screenshot.png'
			);
		}

		echo json_encode($tool_sets);
	}*/

	public function sample() {
		
	}
}