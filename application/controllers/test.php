<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class test extends Backend_Controller {

  	public function __construct() {
		parent::__construct();
		$this->load->model('test_m');
	}

	public function index() { 

		return $this->display_view();

	}

	function display_view(){   
		$this->user_m->checkPermissions("test-users",true); 
		$page_data = array(

			'title' 		=> 'Test Page', 
			'subview'		=> 'test/test_second_v.php',
			'styles'		=> array(
				'plugins/datatables/datatables.min',
				'plugins/codemirror/codemirror', 
				'css/test-custom',
			),
			'scripts'		=> array(
					'plugins/datatables/datatables.min',
					'plugins/codemirror/codemirror', 
					'plugins/quicksearch/jquery.quicksearch',  
					'ajax/test', 
			),
			'table_test'	=> array_chunk($this->pagination_array($this->test_m->getTestSearch('hello')), 10)

		); 
		$this->load->view('test/test_view_v.php', $page_data); 
	}

	function pagination_array($dbase = array()){
		$dbase_chunk = array(); 
		foreach ($dbase as $key => $value) {
			$dbase_chunk[] = array(
				'id' => $value->id,
				'full_name' => $value->full_name, 
				'age' => $value->age, 
				'gender' => $value->gender,  
			);
		} 
		return $dbase_chunk; 
	}

	function pagination_json(){
		$page_info = array(
			'page_number' 	=> $_POST['page_selected'], 
		); 

		$page_number = $page_info['page_number'];

		$data = new stdClass();
		$data = $this->test_m->getTest();

		if(isset($_POST['search']) || !empty($_POST['search'])){
			$data = $this->test_m->getTestSearch($_POST['search']);
		}  
		if (count($data) <= 0) {
			echo json_encode(
			array(
				array(
					'id' 		=> 'NoData', 
					'full_name' => '<span class="text-danger"> No Data </span>',
					'age' 		=> '<span class="text-danger"> No Data </span>',
					'gender' 	=> '<span class="text-danger"> No Data </span>',
				)
			), true);
			return false;
		}
		echo json_encode(array_chunk($this->pagination_array($data), 10)[$page_number], true);
	}

	function page_link(){
		$count = count($this->test_m->getTest());
		if(isset($_POST['search']) || !empty($_POST['search'])){
			$count = count($this->test_m->getTestSearch($_POST['search']));
		}
		$total = ceil($count/10);
		
		if (count($count) <= 0) {
			echo json_encode(
			array(
				'total_pagination' 	=>  0, 
				'total_count' 		=>  0
			), true);
			return false;
		}

		echo json_encode(array(
			'total_pagination' 	=> $total, 
			'total_count' 		=> $count,
		), true); 
	} 

}