<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('user_m');

        if($this->user_m->loggedin()) {
            redirect('main');
        }
	}

	public function index() {
		if(empty($_POST)) {	
			$page_data['title'] = 'Login';
			$page_data['subview'] = 'login_v';
			$this->load->view('layouts/login_layout', $page_data);
		} else {
			$this->user_m->login();
		}
	}
}