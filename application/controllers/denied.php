<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Denied extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('user_m');
		$this->load->model('cms_m');
	}

	public function index() {
		$page_data['title'] = 'Access Denied';
		$page_data['subview'] = 'commons/denied';
		$page_data['page_header_data'] = array('title' => 'Access Denied', 'description' => '');
		$this->load->view('layouts/main_layout', $page_data);
	}
}