<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index() {
		$this->load->model('project_m');
		$this->load->model('tool_m');
		$this->load->model('theme_m');
		$this->load->model('project_m');
		$this->load->model('plugin_m');
		$this->load->model('release_m');

		$page_data['releases'] = $this->release_m->get();

		$page_data['projects'] = $this->project_m->get_inprogress_projects();

		$page_data['title'] = 'Serino Master Home';
		$page_data['subview'] = 'dashboard_v';
		$page_data['page_header_data'] = array('title' => 'Dashboard', 'description' => 'Welcome to Serino Test Harness.');
		$this->load->view('layouts/main_layout', $page_data);
	}
}