<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model(array('plugin_m', 'user_m'));
	}

	/*********************************************/

	#API CALLS

	/*********************************************/

	function index() {
		$page_data['title'] = 'Serino Master CMS - Store';
		$page_data['subview'] = 'store_v';
		$page_data['plugin'] = $this->plugin_m->get(1);
		$this->load->view('layouts/store_layout', $page_data);
	}
}
