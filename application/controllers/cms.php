<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('user_m');
		$this->load->model('cms_m');
	}

	public function index() {
		$this->tools_management();
	}

	/*********************************************/ 

	#CMS MANAGEMENT

	/*********************************************/

	public function cms_management() {
		$this->user_m->checkPermissions("view-cms",true);
		$page_data['title'] = 'CMS Management';
		$page_data['subview'] = 'cms/cms_management_v';
		$page_data['page_header_data'] = array('title' => 'CMS Management', 'description' => '');
		$page_data['styles'] = array('plugins/datatables/datatables.min');
		$page_data['scripts'] = array('plugins/datatables/datatables.min','custom/cms.management');
		$this->load->view('layouts/main_layout', $page_data);
	}

	public function view_cms($cms_id) {
		$this->user_m->checkPermissions("view-cms",true);
		$cms = $this->cms_m->get($cms_id);
		$page_data['title'] = 'Serino CMS '.$cms->version.' - '.$cms->codename;
		$page_data['subview'] = 'cms/view_cms_v';
		$page_data['page_header_data'] = array('title' => 'Serino CMS '.$cms->version.' - '.$cms->codename, 'description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('cms/cms_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to CMS Management</a>');
		$page_data['cms'] = $cms;

		$page_data['scripts'] = array('custom/preview.cms');

		$this->load->view('layouts/main_layout', $page_data);
	}	

	public function save_cms($cms_id = NULL) {
		$this->user_m->checkPermissions(array("update-cms","add-cms"),true);
		if(empty($_POST)) {
			$page_data['title'] = ($cms_id) ? 'Edit CMS' : 'Add CMS';
			$page_data['subview'] = 'cms/save_cms_v';
			$page_data['page_header_data'] = array('description' => '');
			$page_data['styles'] = array('plugins/jasny-bootstrap/css/jasny-bootstrap.min','plugins/summernote/summernote');
			$page_data['scripts'] = array('plugins/jasny-bootstrap/js/jasny-bootstrap.min','plugins/summernote/summernote.min','custom/save.cms');
			
			if($cms_id) {
				$page_data['cms'] = $this->cms_m->get($cms_id);
				$page_data['page_header_data']['title'] = 'Edit CMS version '.$page_data['cms']->version;		
			} else {
				$page_data['cms'] = $this->cms_m->get_new();
				$page_data['page_header_data']['title'] = 'Add CMS - <span class="cms_codename">'.$page_data['cms']->codename.'</span>';
			}

			$this->load->view('layouts/main_layout', $page_data);
		} else {
			$this->tool_m->save_tool($cms_id);
		}
	}
}