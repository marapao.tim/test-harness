<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email_Templates extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);

		$this->load->model('user_m');
		$this->load->model('email_templates_m');
		$this->load->model('unsafecrypto');
	}

	public function index() {
		$this->user_m->checkPermissions("view-email-templates",true);

		$page_data['title'] = 'Email Templates';
		$page_data['subview'] = 'email_templates/view_v';
		$page_data['page_header_data'] = array('title' => '<i class="ion-email"></i> Email Templates', 'description' => 'You can store email templates here.');

		$page_data['templates'] = $this->email_templates_m->get();

		$page_data['styles'] = array('plugins/datatables/dataTables.bootstrap');
		$page_data['scripts'] = array('plugins/datatables/jquery.dataTables.min','plugins/datatables/dataTables.bootstrap.min','custom/view.email_templates');
		$this->load->view('layouts/main_layout', $page_data);
	}

	public function add(){
		$this->user_m->checkPermissions("add-email-templates",true);
		$page_data['title'] = 'Email Templates';
		$page_data['subview'] = 'email_templates/add_v';
		$page_data['page_header_data'] = array('title' => '<i class="ion-email"></i> Add Email Template', 'description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('email_templates').'"><i class="ion-ios-arrow-thin-left"></i> Back to Email Templates List</a>');

		$page_data["modules"] = $this->email_templates_m->get_template_categories();

		$page_data['styles'] = array('plugins/jasny-bootstrap/css/jasny-bootstrap.min','plugins/bootstrap-tags/bootstrap-tagsinput','plugins/bootstrap-multiselect/css/bootstrap-multiselect','plugins/summernote/summernote');
		$page_data['scripts'] = array('plugins/jasny-bootstrap/js/jasny-bootstrap.min','plugins/bootstrap-tags/bootstrap-tagsinput.min','plugins/bootstrap-multiselect/js/bootstrap-multiselect','plugins/summernote/summernote.min','custom/email_templates');
		$this->load->view('layouts/main_layout', $page_data);
	}

	public function edit($id){
		$this->user_m->checkPermissions("edit-email-templates",true);
		$page_data['title'] = 'Email Templates';
		$page_data['subview'] = 'email_templates/edit_v';
		$page_data['page_header_data'] = array('title' => '<i class="ion-email"></i> Edit Email Template', 'description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('email_templates').'"><i class="ion-ios-arrow-thin-left"></i> Back to Email Templates List</a>');

		$page_data["modules"] = $this->email_templates_m->get_template_categories();
		$page_data["template"] = $this->email_templates_m->get($id);
		
		$page_data['styles'] = array('plugins/jasny-bootstrap/css/jasny-bootstrap.min','plugins/bootstrap-tags/bootstrap-tagsinput','plugins/bootstrap-multiselect/css/bootstrap-multiselect','plugins/summernote/summernote');
		$page_data['scripts'] = array('plugins/jasny-bootstrap/js/jasny-bootstrap.min','plugins/bootstrap-tags/bootstrap-tagsinput.min','plugins/bootstrap-multiselect/js/bootstrap-multiselect','plugins/summernote/summernote.min','custom/email_templates');
		$this->load->view('layouts/main_layout', $page_data);
	}

	public function create_template(){
		$ret = $this->email_templates_m->create_template();

 		redirect('/email_templates/edit/'.$ret,'refresh');
	}

	public function update_template($id){
		$this->email_templates_m->update_template($id);

 		redirect('/email_templates/edit/'.$id,'refresh');
	}

	public function delete($id){
		$ret = $this->email_templates_m->delete($id);
		echo json_encode($ret);
 		//redirect('/email_templates/','refresh');
	}

}
