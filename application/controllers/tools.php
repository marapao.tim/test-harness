<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tools extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);

		$this->load->model('user_m');
		$this->load->model('tool_m');
	}

	public function index() {
		$this->tools_management();
	}

	/*********************************************/ 

	#TOOLS MANAGEMENT

	/*********************************************/
	
	public function tools_management() {
		$this->user_m->checkPermissions("view-tools",true);
		$page_data['title'] = 'Tools Management';
		$page_data['subview'] = 'tools/tool_management_v';
		$page_data['page_header_data'] = array('title' => 'Tools Management', 'description' => 'You can manage your Serino Page Builder Tools here.');
		$page_data['styles'] = array('plugins/datatables/datatables.min');
		$page_data['scripts'] = array('plugins/datatables/datatables.min','custom/tool.functions','custom/tool.management');

		$this->load->view('layouts/main_layout', $page_data);
	}

	public function view_tool($tool_id) {
		$this->user_m->checkPermissions("view-tools",true);
		$tool = $this->tool_m->get($tool_id);

		if(!$this->user_m->checkIfAdmin($this->session->userdata('user_id'))) {
			if($tool->created_by !== $this->session->userdata('user_id')) {
				$this->session->set_flashdata('alert_type', 'danger');
				$this->session->set_flashdata('alert_message', 'Not Allowed.');
				redirect('tools/tools_management');
			}
		}

		$page_data['title'] = 'View Tool - '.$tool->tool_title;
		$page_data['subview'] = 'tools/view_tool_v';
		$page_data['page_header_data'] = array('title' => '<span class="tool-title">View Tool - '.$tool->tool_title.'</span>', 'description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('tools/tools_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to Tools Management</a>');
		$page_data['tool'] = $tool;

		$page_data['styles'] = array('plugins/bootstrap-milestones/bootstrap-milestones.min','plugins/lightcase/css/lightcase');
		$page_data['scripts'] = array('custom/tool.functions','plugins/lightcase/js/lightcase','custom/preview.tool');
		
		$this->load->view('layouts/main_layout', $page_data);
	}	

	public function save_tool($tool_id = NULL) {
		$this->user_m->checkPermissions("add-tools",true);
		if(empty($_POST)) {
			$this->load->model('cms_m');

			$page_data['title'] = ($tool_id) ? 'Edit Tool' : 'Add Tool';
			$page_data['subview'] = 'tools/save_tool_v';
			$page_data['page_header_data'] = array('description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('tools/tools_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to Tools Management</a>');
			$page_data['styles'] = array('plugins/jasny-bootstrap/css/jasny-bootstrap.min','plugins/bootstrap-tags/bootstrap-tagsinput','plugins/bootstrap-multiselect/css/bootstrap-multiselect','plugins/summernote/summernote');
			$page_data['scripts'] = array('plugins/jasny-bootstrap/js/jasny-bootstrap.min','plugins/bootstrap-tags/bootstrap-tagsinput.min','plugins/bootstrap-multiselect/js/bootstrap-multiselect','plugins/summernote/summernote.min','custom/tool.functions','custom/save.tool');
			$page_data['categories'] = $this->tool_m->get_distinct('tool_category');
			$page_data['cms'] = $this->cms_m->get();
			
			if($tool_id) {
				$page_data['tool'] = $this->tool_m->get($tool_id);

				if(!$this->user_m->checkIfAdmin($this->session->userdata('user_id'))) {
					if($page_data['tool']->created_by !== $this->session->userdata('user_id')) {
						$this->session->set_flashdata('alert_type', 'danger');
						$this->session->set_flashdata('alert_message', 'Not Allowed.');
						redirect('tools/tools_management');
					}
				}

				$page_data['page_header_data']['title'] = 'Edit Tool - <span class="tool-title">'.$page_data['tool']->tool_title.'</span>';		
			} else {
				$page_data['tool'] = $this->tool_m->get_new();
				$page_data['page_header_data']['title'] = 'Add Tool - <span class="tool-title">'.$page_data['tool']->tool_title.'</span>';
			}

			$this->load->view('layouts/main_layout', $page_data);
		} else {
			$this->tool_m->save_tool($tool_id);
		}
	}

	public function update_tool($tool_id) {
		$this->user_m->checkPermissions("update-tools",true);
		if(empty($_POST)) {
			$this->load->model('cms_m');

			$tool = $this->tool_m->get($tool_id);
			$page_data['title'] = 'Update Tool - '.$tool->tool_title;
			$page_data['subview'] = 'tools/update_tool_v';
			$page_data['page_header_data'] = array('description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('tools/tools_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to Tools Management</a>');
			$page_data['styles'] = array('plugins/jasny-bootstrap/css/jasny-bootstrap.min','plugins/bootstrap-multiselect/css/bootstrap-multiselect','plugins/summernote/summernote');
			$page_data['scripts'] = array('plugins/jasny-bootstrap/js/jasny-bootstrap.min','plugins/summernote/summernote.min','plugins/bootstrap-multiselect/js/bootstrap-multiselect','custom/update.tool');

			$page_data['cms'] = $this->cms_m->get();
			$page_data['tool'] = $tool;

			if(!$this->user_m->checkIfAdmin($this->session->userdata('user_id'))) {
				if($page_data['tool']->created_by !== $this->session->userdata('user_id')) {
					$this->session->set_flashdata('alert_type', 'danger');
					$this->session->set_flashdata('alert_message', 'Not Allowed.');
					redirect('tools/tools_management');
				}
			}

			$page_data['page_header_data']['title'] = 'Update Tool - '.$tool->tool_title;
			
			$this->load->view('layouts/main_layout', $page_data);
		} else {
			$tool = $this->tool_m->get($tool_id);
			$delete_path = APPPATH.'../tools/'.$tool->tool_name;
			$this->_delete_directory($delete_path);

			$this->tool_m->update_tool($tool_id);
		}
	}

	function _delete_directory($directory, $empty = false) { 
	    if(substr($directory,-1) == "/") { 
	        $directory = substr($directory,0,-1); 
	    } 

	    if(!file_exists($directory) || !is_dir($directory)) { 
	        return false; 
	    } elseif(!is_readable($directory)) { 
	        return false; 
	    } else { 
	        $directoryHandle = opendir($directory); 
	        
	        while ($contents = readdir($directoryHandle)) { 
	            if($contents != '.' && $contents != '..') { 
	                $path = $directory . "/" . $contents; 
	                
	                if(is_dir($path)) { 
	                    $this->_delete_directory($path); 
	                } else { 
	                    unlink($path); 
	                } 
	            } 
	        } 
	        
	        closedir($directoryHandle); 

	        if($empty == false) { 
	            if(!rmdir($directory)) { 
	                return false; 
	            } 
	        } 
	        
	        return true; 
	    } 
	}
}