<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Appbuilder extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
	}

	public function upload() {
		if(count($_FILES)) {
	 		$file_path = FCPATH.'store/app';
	 		file_exists($file_path) || mkdir($file_path,0777);
	 		$filepath = $file_path.'/file.zip';

	 		move_uploaded_file($_FILES['appfile']['tmp_name'], $filepath);

	 		$response = [
	 			'successful' => true,
	 			'message' => 'Upload successful.'
	 		];

	 		echo json_encode($response);
		} else {
	 		$response = [
	 			'successful' => false,
	 			'message' => 'File is required.'
	 		];

	 		echo json_encode($response);
		}
	}
}
