<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tools extends CI_Controller {

	public $messages = array(
		'add' => array(
			'success'  => 'Tool added successfully.',
			'failed'   => ''
		),
		'edit' => array(
			'success' => 'Tool updated successfully.',
			'failed'  => ''
		),
		'update' => array(
			'success' => 'Tool version updated successfully.',
			'failed'  => ''
		),
		'delete' => array(
			'success' => 'Tool deleted successfully.',
			'failed'  => ''
		),
		'delete_all' => array(
			'success' => 'All tools were deleted successfully.',
			'failed'  => ''
		)
	);

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_m');
		$this->load->model('tool_m');

		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
	}

	public function index($tool_id = null) {
		$this->load->helper('array');
		if($tool_id == null) {
			switch ($this->input->server('REQUEST_METHOD')) {

				/** GET ALL TOOLS */

				case 'GET':
					if(!$this->user_m->checkIfAdmin($this->session->userdata('user_id'))) {
						$this->db->where('created_by', $this->session->userdata('user_id'));
					}

					$tools = $this->tool_m->get();

					$response = array(
						'count' => count($tools),
						'items' => $tools,
						'successful' => true
					);

					echo json_encode($response);

					break;				

				/** ADD NEW TOOL */

				case 'POST':
					//validation
					$this->load->library('form_validation');
					$this->form_validation->set_rules('tool_title', 'title', 'required');
					$this->form_validation->set_rules('tool_type', 'type', 'required');
					$this->form_validation->set_rules('tool_category', 'category', 'required');

					$this->form_validation->set_rules('cms_support', 'cms support', 'required');

					//validate fields
					if(!$this->form_validation->run()) {
						$response = array(
							'successful' => false,
							'message'    => validation_errors('<div>', '</div>')
						);

						echo json_encode($response);
						die();
					}

					//validate if file exists
					if($_FILES['tool_file']['name'] == '') {
						$response = array(
							'successful' => false,
							'message'    => 'Please select a file'
						);

						echo json_encode($response);
						die();
					}

					//construct tool cms id
					$this->db->order_by('tool_id', 'DESC');
					$this->db->limit(1);
					$last_id = $this->tool_m->get();

					if(count($last_id)) {
						$last_id = ($last_id[0]->tool_id + 1);
					} else {
						$last_id = 1;
					}

					$tool_cms_id = str_pad($last_id, 10, '0',STR_PAD_LEFT).'-'.$this->tool_m->clean_string($this->input->post('tool_title'));

					//version meta
		 			$version_meta = array(
		 				"1.00" => array(
		 					'release_date' => date('Y-m-d H:i:s'),
		 					'version'      => '1.00',
		 					'filename'     => $tool_cms_id.'.zip',
		 					'cms_support'  => implode(',',$this->input->post('cms_support')),
		 					'remarks'      => $this->input->post('version_remarks'),
		 					'updated_by'   => $this->session->userdata('user_fullname')
		 				)
		 			);

					$_POST = elements(array(
						'tool_title',
						'tool_type',
						'tool_author',
						'tool_category',
						'tool_version',
						'tool_tags',
						'tool_description'
					),$_POST);

					//clear entities
					//$_POST['tool_description'] = htmlentities($this->input->post('tool_description'));

					$_POST['tool_cms_id'] = $tool_cms_id;
					$_POST['tool_version'] = '1.00';
					$_POST['version_metas'] = serialize($version_meta);

			 		//create dir
			 		$tools_path = FCPATH.'store/tools';
			 		$tool_path = $tools_path.'/'.$tool_cms_id;
			 		$extract_path = $tool_path.'/'.$_POST['tool_version'];

			 		file_exists($tools_path) || mkdir($tools_path,0777);
			 		file_exists($tool_path) || mkdir($tool_path,0777);
					file_exists($extract_path) || mkdir($extract_path,0777);

					//construct name then upload
					$filepath = $extract_path.'/'.$tool_cms_id.'.zip';
					move_uploaded_file($_FILES['tool_file']['tmp_name'], $filepath);

					//unzip the file
					$zip = new ZipArchive;
					$open_zip = $zip->open($filepath);
					if ($open_zip === TRUE) {
						$zip->extractTo($extract_path);
						$zip->close();
					} else {
						$response = array(
							'successful' => false,
							'message'    => 'Problem unzipping the file'
						);

						echo json_encode($response);
						die();
					}

					if($this->tool_m->save($this->input->post())) {
						$response = array(
							'successful' => true,
							'tool_id'    => $this->db->insert_id(),
							'message'    => $this->messages['add']['success']
						);
					} else {
						$response = array(
							'successful' => false,
							'message'    => $this->messages['add']['failed']
						);
					}

					$this->session->set_flashdata('alert_type', ($response['successful']) ? 'success' : 'danger');
					$this->session->set_flashdata('alert_message', $response['message']);
					echo json_encode($response);
					break;				

				/** DELETE ALL TOOLS */

				case 'DELETE':
					$this->db->empty_table('srn_tools');
					
					$response = array(
						'successful' => true,
						'message'    => $this->messages['delete_all']['success']
					);

					echo json_encode($response);

					break;
				
				default:
					# code...
					break;
			}
		} else {
			$tool = $this->tool_m->get($tool_id);

			if(count($tool) <= 0) {
				$response = array(
					'count' => 0,
					'items' => array(),
					'successful' => false,
					'message' => 'Tool doesn\'t exists' 
				);

				echo json_encode($response);
				die();
			}

			switch ($this->input->server('REQUEST_METHOD')) {

				/** GET TOOL USING ID */

				case 'GET':
					
					$tool->version_metas = unserialize($tool->version_metas);

					$response = array(
						'count' => 1,
						'model' => $tool,
						'successful' => true
					);

					echo json_encode($response);

					break;		

				/** UPDATE/EDIT TOOL BY ID */		

				case 'POST':
					if($this->uri->segment(4) == 'update') {
						//validation
						$this->load->library('form_validation');
						$this->form_validation->set_rules('cms_support', 'cms support', 'required');
						$this->form_validation->set_rules('tool_version', 'new version', 'required|numeric|greater_than['.$tool->tool_version.']');

						//validate fields
						if(!$this->form_validation->run()) {
							$response = array(
								'successful' => false,
								'message'    => validation_errors('<div>', '</div>')
							);

							echo json_encode($response);
							die();
						}

						//validate if file exists
						if($_FILES['tool_file']['name'] == '') {
							$response = array(
								'successful' => false,
								'message'    => 'Please select a file'
							);

							echo json_encode($response);
							die();
						}

						//update version metas
						$version_metas = unserialize($tool->version_metas);
						$new_version = number_format($this->input->post('tool_version'), 2);

				 		$version_metas[$new_version] = array(
							'release_date' => date('Y-m-d H:i:s'),
							'version'      => $new_version,
							'filename'     => $tool->tool_cms_id.'.zip',
							'cms_support'  => implode(',',$this->input->post('cms_support')),
							'remarks'      => $this->input->post('version_remarks'),
							'updated_by'   => $this->session->userdata('user_fullname')
						);

				 		$new_tool_data = array(
				 			'tool_version' => $new_version,
				 			'version_metas' => serialize($version_metas)
				 		);

				 		//create dir
				 		$tools_path = FCPATH.'store/tools';
				 		$tool_path = $tools_path.'/'.$tool->tool_cms_id;
				 		$extract_path = $tool_path.'/'.$new_version;

						file_exists($extract_path) || mkdir($extract_path,0777);

						//construct name then upload
						$filepath = $extract_path.'/'.$tool->tool_cms_id.'.zip';
						move_uploaded_file($_FILES['tool_file']['tmp_name'], $filepath);

						//unzip the file
						$zip = new ZipArchive;
						$open_zip = $zip->open($filepath);
						if ($open_zip === TRUE) {
							$zip->extractTo($extract_path);
							$zip->close();
						} else {
							$response = array(
								'successful' => false,
								'message'    => 'Problem unzipping the file'
							);

							echo json_encode($response);
							die();
						}

						if($this->tool_m->save($new_tool_data, $tool_id)) {
							$response = array(
								'successful' => true,
								'message'    => $this->messages['update']['success']
							);
						} else {
							$response = array(
								'successful' => false,
								'message'    => $this->messages['update']['failed']
							);
						}

						$this->session->set_flashdata('alert_type', ($response['successful']) ? 'success' : 'danger');
						$this->session->set_flashdata('alert_message', $response['message']);
						echo json_encode($response);

					} else {
						//validation
						$this->load->library('form_validation');
						$this->form_validation->set_rules('tool_title', 'title', 'required');
						$this->form_validation->set_rules('tool_type', 'type', 'required');
						$this->form_validation->set_rules('tool_category', 'category', 'required');

						//validate fields
						if(!$this->form_validation->run()) {
							$response = array(
								'successful' => false,
								'message'    => validation_errors('<div>', '</div>')
							);

							echo json_encode($response);
							die();
						}

						$_POST = elements(array(
							'tool_title',
							'tool_type',
							'tool_author',
							'tool_category',
							'tool_tags',
							'tool_description'
						),$_POST);

						//clear entities
						//$_POST['tool_description'] = htmlentities($this->input->post('tool_description'));

						if($this->tool_m->save($this->input->post(), $tool_id)) {
							$response = array(
								'successful' => true,
								'message'    => $this->messages['edit']['success']
							);
						} else {
							$response = array(
								'successful' => false,
								'message'    => $this->messages['edit']['failed']
							);
						}

						$this->session->set_flashdata('alert_type', ($response['successful']) ? 'success' : 'danger');
						$this->session->set_flashdata('alert_message', $response['message']);
						echo json_encode($response);
					}

					break;

				/** DELETE TOOL BY ID */	

				case 'DELETE':
					if(!$this->user_m->checkIfAdmin($this->session->userdata('user_id'))) {
						if($tool->created_by !== $this->session->userdata('user_id')) {
							$response = array(
								'successful' => false,
								'message'    => 'Not Allowed.'
							);
							$this->session->set_flashdata('alert_type', ($response['successful']) ? 'success' : 'danger');
							$this->session->set_flashdata('alert_message', $response['message']);
							echo json_encode($response);
							die();
						}
					}

					$tool_dir_path = FCPATH.'store/tools/'.$tool->tool_cms_id;
					$this->tool_m->_delete_directory($tool_dir_path);

					$this->tool_m->delete($tool_id);



					$response = array(
						'successful' => true,
						'message'    => $this->messages['delete']['success']
					);

					$this->session->set_flashdata('alert_type', ($response['successful']) ? 'success' : 'danger');
					$this->session->set_flashdata('alert_message', $response['message']);
					echo json_encode($response);
					break;
				
				default:
					# code...
					break;
			}
		}
	}

	public function update($skip = 0, $limit = 10) {
		$tools = $this->db->query('SELECT `tool_id`, `tool_cms_id`, `tool_title`, `tool_type`, `tool_author`, `tool_version`, `tool_category`, `tool_tags`, `tool_description`, `version_metas`, CONCAT("'.base_url('store/tools/').'","/",tool_cms_id,"/",tool_version,"/",tool_cms_id,".zip") as file_path FROM srn_tools 
			WHERE FIND_IN_SET(`tool_cms_id`, "'.$this->input->post('tool_cms_ids').'")
			LIMIT '.$limit.' OFFSET '.$skip)->result();

		foreach ($tools as $key => $tool) {
			$tool->version_metas = unserialize($tool->version_metas);
		}

		$response = array(
			'count' => count($tools),
			'items' => $tools,
			'successful' => true
		);

		echo json_encode($response);
	}
}
