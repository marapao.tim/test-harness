<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Roles extends CI_Controller {

	public $messages = array(
		'add' => array(
			'success'  => 'User Role added successfully.',
			'failed'   => ''
		),
		'edit' => array(
			'success' => 'User Role updated successfully.',
			'failed'  => ''
		),
		'delete' => array(
			'success' => 'User Role deleted successfully.',
			'failed'  => ''
		),
		'delete_all' => array(
			'success' => 'All User Roles were deleted successfully.',
			'failed'  => ''
		)
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('user_role_m');
	}

	public function index($user_role_id = null) {
		$this->load->helper('array');

		if($user_role_id == null) {
			switch ($this->input->server('REQUEST_METHOD')) {
				case "GET":
					$user_roles = $this->user_role_m->get();
					foreach ($user_roles as $key => $value) {
						$value->user_role_permissions = unserialize($value->user_role_permissions);
					}

					$response = array(
						'count' 		=> count($user_roles),
						'items' 		=> $user_roles,
						'successful'	=> true
					);

					echo json_encode($response);
					break;

				case "POST":
					$_POST['user_role_code'] = $this->user_role_m->clean_string($this->input->post('user_role_title'));
					$this->load->library('form_validation');
					$this->form_validation->set_rules('user_role_title', 'title', 'required');
					$this->form_validation->set_rules('user_role_permissions', 'permissions', 'required');
					$this->form_validation->set_rules('user_role_code', 'title', 'is_unique[srn_user_roles.user_role_code]');

					if(!$this->form_validation->run()) {
						$response = array(
							'successful' => false,
							'message'    => validation_errors('<div>', '</div>')
						);

						echo json_encode($response);
						die();
					}

					$_POST['user_role_permissions'] = serialize($this->input->post('user_role_permissions'));

					if($this->user_role_m->save($this->input->post())) {
						$response = array(
							'successful' 	=> true,
							'user_role_id'	=> $this->db->insert_id(),
							'message'    	=> $this->messages['add']['success']
						);
					} else {
						$response = array(
							'successful' => false,
							'message'    => $this->messages['add']['failed']
						);
					}

					$this->session->set_flashdata('alert_type', $response['successful']);
					$this->session->set_flashdata('alert_message', $response['message']);
					echo json_encode($response);
					break;			
			}
		} else {
			$user_role = $this->user_role_m->get($user_role_id);
				
			if(count($user_role) <= 0) {
				$response = array(
					'count' => 0,
					'items' => array(),
					'successful' => false,
					'message' => 'User Role doesn\'t exists' 
				);

				echo json_encode($response);
				die();
			}
			switch($this->input->server('REQUEST_METHOD')) {
				case 'GET':
					$response = array(
						'count' => 1,
						'model' => $user_role,
						'successful' => true
					);

					echo json_encode($response);

					break;

				case 'POST':
					$_POST['user_role_code'] = $this->user_role_m->clean_string($this->input->post('user_role_title'));
					$this->load->library('form_validation');
					$this->form_validation->set_rules('user_role_title', 'title', 'required');
					$this->form_validation->set_rules('user_role_permissions', 'permissions', 'required');
					if($this->input->post('user_role_code') != $user_role->user_role_code) {
						$this->form_validation->set_rules('user_role_code', 'title', 'is_unique[srn_user_roles.user_role_code]');
					}

					if(!$this->form_validation->run()) {
						$response = array(
							'successful' => false,
							'message'    => validation_errors('<div>', '</div>')
						);

						echo json_encode($response);
						die();
					}

					$_POST['user_role_permissions'] = serialize($this->input->post('user_role_permissions'));

					if($this->user_role_m->save($this->input->post(), $user_role_id)) {
						$response = array(
							'successful' => true,
							'message'    => $this->messages['edit']['success']
						);
					} else {
						$response = array(
							'successful' => false,
							'message'    => $this->messages['edit']['failed']
						);
					}
					
					echo json_encode($response);
					break;

				case 'DELETE':
					$this->user_role_m->delete($user_role_id);

					$response = array(
						'successful' => true,
						'message'    => $this->messages['delete']['success']
					);

					echo json_encode($response);
					break;
			}
		}
	}
}