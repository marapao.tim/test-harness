<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

	public $messages = array(
		'add' => array(
			'success'  => 'User Role added successfully.',
			'failed'   => ''
		),
		'edit' => array(
			'success' => 'User Role updated successfully.',
			'failed'  => ''
		),
		'delete' => array(
			'success' => 'User Role deleted successfully.',
			'failed'  => ''
		),
		'delete_all' => array(
			'success' => 'All User Roles were deleted successfully.',
			'failed'  => ''
		)
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('user_role_m');
	}

	public function index() {
		
	}

	public function fetch($environment, $type,$accountid,$siteid){
		$this->load->library('session');
		header("Content-Type: application/json");
		$headers = $this->clientRequest();
		//var_dump($headers);
		$env = array(
			"dev"=>"http://dev-customer.igentechnologies.net/public/?action=".$type."&accountid=".$accountid."&siteid=".$siteid,
			"qa"=>"http://qa2-customer.igentechnologies.net/public/?action=".$type."&accountid=".$accountid."&siteid=".$siteid,
			"uat"=>"http://uat2-customer.igentechnologies.net/public/?action=".$type."&accountid=".$accountid."&siteid=".$siteid,
			"prod"=>"https://customer2.serino.com/public/?action=".$type."&accountid=".$accountid."&siteid=".$siteid,
		);

		$context = stream_context_create(array(
			'http' => array(
				// http://www.php.net/manual/en/context.http.php
				'method' => 'GET',
				'header' => "Authorization: ".$headers["Authorization"]."\r\n".
					"Content-Type: application/json\r\n".
					"Accept: application/json\r\n"
			)
		));

		$response = @file_get_contents($env[$environment], FALSE, $context);

		echo $response;
	}

	public function clientRequest() {

	    $headers=array();
	    foreach (getallheaders() as $name => $value) {
	        $headers[$name] = $value;
	    }

	    return $headers;
	}
}