<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permissions extends CI_Controller {

	public $messages = array(
		'add' => array(
			'success'  => 'Permission added successfully.',
			'failed'   => ''
		),
		'edit' => array(
			'success' => 'Permission updated successfully.',
			'failed'  => ''
		),
		'delete' => array(
			'success' => 'Permission deleted successfully.',
			'failed'  => ''
		),
		'delete_all' => array(
			'success' => 'All Permissions were deleted successfully.',
			'failed'  => ''
		)
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('permission_m');
	}

	public function index($permission_id = null) {
		$this->load->helper('array');

		if($permission_id == null) {
			switch ($this->input->server('REQUEST_METHOD')) {
				case "GET":
					$permissions = $this->permission_m->getPermissions();

					$response = array(
						'count' 		=> count($permissions),
						'items' 		=> $permissions,
						'successful'	=> true
					);

					echo json_encode($response);
					break;

				case "POST":
					$_POST['permission_code'] = $this->permission_m->clean_string($this->input->post('permission_title'));
					$this->load->library('form_validation');
					$this->form_validation->set_rules('permission_title', 'title', 'required');
					$this->form_validation->set_rules('permission_group', 'group', 'required');
					$this->form_validation->set_rules('permission_code', 'title', 'is_unique[srn_permissions.permission_code]');

					if(!$this->form_validation->run()) {
						$response = array(
							'successful' => false,
							'message'    => validation_errors('<div>', '</div>')
						);

						echo json_encode($response);
						die();
					}

					

					if($this->permission_m->save($this->input->post())) {
						$response = array(
							'successful' 	=> true,
							'permission_id'	=> $this->db->insert_id(),
							'message'    	=> $this->messages['add']['success']
						);
					} else {
						$response = array(
							'successful' => false,
							'message'    => $this->messages['add']['failed']
						);
					}

					$this->session->set_flashdata('alert_type', $response['successful']);
					$this->session->set_flashdata('alert_message', $response['message']);
					echo json_encode($response);
					break;			
			}
		} else {
			$permission = $this->permission_m->get($permission_id);
				
			if(count($permission) <= 0) {
				$response = array(
					'count' => 0,
					'items' => array(),
					'successful' => false,
					'message' => 'Permission doesn\'t exists' 
				);

				echo json_encode($response);
				die();
			}
			switch($this->input->server('REQUEST_METHOD')) {
				case 'GET':
					$response = array(
						'count' => 1,
						'model' => $permission,
						'successful' => true
					);

					echo json_encode($response);

					break;

				case 'POST':
					$_POST['permission_code'] = $this->permission_m->clean_string($this->input->post('permission_title'));
					$this->load->library('form_validation');
					$this->form_validation->set_rules('permission_title', 'title', 'required');
					$this->form_validation->set_rules('permission_group', 'group', 'required');
					if($this->input->post('permission_code') != $permission->permission_code) {
						$this->form_validation->set_rules('permission_code', 'title', 'is_unique[srn_permissions.permission_code]');
					}

					if(!$this->form_validation->run()) {
						$response = array(
							'successful' => false,
							'message'    => validation_errors('<div>', '</div>')
						);

						echo json_encode($response);
						die();
					}

					if($this->permission_m->save($this->input->post(), $permission_id)) {
						$response = array(
							'successful' => true,
							'message'    => $this->messages['edit']['success']
						);
					} else {
						$response = array(
							'successful' => false,
							'message'    => $this->messages['edit']['failed']
						);
					}
					
					echo json_encode($response);
					break;

				case 'DELETE':
					$this->permission_m->delete($permission_id);

					$response = array(
						'successful' => true,
						'message'    => $this->messages['delete']['success']
					);

					echo json_encode($response);
					break;
			}
		}
	}
}