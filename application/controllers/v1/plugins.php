<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Plugins extends CI_Controller {

	public $messages = array(
		'add' => array(
			'success'  => 'Plugin added successfully.',
			'failed'   => ''
		),
		'edit' => array(
			'success' => 'Plugin updated successfully.',
			'failed'  => ''
		),
		'update' => array(
			'success' => 'Plugin version updated successfully.',
			'failed'  => ''
		),
		'delete' => array(
			'success' => 'Plugin deleted successfully.',
			'failed'  => ''
		),
		'delete_all' => array(
			'success' => 'All plugins were deleted successfully.',
			'failed'  => ''
		)
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('user_m');
		$this->load->model('plugin_m');

		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
	}

	public function index($plugin_id = null) {
		$this->load->helper('array');
		if($plugin_id == null) {
			switch ($this->input->server('REQUEST_METHOD')) {

				/** GET ALL PLUGINS */

				case 'GET':
					$plugins = $this->plugin_m->get();

					$response = array(
						'count' => count($plugins),
						'items' => $plugins,
						'successful' => true
					);

					echo json_encode($response);

					break;				

				/** ADD NEW PLUGIN */

				case 'POST':
					//validation
					$this->load->library('form_validation');
					$this->form_validation->set_rules('title', 'title', 'required');
					$this->form_validation->set_rules('category', 'category', 'required');

					$this->form_validation->set_rules('cms_support', 'cms support', 'required');

					//validate fields
					if(!$this->form_validation->run()) {
						$response = array(
							'successful' => false,
							'message'    => validation_errors('<div>', '</div>')
						);

						echo json_encode($response);
						die();
					}

					//validate if file exists
					if($_FILES['plugin_file']['name'] == '') {
						$response = array(
							'successful' => false,
							'message'    => 'Please select a file'
						);

						echo json_encode($response);
						die();
					}

					//construct plugin cms id
					$this->db->order_by('plugin_id', 'DESC');
					$this->db->limit(1);
					$last_id = $this->plugin_m->get();

					if(count($last_id)) {
						$last_id = ($last_id[0]->plugin_id + 1);
					} else {
						$last_id = 1;
					}

					$plugin_cms_id = str_pad($last_id, 10, '0',STR_PAD_LEFT).'-'.$this->plugin_m->clean_string($this->input->post('title'));

					//version meta
		 			$version_meta = array(
		 				"1.00" => array(
		 					'release_date' => date('Y-m-d H:i:s'),
		 					'version'      => '1.00',
		 					'filename'     => $plugin_cms_id.'.zip',
		 					'cms_support'  => implode(',',$this->input->post('cms_support')),
		 					'remarks'      => $this->input->post('version_remarks'),
		 					'updated_by'   => $this->session->userdata('user_fullname')
		 				)
		 			);

					$_POST = elements(array(
						'title',
						'author',
						'category',
						'version',
						'tags',
						'description'
					),$_POST);

					//clear entities
					//$_POST['tool_description'] = htmlentities($this->input->post('tool_description'));

					$_POST['plugin_cms_id'] = $plugin_cms_id;
					$_POST['version'] = '1.00';
					$_POST['version_metas'] = serialize($version_meta);

			 		//create dir
			 		$plugins_path = FCPATH.'store/plugins';
			 		$plugin_path = $plugins_path.'/'.$plugin_cms_id;
			 		$extract_path = $plugin_path.'/'.$_POST['version'];

			 		file_exists($plugins_path) || mkdir($plugins_path,0777);
			 		file_exists($plugin_path) || mkdir($plugin_path,0777);
					file_exists($extract_path) || mkdir($extract_path,0777);

					//construct name then upload
					$filepath = $extract_path.'/'.$plugin_cms_id.'.zip';
					move_uploaded_file($_FILES['plugin_file']['tmp_name'], $filepath);

					//unzip the file
					$zip = new ZipArchive;
					$open_zip = $zip->open($filepath);
					if ($open_zip === TRUE) {
						$zip->extractTo($extract_path);
						$zip->close();
					} else {
						$response = array(
							'successful' => false,
							'message'    => 'Problem unzipping the file'
						);

						echo json_encode($response);
						die();
					}

					if($this->plugin_m->save($this->input->post())) {
						$response = array(
							'successful' => true,
							'plugin_id'    => $this->db->insert_id(),
							'message'    => $this->messages['add']['success']
						);
					} else {
						$response = array(
							'successful' => false,
							'message'    => $this->messages['add']['failed']
						);
					}

					$this->session->set_flashdata('alert_type', $response['successful']);
					$this->session->set_flashdata('alert_message', $response['message']);
					echo json_encode($response);
					break;				

				/** DELETE ALL PLUGINS */

				case 'DELETE':
					$this->db->empty_table('srn_plugins');
					
					$response = array(
						'successful' => true,
						'message'    => $this->messages['delete_all']['success']
					);

					echo json_encode($response);

					break;
				
				default:
					# code...
					break;
			}
		} else {
			$plugin = $this->plugin_m->get($plugin_id);

			if(count($plugin) <= 0) {
				$response = array(
					'count' => 0,
					'items' => array(),
					'successful' => false,
					'message' => 'Plugin doesn\'t exists' 
				);

				echo json_encode($response);
				die();
			}

			switch ($this->input->server('REQUEST_METHOD')) {

				/** GET PLUGIN USING ID */

				case 'GET':
					
					$plugin->version_metas = unserialize($plugin->version_metas);

					$response = array(
						'count' => 1,
						'model' => $plugin,
						'successful' => true
					);

					echo json_encode($response);

					break;		

				/** UPDATE/EDIT PLUGIN BY ID */		

				case 'POST':
					if($this->uri->segment(4) == 'update') {
						//validation
						$this->load->library('form_validation');
						$this->form_validation->set_rules('cms_support', 'cms support', 'required');
						$this->form_validation->set_rules('version', 'new version', 'required|numeric|greater_than['.$plugin->version.']');

						//validate fields
						if(!$this->form_validation->run()) {
							$response = array(
								'successful' => false,
								'message'    => validation_errors('<div>', '</div>')
							);

							echo json_encode($response);
							die();
						}

						//validate if file exists
						if($_FILES['plugin_file']['name'] == '') {
							$response = array(
								'successful' => false,
								'message'    => 'Please select a file'
							);

							echo json_encode($response);
							die();
						}

						//update version metas
						$version_metas = unserialize($plugin->version_metas);
						$new_version = number_format($this->input->post('version'), 2);

				 		$version_metas[$new_version] = array(
							'release_date' => date('Y-m-d H:i:s'),
							'version'      => $new_version,
							'filename'     => $plugin->plugin_cms_id.'.zip',
							'cms_support'  => implode(',',$this->input->post('cms_support')),
							'remarks'      => $this->input->post('version_remarks'),
							'updated_by'   => $this->session->userdata('user_fullname')
						);

				 		$new_plugin_data = array(
				 			'version' => $new_version,
				 			'version_metas' => serialize($version_metas)
				 		);

				 		//create dir
				 		$plugins_path = FCPATH.'store/plugins';
				 		$plugin_path = $plugins_path.'/'.$plugin->plugin_cms_id;
				 		$extract_path = $plugin_path.'/'.$new_version;

						file_exists($extract_path) || mkdir($extract_path,0777);

						//construct name then upload
						$filepath = $extract_path.'/'.$plugin->plugin_cms_id.'.zip';
						move_uploaded_file($_FILES['plugin_file']['tmp_name'], $filepath);

						//unzip the file
						$zip = new ZipArchive;
						$open_zip = $zip->open($filepath);
						if ($open_zip === TRUE) {
							$zip->extractTo($extract_path);
							$zip->close();
						} else {
							$response = array(
								'successful' => false,
								'message'    => 'Problem unzipping the file'
							);

							echo json_encode($response);
							die();
						}

						if($this->plugin_m->save($new_plugin_data, $plugin_id)) {
							$response = array(
								'successful' => true,
								'message'    => $this->messages['update']['success']
							);
						} else {
							$response = array(
								'successful' => false,
								'message'    => $this->messages['update']['failed']
							);
						}

						$this->session->set_flashdata('alert_type', $response['successful']);
						$this->session->set_flashdata('alert_message', $response['message']);
						echo json_encode($response);

					} else {
						//validation
						$this->load->library('form_validation');
						$this->form_validation->set_rules('title', 'title', 'required');
						$this->form_validation->set_rules('category', 'category', 'required');

						//validate fields
						if(!$this->form_validation->run()) {
							$response = array(
								'successful' => false,
								'message'    => validation_errors('<div>', '</div>')
							);

							echo json_encode($response);
							die();
						}

						$_POST = elements(array(
							'title',
							'author',
							'category',
							'tags',
							'description'
						),$_POST);

						//clear entities
						//$_POST['tool_description'] = htmlentities($this->input->post('tool_description'));

						if($this->plugin_m->save($this->input->post(), $plugin_id)) {
							$response = array(
								'successful' => true,
								'message'    => $this->messages['edit']['success']
							);
						} else {
							$response = array(
								'successful' => false,
								'message'    => $this->messages['edit']['failed']
							);
						}

						echo json_encode($response);
					}

					break;

				/** DELETE PLUGIN BY ID */	

				case 'DELETE':
					$plugin_dir_path = FCPATH.'store/plugins/'.$plugin->plugin_cms_id;
					$this->plugin_m->_delete_directory($plugin_dir_path);

					$this->plugin_m->delete($plugin_id);

					$response = array(
						'successful' => true,
						'message'    => $this->messages['delete']['success']
					);

					$this->session->set_flashdata('alert_type', $response['successful']);
					$this->session->set_flashdata('alert_message', $response['message']);
					echo json_encode($response);
					break;
				
				default:
					# code...
					break;
			}
		}
	}

	public function update($skip = 0, $limit = 10) {
		$plugins = $this->db->query('SELECT * FROM srn_plugins 
			WHERE FIND_IN_SET(`plugin_cms_id`, "'.$this->input->post('plugin_cms_ids').'")
			LIMIT '.$limit.' OFFSET '.$skip)->result();

		foreach ($plugins as $key => $plugin) {
			$plugin->version_metas = unserialize($plugin->version_metas);
		}

		$response = array(
			'count' => count($plugins),
			'items' => $plugins,
			'successful' => true
		);

		echo json_encode($response);
	}
}
