<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Themes extends CI_Controller {

	public $messages = array(
		'add' => array(
			'success'  => 'Theme added successfully.',
			'failed'   => ''
		),
		'edit' => array(
			'success' => 'Theme updated successfully.',
			'failed'  => ''
		),
		'update' => array(
			'success' => 'Theme version updated successfully.',
			'failed'  => ''
		),
		'delete' => array(
			'success' => 'Theme deleted successfully.',
			'failed'  => ''
		),
		'delete_all' => array(
			'success' => 'All themes were deleted successfully.',
			'failed'  => ''
		)
	);

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_m');
		$this->load->model('theme_m');

		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
	}

	public function index($theme_id = null) {
		$this->load->helper('array');
		if($theme_id == null) {
			switch ($this->input->server('REQUEST_METHOD')) {

				/** GET ALL THEMES */

				case 'GET':

					if($this->session->userdata('user_id')) {
						if(!$this->user_m->checkIfAdmin($this->session->userdata('user_id'))) {
							$this->db->where('created_by', $this->session->userdata('user_id'));
						}
					}

					$themes = $this->theme_m->get();

					$response = array(
						'count' => count($themes),
						'items' => $themes,
						'successful' => true
					);

					echo json_encode($response);

					break;				

				/** ADD NEW THEME */

				case 'POST':
					//validation
					$this->load->library('form_validation');
					$this->form_validation->set_rules('theme_title', 'title', 'required');
					$this->form_validation->set_rules('theme_category', 'category', 'required');
					$this->form_validation->set_rules('cms_support', 'cms support', 'required');

					//validate fields
					if(!$this->form_validation->run()) {
						$response = array(
							'successful' => false,
							'message'    => validation_errors('<div>', '</div>')
						);

						echo json_encode($response);
						die();
					}

					//validate if file exists
					if($_FILES['theme_file']['name'] == '') {
						$response = array(
							'successful' => false,
							'message'    => 'Please select a file'
						);

						echo json_encode($response);
						die();
					}

					$this->db->order_by('theme_id', 'DESC');
					$this->db->limit(1);
					$last_id = $this->theme_m->get();

					if(count($last_id)) {
						$last_id = ($last_id[0]->theme_id + 1);
					} else {
						$last_id = 1;
					}

					$theme_cms_id = str_pad($last_id, 10, '0',STR_PAD_LEFT).'-'.$this->theme_m->clean_string($this->input->post('theme_title'));

					//version meta
		 			$version_meta = array(
		 				"1.00" => array(
		 					'release_date' => date('Y-m-d H:i:s'),
		 					'version'      => '1.00',
		 					'filename'     => $theme_cms_id.'.zip',
		 					'cms_support'  => implode(',',$this->input->post('cms_support')),
		 					'remarks'      => 'Initial Release',
		 					'updated_by'   => $this->session->userdata('user_id')
		 				)
		 			);

					$_POST = elements(array(
						'theme_title',
						'theme_author',
						'theme_category',
						'theme_version',
						'theme_tags',
						'theme_description'
					),$_POST);

					//clear entities
					//$_POST['theme_description'] = htmlentities($this->input->post('theme_description'));

					$_POST['theme_cms_id'] = $theme_cms_id;
					$_POST['theme_version'] = '1.00';
					$_POST['version_metas'] = serialize($version_meta);

			 		//create dir
			 		$themes_path = FCPATH.'store/themes';
			 		$theme_path = $themes_path.'/'.$theme_cms_id;
			 		$extract_path = $theme_path.'/'.$_POST['theme_version'];

			 		file_exists($themes_path) || mkdir($themes_path,0777);
			 		file_exists($theme_path) || mkdir($theme_path,0777);
					file_exists($extract_path) || mkdir($extract_path,0777);

					//construct name then upload
					$filepath = $extract_path.'/'.$theme_cms_id.'.zip';
					move_uploaded_file($_FILES['theme_file']['tmp_name'], $filepath);

					//unzip the file
					$zip = new ZipArchive;
					$open_zip = $zip->open($filepath);
					if ($open_zip === TRUE) {
						$zip->extractTo($extract_path);
						$zip->close();
					} else {
						$response = array(
							'successful' => false,
							'message'    => 'Problem unzipping the file'
						);

						echo json_encode($response);
						die();
					}

					if($this->theme_m->save($this->input->post())) {
						$response = array(
							'successful' => true,
							'theme_id'    => $this->db->insert_id(),
							'message'    => $this->messages['add']['success']
						);
					} else {
						$response = array(
							'successful' => false,
							'message'    => $this->messages['add']['failed']
						);
					}

					$this->session->set_flashdata('alert_type', ($response['successful']) ? 'success' : 'danger');
					$this->session->set_flashdata('alert_message', $response['message']);
					echo json_encode($response);
					break;				

				/** DELETE ALL THEMES */

				case 'DELETE':
					$this->db->empty_table('srn_themes');
					
					$response = array(
						'successful' => true,
						'message'    => $this->messages['delete_all']['success']
					);

					echo json_encode($response);

					break;
				
				default:
					# code...
					break;
			}
		} else {
			$theme = $this->theme_m->get($theme_id);

			if(count($theme) <= 0) {
				$response = array(
					'count' => 0,
					'items' => array(),
					'successful' => false,
					'message' => 'Theme doesn\'t exists' 
				);

				echo json_encode($response);
				die();
			}

			switch ($this->input->server('REQUEST_METHOD')) {

				/** GET THEME USING ID */

				case 'GET':
					
					$theme->version_metas = unserialize($theme->version_metas);

					$response = array(
						'count' => 1,
						'model' => $theme,
						'successful' => true
					);

					echo json_encode($response);

					break;		

				/** UPDATE/EDIT THEME BY ID */		

				case 'POST':
					if($this->uri->segment(4) == 'update') {
						//validation
						$this->load->library('form_validation');
						$this->form_validation->set_rules('cms_support', 'cms support', 'required');
						$this->form_validation->set_rules('theme_version', 'new version', 'required|numeric|greater_than['.$theme->theme_version.']');

						//validate fields
						if(!$this->form_validation->run()) {
							$response = array(
								'successful' => false,
								'message'    => validation_errors('<div>', '</div>')
							);

							echo json_encode($response);
							die();
						}

						//validate if file exists
						if($_FILES['theme_file']['name'] == '') {
							$response = array(
								'successful' => false,
								'message'    => 'Please select a file'
							);

							echo json_encode($response);
							die();
						}

						//update version metas
						$version_metas = unserialize($theme->version_metas);
						$new_version = number_format($this->input->post('theme_version'), 2);

				 		$version_metas[$new_version] = array(
								'release_date' => date('Y-m-d H:i:s'),
								'version'      => $new_version,
								'filename'     => $theme->theme_cms_id.'.zip',
								'cms_support'  => implode(',',$this->input->post('cms_support')),
								'remarks'      => $this->input->post('version_remarks'),
								'updated_by'   => $this->session->userdata('user_id')
						);

				 		$new_theme_data = array(
				 			'theme_version' => $new_version,
				 			'version_metas' => serialize($version_metas)
				 		);

				 		//create dir
				 		$themes_path = FCPATH.'store/themes';
				 		$theme_path = $themes_path.'/'.$theme->theme_cms_id;
				 		$extract_path = $theme_path.'/'.$new_version;

						file_exists($extract_path) || mkdir($extract_path,0777);

						//construct name then upload
						$filepath = $extract_path.'/'.$theme->theme_cms_id.'.zip';
						move_uploaded_file($_FILES['theme_file']['tmp_name'], $filepath);

						//unzip the file
						$zip = new ZipArchive;
						$open_zip = $zip->open($filepath);
						if ($open_zip === TRUE) {
							$zip->extractTo($extract_path);
							$zip->close();
						} else {
							$response = array(
								'successful' => false,
								'message'    => 'Problem unzipping the file'
							);

							echo json_encode($response);
							die();
						}

						if($this->theme_m->save($new_theme_data, $theme_id)) {
							$response = array(
								'successful' => true,
								'message'    => $this->messages['update']['success']
							);
						} else {
							$response = array(
								'successful' => false,
								'message'    => $this->messages['update']['failed']
							);
						}

						$this->session->set_flashdata('alert_type', ($response['successful']) ? 'success' : 'danger');
						$this->session->set_flashdata('alert_message', $response['message']);
						echo json_encode($response);

					} else {
						//validation
						$this->load->library('form_validation');
						$this->form_validation->set_rules('theme_title', 'title', 'required');
						$this->form_validation->set_rules('theme_category', 'category', 'required');

						//validate fields
						if(!$this->form_validation->run()) {
							$response = array(
								'successful' => false,
								'message'    => validation_errors('<div>', '</div>')
							);

							echo json_encode($response);
							die();
						}

						$_POST = elements(array(
							'theme_title',
							'theme_author',
							'theme_category',
							'theme_tags',
							'theme_description'
						),$_POST);

						//clear entities
						//$_POST['theme_description'] = htmlentities($this->input->post('theme_description'));

						if($this->theme_m->save($this->input->post(), $theme_id)) {
							$response = array(
								'successful' => true,
								'message'    => $this->messages['edit']['success']
							);
						} else {
							$response = array(
								'successful' => false,
								'message'    => $this->messages['edit']['failed']
							);
						}
						$this->session->set_flashdata('alert_type', ($response['successful']) ? 'success' : 'danger');
						$this->session->set_flashdata('alert_message', $response['message']);
						echo json_encode($response);
					}

					break;

				/** DELETE THEME BY ID */	

				case 'DELETE':
					if(!$this->user_m->checkIfAdmin($this->session->userdata('user_id'))) {
						if($theme->created_by !== $this->session->userdata('user_id')) {
							$response = array(
								'successful' => false,
								'message'    => 'Not Allowed.'
							);
							$this->session->set_flashdata('alert_type', ($response['successful']) ? 'success' : 'danger');
							$this->session->set_flashdata('alert_message', $response['message']);
							echo json_encode($response);
							die();
						}
					}

					$theme_dir_path = FCPATH.'store/themes/'.$theme->theme_cms_id;
					$this->theme_m->_delete_directory($theme_dir_path);

					$this->theme_m->delete($theme_id);

					$response = array(
						'successful' => true,
						'message'    => $this->messages['delete']['success']
					);

					$this->session->set_flashdata('alert_type', ($response['successful']) ? 'success' : 'danger');
					$this->session->set_flashdata('alert_message', $response['message']);
					echo json_encode($response);
					break;
				
				default:
					# code...
					break;
			}
		}
	}

	public function update($skip = 0, $limit = 10) {
		$themes = $this->db->query('SELECT * FROM srn_themes 
			WHERE FIND_IN_SET(`theme_cms_id`, "'.$this->input->post('theme_cms_ids').'")
			LIMIT '.$limit.' OFFSET '.$skip)->result();

		foreach ($themes as $key => $theme) {
			$theme->version_metas = unserialize($theme->version_metas);
		}

		$response = array(
			'count' => count($themes),
			'items' => $themes,
			'successful' => true
		);

		echo json_encode($response);
	}
}
