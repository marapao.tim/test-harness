<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms extends CI_Controller {

	public $messages = array(
		'add' => array(
			'success'  => 'CMS added successfully.',
			'failed'   => ''
		),
		'edit' => array(
			'success' => 'CMS updated successfully.',
			'failed'  => ''
		),
		'delete' => array(
			'success' => 'CMS deleted successfully.',
			'failed'  => ''
		)
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('user_m');
		$this->load->model('cms_m');

		header('Access-Control-Allow-Origin: *');
	}

	public function index($cms_id = null) {
		$this->load->helper('array');
		if($cms_id == null) {
			switch ($this->input->server('REQUEST_METHOD')) {

				/** GET ALL CMS */

				case 'GET':
					$cms = $this->cms_m->get();

					foreach($cms as $key=>$value){
						$cms[$key]->file_path = base_url('store/cms/'.$cms[$key]->version.'.zip');
					}
					$response = array(
						'count' => count($cms),
						'items' => $cms,
						'successful' => true
					);

					echo json_encode($response);
					break;				

				/** ADD NEW CMS */

				case 'POST':
					//validation
					$this->load->library('form_validation');
					$this->form_validation->set_rules('codename', 'codename', 'required|is_unique[srn_cms.codename]');
					$this->form_validation->set_rules('version', 'version', 'required|numeric|is_unique[srn_cms.version]');

					if(!$this->form_validation->run()) {
						$response = array(
							'successful' => false,
							'message'    => validation_errors('<div>', '</div>')
						);

						echo json_encode($response);
						die();
					}

					if($_FILES['cms_file']['name'] == '') {
						$response = array(
							'successful' => false,
							'message'    => 'Please select a file'
						);

						echo json_encode($response);
						die();
					}

					$_POST = elements(array(
						'codename',
						'version',
						'release_notes'
					),$_POST);

					//clear entities
					//$_POST['release_notes'] = htmlentities($this->input->post('release_notes'));

			 		//create dir
			 		$cms_path = FCPATH.'store/cms';

			 		file_exists($cms_path) || mkdir($cms_path,0777);

					//construct name then upload
					$filepath = $cms_path.'/'.number_format($this->input->post('version'), 2).'.zip';
					move_uploaded_file($_FILES['cms_file']['tmp_name'], $filepath);

					if($this->cms_m->save($this->input->post())) {
						$response = array(
							'successful' => true,
							'model'      => array(
									'cms_id' => $this->db->insert_id()
								),
							'message'    => $this->messages['add']['success']
						);
					} else {
						$response = array(
							'successful' => false,
							'message'    => $this->messages['add']['failed']
						);
					}

					$this->session->set_flashdata('alert_type', $response['successful']);
					$this->session->set_flashdata('alert_message', $response['message']);
					echo json_encode($response);
					break;					
				
				default:
					# code...
					break;
			}
		} else {
			$cms = $this->cms_m->get($cms_id);
			$cms->file_path = base_url('store/cms/'.$cms->version.'.zip');
			if(count($cms) <= 0) {
				$response = array(
					'count' => 0,
					'successful' => false,
					'message' => 'CMS doesn\'t exists' 
				);

				echo json_encode($response);
				die();
			}

			switch ($this->input->server('REQUEST_METHOD')) {

				/** GET CMS USING ID */

				case 'GET':
					
					$response = array(
						'count' => 1,
						'model' => $cms,
						'successful' => true
					);

					echo json_encode($response);
					break;		

				case 'DELETE':
					$file_path = FCPATH.'store/cms/'.$cms->version.'.zip';
					@unlink($file_path);

					$this->cms_m->delete($cms_id);

					$response = array(
						'successful' => true,
						'message'    => $this->messages['delete']['success']
					);

					echo json_encode($response);
					break;
				
				default:
					# code...
					break;
			}
		}
	}

	public function update() {
		$this->db->order_by('cms_id', 'DESC');
		$this->db->limit(1);
		$cms = $this->db->get('srn_cms')->row();
		$cms->file_path = base_url('store/cms/'.$cms->version.'.zip');

		echo json_encode($cms);
	}
}
