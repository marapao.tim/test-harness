<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jmeter extends CI_Controller {

	public $messages = array(
		'add' => array(
			'success'  => 'CMS added successfully.',
			'failed'   => ''
		),
		'edit' => array(
			'success' => 'CMS updated successfully.',
			'failed'  => ''
		),
		'delete' => array(
			'success' => 'CMS deleted successfully.',
			'failed'  => ''
		)
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('user_m');
		$this->load->model('jmeter_testrunners_m');
		$this->load->model('cms_m');

		header('Access-Control-Allow-Origin: *');
	}

	public function index($cms_id = null) {
		
	}

	public function get_runners($hour=0,$week=0,$month=0) {
		$ret = $this->jmeter_testrunners_m->get_runner_api($hour,$week,$month);

		echo json_encode($ret);
	}

	public function refresh_runner($id){
		$ret = $this->jmeter_testrunners_m->refresh_runner($id);
	}

	public function runner_history($id){
		$ret = $this->jmeter_testrunners_m->create_runner_history($id);
	}
}
