<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logs extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index() {
		$this->view_logs();
	}

	public function view_logs() {
		$page_data['title'] = 'View System Logs';
		$page_data['subview'] = 'logs/view_logs_v';
		$page_data['page_header_data'] = array('title' => '<i class="ion-clipboard"></i> View Logs', 'description' => '');

		$page_data['logs'] = $this->logs_m->get();

		$page_data['styles'] = array('plugins/datatables/datatables.min');
		$page_data['scripts'] = array('plugins/datatables/datatables.min','custom/view.logs');

		$this->load->view('layouts/main_layout', $page_data);
	}	

	public function view_log($log_id) {
		$page_data['title'] = 'View Log';
		$page_data['subview'] = 'logs/view_log_v';
		$page_data['page_header_data'] = array('title' => 'View Log - '.$log_id, 'description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('logs').'"><i class="ion-ios-arrow-thin-left"></i> Back to Logs</a>');
		$page_data['log'] = $this->logs_m->get($log_id);

		$this->load->view('layouts/main_layout', $page_data);
	}
}