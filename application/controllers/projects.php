<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projects extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);

		$this->load->model('user_m');
		$this->load->model('project_m');
		$this->load->model('unsafecrypto');
	}

	public function index() {
		$this->project_management();
	}

	/*********************************************/

	#PROJECT MANAGEMENT

	/*********************************************/

	public function project_management() {
		$page_data['title'] = 'Projects Management';
		$page_data['subview'] = 'projects/project_management_v';
		$page_data['page_header_data'] = array('title' => '<i class="ion-easel"></i> Project Management', 'description' => 'You can manage your Serino Project Initialization here.');

		$page_data['projects'] = $this->project_m->get();

		$page_data['styles'] = array('plugins/datatables/dataTables.bootstrap');
		$page_data['scripts'] = array('plugins/datatables/jquery.dataTables.min','plugins/datatables/dataTables.bootstrap.min','plugins/quicksearch/jquery.quicksearch','custom/projects.management');
		$this->load->view('layouts/main_layout', $page_data);
	}

	public function view_project($project_id) {
		$this->load->model('theme_m');
		$this->load->model('tool_m');

		$this->load->helper(array('array'));

		$project = $this->project_m->get($project_id);
		$page_data['title'] = 'View Project - '.$project->project_title;
		$page_data['subview'] = 'projects/view_project_v';
		$page_data['page_header_data'] = array('title' => 'View Project - '.$project->project_title, 'description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('projects/project_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to Project Management</a>');
		$page_data['project'] = $project;
		$page_data['project']->config_meta = unserialize($page_data['project']->config_meta);
		$page_data['themes'] = $this->theme_m->get();
		$page_data['tools'] = $this->tool_m->get();
		$page_data['timezones'] = $this->timezone_list();
		$page_data['token'] = $this->unsafecrypto->generateToken();

		$page_data['artifacts'] = $this->project_m->projectArtifacts($project_id);

		$page_data['styles'] = array('plugins/datatables/dataTables.bootstrap','plugins/multiselect/css/multi-select','plugins/jasny-bootstrap/css/jasny-bootstrap.min','plugins/select2/select2.min');
		$page_data['scripts'] = array('plugins/datatables/jquery.dataTables.min','plugins/datatables/dataTables.bootstrap.min','plugins/parsley/parsley.config','plugins/parsley/parsley.min','plugins/quicksearch/jquery.quicksearch','plugins/multiselect/js/jquery.multi-select','plugins/jasny-bootstrap/js/jasny-bootstrap.min','plugins/select2/select2.full.min','custom/view.project');
		$this->load->view('layouts/main_layout', $page_data);
	}

	public function project_tasks_management($project_id) {
		$this->load->helper(array('array'));

		$project = $this->project_m->get($project_id);
		$page_data['title'] = 'Project Tasks Management - '.$project->project_title;
		$page_data['subview'] = 'projects/task_project_v';
		$page_data['page_header_data'] = array('title' => 'Project Tasks Management - '.$project->project_title, 'description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('projects/project_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to Project Management</a>');
		$page_data['project'] = $project;
		$page_data['project']->config_meta = unserialize($page_data['project']->config_meta);
		$page_data['timezones'] = $this->timezone_list();
		$page_data['token'] = $this->unsafecrypto->generateToken();

		$page_data['styles'] = array('plugins/datatables/dataTables.bootstrap','plugins/multiselect/css/multi-select','plugins/jasny-bootstrap/css/jasny-bootstrap.min','plugins/select2/select2.min');
		$page_data['scripts'] = array('plugins/datatables/jquery.dataTables.min','plugins/datatables/dataTables.bootstrap.min','plugins/parsley/parsley.config','plugins/parsley/parsley.min','plugins/jasny-bootstrap/js/jasny-bootstrap.min','plugins/select2/select2.full.min','custom/tasks.project');
		$this->load->view('layouts/main_layout', $page_data);
	}

	public function save_project($project_id = NULL) {
		if(empty($_POST)) {
			$page_data['title'] = ($project_id) ? 'Edit Project' : 'Save Project';
			$page_data['subview'] = 'projects/save_project_v';
			$page_data['page_header_data'] = array('description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('projects/project_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to Project Management</a>');
			$page_data['styles'] = array('plugins/multiselect/css/multi-select');
			$page_data['scripts'] = array('plugins/parsley/parsley.config','plugins/parsley/parsley.min','plugins/quicksearch/jquery.quicksearch','plugins/multiselect/js/jquery.multi-select','custom/save.project');

			if($project_id) {
				$page_data['page_header_data']['title'] = 'Edit Project';
				$page_data['project'] = $this->project_m->get($project_id);
			} else {
				$page_data['page_header_data']['title'] = 'Add Project';

				$page_data['project'] = $this->project_m->get_new();
			}

			$this->load->view('layouts/main_layout', $page_data);
		} else {
			$this->project_m->save_project($project_id);
		}
	}

	public function delete_project($project_id) {
		$project = $this->project_m->get($project_id);

		$this->project_m->delete($project_id);

		$this->session->set_flashdata('alert', 'Project successfully deleted.');
		$this->session->set_flashdata('alert_type','success');

		$this->logs_m->save_log('Delete Project', serialize($project));

		redirect('projects/project_management','refresh');
	}

	public function download_project($project_id) {
		//generate srn config
		$project = $this->project_m->get($project_id);
		$page_data['project']->config_meta = unserialize($page_data['project']->config_meta);
	}

	/*********************************************/

	#AJAX CALLS

	/*********************************************/

	function project_name_exists($id = false) {
		if($id) {
			$this->db->where('project_id != ', $id);
		}

		$query = $this->project_m->get_by(array('project_name' => $this->input->post('project_name')));

		if(count($query)) {
			echo 0;
		} else {
			echo 1;
		}
		die();
	}

	function upload_project_logo($project_id) {
		$this->initialize_project($project_id);

		$project = $this->project_m->get($project_id);

		$config['upload_path'] = FCPATH.'store/projects/'.$project->project_name.'/images/orig/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '0';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$config['encrypt_name']  = true;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('project_logo'))
		{
			print_r($this->upload->display_errors());
		}
		else
		{
			$project_data['project_logo'] = $this->upload->data()['file_name'];
			
			include(APPPATH.'third_party/resize_class.php');

			$logo_150x150 = new resize($this->upload->data()['file_path'].$this->upload->data()['file_name']);
			$logo_150x150 -> resizeImage(150, 150, 'crop');
			if($logo_150x150 -> saveImage(FCPATH.'store/projects/'.$project->project_name.'/images/150x150/'.$project_data['project_logo'], 100)) {
				$this->project_m->save($project_data, $project_id);

				$data = array(
					'status' => true,
					'new_image' => $project_data['project_logo'],
					'project_name' => $project->project_name
				);

				$this->logs_m->save_log('Changed Project Logo', serialize($data));

				header('Content-Type: application/json');
				echo json_encode($data);
			} else {
				die('Error saving the image.');
			}
		}
	}

	function upload_new_project_artifact($project_id) {
		$this->initialize_project($project_id);

		$project = $this->project_m->get($project_id);
		//var_dump($project);die();
		$file_path = FCPATH.'store/project_artifacts/'.$project->project_name.'/';

		is_dir($file_path) || mkdir($file_path,0777,true);

		if($_FILES['artifact_file']['name'] == '') {
			$response = array(
				'successful' => false,
				'message'    => 'Please select a file'
			);

			echo json_encode($response);
			die();
		}

		$path = $_FILES['artifact_file']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);

		$date = new DateTime();
		$filepath = $file_path.'/'.$date->getTimestamp().'.'.$ext;
		move_uploaded_file($_FILES['artifact_file']['tmp_name'], $filepath);

		$query = array(
			"project_id"=>$project_id,
			"artifact_name"=>$this->input->post('artifact_name'),
			"artifact_description"=>$this->input->post('artifact_description'),
			"artifact_tags"=>$this->input->post('artifact_tags'),
			"url"=>'store/project_artifacts/'.$project->project_name.'/'.$date->getTimestamp().'.'.$ext
		);

		$this->project_m->createArtifact($query);

		echo json_encode(array("successful"=>true));
	}

	function upload_artifact_files($project_id, $artifact_id) {
		$this->initialize_project($project_id);

		$project = $this->project_m->get($project_id);
		//var_dump($project);die();
		$file_path = FCPATH.'store/project_artifacts/'.$project->project_name.'/';

		is_dir($file_path) || mkdir($file_path,0777,true);

		if($_FILES['artifact_file']['name'] == '') {
			$response = array(
				'successful' => false,
				'message'    => 'Please select a file'
			);

			echo json_encode($response);
			die();
		}

		$path = $_FILES['artifact_file']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);

		$date = new DateTime();
		$filepath = $file_path.'/'.$date->getTimestamp().'.'.$ext;
		move_uploaded_file($_FILES['artifact_file']['tmp_name'], $filepath);

		$v_major = $this->input->post('file_major_version');
		$v_minor = $this->input->post('file_minor_version');
		$v_patch = $this->input->post('file_patch_version');

		switch($this->input->post('update_version')){
			case "Major":
				$v_major+=1;
				$v_minor=0;
				$v_patch=0;
			break;
			case "Minor":
				$v_minor+=1;
				$v_patch=0;
			break;
			case "Patch":
				$v_patch+=1;
			break;
		}

		$query = array(
			"artifact_id"=>$artifact_id,
			"file_description"=>$this->input->post('file_description'),
			"file_tags"=>$this->input->post('file_tags'),
			"file_version"=>$v_major.".".$v_minor.".".$v_patch,
			"file_url"=>'store/project_artifacts/'.$project->project_name.'/'.$date->getTimestamp().'.'.$ext,
			"file_status"=>"Active",
			"file_security"=>serialize(array())
		);

		$this->project_m->addArtifactFile($query);

		echo json_encode(array("successful"=>true));
	}

	function view_artifacts($artifact_id,$file_id) {
		$file = $this->project_m->get_artifactFile($file_id);

		$user_role_id = $this->session->userdata('user_role_id');
		if($user_role_id >= 0){
			$mime = mime_content_type(FCPATH.$file->file_url);
			header('Content-Type: ' . $mime);
			include(FCPATH.$file->file_url);
		}
	}

	/*********************************************/

	#CONTROLLER FUNCTIONS

	/*********************************************/

	function timezone_list() {
	  $zones_array = array();
	  $timestamp = time();
	  foreach(timezone_identifiers_list() as $key => $zone) {
	    date_default_timezone_set($zone);
	    $zones_array[$key]['zone'] = $zone;
	    $zones_array[$key]['diff_from_GMT'] = 'UTC/GMT ' . date('P', $timestamp);
	  }
	  return $zones_array;
	}

	function initialize_project($project_id) {
		$project = $this->project_m->get($project_id);

		//create project directory
		$project_dir = FCPATH.'store/projects/'.$project->project_name;

		is_dir($project_dir) || mkdir($project_dir,0777,true);

		$project_artifacts_dir = FCPATH.'store/project_artifacts/'.$project->project_name;

		is_dir($project_artifacts_dir) || mkdir($project_artifacts_dir,0777,true);

		//compose project image folders
		$img_dir = $project_dir.'/images/'; //image
		$img_orig_dir = $img_dir.'orig'; //original
		$img_150x150_dir = $img_dir.'150x150'; //150x150

		is_dir($img_dir) || mkdir($img_dir);
		is_dir($img_orig_dir) || mkdir($img_orig_dir);
		is_dir($img_150x150_dir) || mkdir($img_150x150_dir);
	}
}
