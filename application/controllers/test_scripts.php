<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test_Scripts extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);

		$this->load->model('user_m');
		$this->load->model('jmeter_testscripts_m');
		$this->load->model('unsafecrypto');
	}

	public function index() {
		//$this->user_m->checkPermissions("add-test-script",true);
		$page_data['title'] = 'Test Script Management';
		$page_data['subview'] = 'jmeter/test_scripts/list_v';
		$page_data['page_header_data'] = array('title' => '<i class="ion-easel"></i> Test Script Management', 'description' => 'You can manage your Serino Test Scripts here.');

		$page_data['test_scripts'] = $this->jmeter_testscripts_m->get();

		$page_data['styles'] = array(
			'plugins/datatables/dataTables.bootstrap',
			'plugins/codemirror/codemirror'
		);
		$page_data['scripts'] = array(
			'plugins/datatables/jquery.dataTables.min',
			'plugins/codemirror/codemirror',
			'plugins/datatables/dataTables.bootstrap.min',
			'plugins/quicksearch/jquery.quicksearch',
			'custom/jmeter/test_script/index',
			'plugins/codemirror/mode/css/css',
			'plugins/codemirror/mode/htmlmixed/htmlmixed',
			'plugins/codemirror/mode/javascript/javascript',
			'plugins/codemirror/mode/htmlembedded/htmlembedded',
			'plugins/codemirror/mode/xml/xml',
			'plugins/codemirror/mode/php/php'
		);

		$this->load->view('layouts/main_layout', $page_data);
	}

	public function add(){
		if(empty($_POST) ){
			$page_data['title'] = 'Add Test Script';
			$page_data['subview'] = 'jmeter/test_scripts/add_v';

			$page_data['styles'] = array(
				'plugins/datatables/dataTables.bootstrap',
				'plugins/codemirror/codemirror'
			);
			$page_data['scripts'] = array(
				'plugins/datatables/jquery.dataTables.min',
				'plugins/codemirror/codemirror',
				'plugins/datatables/dataTables.bootstrap.min',
				'plugins/quicksearch/jquery.quicksearch',
				'custom/jmeter/test_script/index',
				'plugins/codemirror/mode/css/css',
				'plugins/codemirror/mode/htmlmixed/htmlmixed',
				'plugins/codemirror/mode/javascript/javascript',
				'plugins/codemirror/mode/htmlembedded/htmlembedded',
				'plugins/codemirror/mode/xml/xml',
				'plugins/codemirror/mode/php/php'
			);

			array_push($page_data['scripts'],"");


			$page_data['projects'] = $this->jmeter_testscripts_m->get_projects();
			$desc = '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('test_scripts').'"><i class="ion-ios-arrow-thin-left"></i> Back to Test Scripts List</a>';

			$page_data['page_header_data'] = array('title' => '<i class="ion-settings"></i> Test Scripts', 'description' => $desc);

			$this->load->view('layouts/main_layout', $page_data);
		}
		else{
			$ret = $this->jmeter_testscripts_m->create_testscript();
			redirect('test_scripts/edit/'.$ret,'refresh');
		}
	}

	public function edit($id=0){
		if(empty($_POST) ){
			$page_data['title'] = 'Test Scripts';
			$page_data['subview'] = 'jmeter/test_scripts/edit_v';

			$page_data['styles'] = array(
				'plugins/datatables/dataTables.bootstrap',
				'plugins/codemirror/codemirror'
			);
			$page_data['scripts'] = array(
				'plugins/datatables/jquery.dataTables.min',
				'plugins/codemirror/codemirror',
				'plugins/datatables/dataTables.bootstrap.min',
				'plugins/quicksearch/jquery.quicksearch',
				'custom/jmeter/test_script/index',
				'plugins/codemirror/mode/css/css',
				'plugins/codemirror/mode/htmlmixed/htmlmixed',
				'plugins/codemirror/mode/javascript/javascript',
				'plugins/codemirror/mode/htmlembedded/htmlembedded',
				'plugins/codemirror/mode/xml/xml',
				'plugins/codemirror/mode/php/php'
			);

			array_push($page_data['scripts'],"");

			$page_data["test_script"] = $this->jmeter_testscripts_m->get($id);

			$page_data['projects'] = $this->jmeter_testscripts_m->get_projects();
			$desc = '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('test_scripts').'"><i class="ion-ios-arrow-thin-left"></i> Back to Test Scripts List</a>';

			$page_data['page_header_data'] = array('title' => '<i class="ion-settings"></i> Test Scripts', 'description' => $desc);

			$this->load->view('layouts/main_layout', $page_data);
		}
		else{
			$this->jmeter_testscripts_m->update_template($id);
			redirect('test_scripts/edit/'.$id,'refresh');
		}
	}

	public function test($id=0,$page=""){
		if(empty($_POST) ){
			$page_data['title'] = 'Test Scripts';
			$page_data['subview'] = 'jmeter/test_scripts/test_v';

			$page_data['styles'] = array(
				'plugins/datatables/dataTables.bootstrap',
				'plugins/codemirror/codemirror',
				'css/test_script_tester',
				'css/jquery-ui.min',
				'css/test-custom'
			);
			$page_data['scripts'] = array(
				'plugins/datatables/jquery.dataTables.min',
				'plugins/codemirror/codemirror',
				'plugins/datatables/dataTables.bootstrap.min',
				'plugins/quicksearch/jquery.quicksearch',
				'custom/jmeter/test_script/index',
				'plugins/codemirror/mode/css/css',
				'plugins/codemirror/mode/htmlmixed/htmlmixed',
				'plugins/codemirror/mode/javascript/javascript',
				'plugins/codemirror/mode/htmlembedded/htmlembedded',
				'plugins/codemirror/mode/xml/xml',
				'plugins/codemirror/mode/php/php',
				'ajax/test',
				'js/jquery-ui.min'
			);

			array_push($page_data['scripts'],"");

			$page_data["test_script"] = $this->jmeter_testscripts_m->get($id);

			$page_data['projects'] = $this->jmeter_testscripts_m->get_projects();
			$desc = '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('test_scripts').'"><i class="ion-ios-arrow-thin-left"></i> Back to Test Scripts List</a>';

			$page_data['page_header_data'] = array('title' => '<i class="ion-settings"></i> Test Scripts', 'description' => $desc);

			$this->load->view('layouts/main_layout', $page_data);
		}
		else{
			switch($page){
				case "generate-script":
					if(!file_exists(FCPATH."runners/".$id)){
						mkdir(FCPATH."runners/".$id,0777,true);
					}

					if(!file_exists(FCPATH."runners/".$id."/results")){
						mkdir(FCPATH."runners/".$id."/results",0777,true);
					}

					$entry_date = date("j-m-Y-g-i");

					$arr = $this->input->post();

					$myfile = fopen(FCPATH."runners/".$id."/script.jmx", "w") or die("Unable to open file!");

					$header = "";
					$footer = "";
					if(file_exists(FCPATH."jmeter/templates/header.xml")){
						$header = file_get_contents(FCPATH."jmeter/templates/header.xml");
					}
					if(file_exists(FCPATH."jmeter/templates/footer.xml")){
						$footer = file_get_contents(FCPATH."jmeter/templates/footer.xml");
						$footer = str_replace("@{{xml_result}}",FCPATH."runners/".$id."/results/result-".$entry_date.".xml",$footer);
					}

					$txt = $header.$arr["script"].$footer;
					fwrite($myfile, html_entity_decode($txt));
					fclose($myfile);

					$root = FCPATH;
					$jmeter_bin = $root."jmeter/bin/";
					$test_script = ''.$jmeter_bin.'jmeter -n -t '.FCPATH."runners/".$id."/script.jmx".' -l '.FCPATH."runners/".$id."/results/result-".$entry_date.".csv";
					$output = shell_exec($test_script);

					if(file_exists(FCPATH."runners/".$id."/results/result-".$entry_date.".xml")){
						$output = file_get_contents(FCPATH."runners/".$id."/results/result-".$entry_date.".xml");
					}
					echo $output;
				break;
			}
		}
	}

	public function delete($id=0){
		$this->jmeter_testscripts_m->delete($id);
		$this->logs_m->save_log('Delete Test Scirpt', serialize($id));
		echo json_encode(array("successful"=>true));
	}

	public function test_script($page = "list", $id = 0){
		
		if(empty($_POST) && $page != "delete") {
			$page_data['title'] = 'Test Scripts';
			$page_data['subview'] = 'jmeter/test_scripts/'.$page.'_v';

			$page_data['styles'] = array(
				'plugins/datatables/dataTables.bootstrap',
				'plugins/codemirror/codemirror'
			);
			$page_data['scripts'] = array(
				'plugins/datatables/jquery.dataTables.min',
				'plugins/codemirror/codemirror',
				'plugins/datatables/dataTables.bootstrap.min',
				'plugins/quicksearch/jquery.quicksearch',
				'custom/jmeter/test_script/index',
				'plugins/codemirror/mode/css/css',
				'plugins/codemirror/mode/htmlmixed/htmlmixed',
				'plugins/codemirror/mode/javascript/javascript',
				'plugins/codemirror/mode/htmlembedded/htmlembedded',
				'plugins/codemirror/mode/xml/xml',
				'plugins/codemirror/mode/php/php'
			);

			if($page == "list"){
				$page_data['test_scripts'] = $this->jmeter_testscripts_m->get();
				$desc = 'You can manage your JMeter Test Scripts here.';
			}
			else if($page == "edit" || $page == "add"){

				array_push($page_data['scripts'],"");

				if($page = "edit"){
					$page_data["test_script"] = $this->jmeter_testscripts_m->get($id);
				}

				$page_data['projects'] = $this->jmeter_testscripts_m->get_projects();
				$desc = '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('jmeter/test_script').'"><i class="ion-ios-arrow-thin-left"></i> Back to Test Scripts List</a>';
			}

			$page_data['page_header_data'] = array('title' => '<i class="ion-settings"></i> Test Scripts', 'description' => $desc);


			

			

			$this->load->view('layouts/main_layout', $page_data);
		}
		else{
			switch($page){
				case "add":
					$ret = $this->jmeter_testscripts_m->create_testscript();
					redirect('/jmeter/test_script/edit/'.$ret,'refresh');
				break;
				case "edit":
					$this->jmeter_testscripts_m->update_template($id);
					redirect('/jmeter/test_script/edit/'.$id,'refresh');
				break;
				case "delete":
					$this->jmeter_testscripts_m->delete($id);
					$this->logs_m->save_log('Delete Email Template', serialize($id));
					echo json_encode(array("successful"=>true));
				break;
			}
		}
	}

	public function runner($page = "list", $id = 0){
		$this->load->model('jmeter_testrunners_m');
		if(empty($_POST) && $page != "delete") {
			$page_data['title'] = 'Scheduled Runner';
			$page_data['subview'] = 'jmeter/runner/'.$page.'_v';

			$page_data['styles'] = array(
				'plugins/datatables/dataTables.bootstrap',
			);
			$page_data['scripts'] = array(
				'plugins/datatables/jquery.dataTables.min',
				'plugins/datatables/dataTables.bootstrap.min',
				'plugins/quicksearch/jquery.quicksearch',
				'custom/jmeter/test_runner/index'
			);

			if($page == "list"){
				$page_data['test_scripts'] = $this->jmeter_testscripts_m->get();
				$page_data['test_runners'] = $this->jmeter_testrunners_m->get_runners();
				$desc = 'You can manage/set automated Jmeter Scheduled Runner here.';
			}
			else if($page == "edit" || $page == "add"){

				array_push($page_data['scripts'],"");

				if($page = "edit"){
					$page_data["test_script"] = $this->jmeter_testscripts_m->get($id);
				}

				$page_data['projects'] = $this->jmeter_testscripts_m->get_projects();
				$desc = '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('jmeter/runner').'"><i class="ion-ios-arrow-thin-left"></i> Back to Scheduled Runners List</a>';
			}

			$page_data['page_header_data'] = array('title' => '<i class="ion-settings"></i> Scheduled Runner', 'description' => $desc);

			$this->load->view('layouts/main_layout', $page_data);
		}
		else{
			switch($page){
				case "add":
					$ret = $this->jmeter_testrunners_m->create_runner();
					if($ret){
						echo json_encode(array("successful"=>true,"message"=>"","id"=>$ret));
					}
				break;
				case "edit":
					$this->jmeter_testrunners_m->update_runner($id);
					redirect('/jmeter/runner/edit/'.$id,'refresh');
				break;
				case "delete":
					$this->jmeter_testrunners_m->delete($id);
					$this->logs_m->save_log('Delete Email Template', serialize($id));
					echo json_encode(array("successful"=>true));
				break;
			}
		}
	}

	public function view_runner_logs($id){
		$context = stream_context_create(array(
			'http' => array(
				// http://www.php.net/manual/en/context.http.php
				'method' => 'GET'
			)
		));
		$response = @file_get_contents("http://34.210.71.54/api/get-results/".$id, FALSE,$context);
		echo $response;
	}

	public function view_runner_results($id){
		$context = stream_context_create(array(
			'http' => array(
				// http://www.php.net/manual/en/context.http.php
				'method' => 'GET'
			)
		));
		$response = @file_get_contents("http://34.210.71.54/api/get-results/".$id, FALSE,$context);
		echo $response;
	}

	public function view_runner_history($id){
		$this->load->model('jmeter_testrunners_m');
		$arr = array(
			"successful"=>true,
			"message"=>""
		);
		$arr["list"] = $this->jmeter_testrunners_m->get_history($id);
		echo json_encode($arr);
	}


	/*==============================================================================================*/

	function display_headers(){
		$results_info = array(
			'result' => $_POST['test_result'], 
		);
		$headers = array(); 
		$headers_two = ''; 
		$headers_three = array();
		$headers_four = array();
		$headers = array( $this->get_contents($results_info['result'], '<requestHeader class="java.lang.String">', '</requestHeader>') ); 
		$headers2 = array( $this->get_contents($results_info['result'], '<responseData class="java.lang.String">', '</responseData>') ); 
		 
		//$headers_three = array_merge($headers[0], $headers2[0]); 

		for ($i=0; $i < count($headers[0]) ; $i++) { 
			$headers_three[]['header'] = $headers[0][$i];
    		$headers_three[]['response'] = $headers2[0][$i];
		} 

		foreach ($headers_three as $key => $value) {
			foreach ($value as $key2 => $value2) {  
				if ($key2 == 'header') { 
				 	$headers_two .= '<requestHeader><requestHeader class="java.lang.String">'.$value2.'</requestHeader>';
				}
				else{ 
				 	$headers_two .= '<responseData class="java.lang.String">'.$value2.'</responseData></requestHeader>';
				} 
			}
		} 
		
		
		echo json_encode($headers_two);
	}

	function display_response(){
		$results_info = array(
			'result' => $_POST['test_result'], 
			'arrayIndex' => $_POST['arrayIndex']
		);
		
		$responses = array(); 
		$responses = $this->get_contents($results_info['result'], '<responseData class="java.lang.String">', '</responseData>');  
		print_r(html_entity_decode($responses[$results_info['arrayIndex']]));
	} 

	function count_response(){
		// $headers_three = $this->get_contents($_POST['test_result'], '<httpSample', '>');
		// $headers_two = array();
		// $counter = 0;
		// foreach ($headers_three as $key => $value) { 
		// 	$headers_two[]  =  $this->get_content($_POST['test_result'], '<httpSample'.$value.'>', '</httpSample>');
		// } 
		// echo json_encode($headers_three);
		echo count($this->get_contents($_POST['test_result'], '<responseData class="java.lang.String">', '</responseData>'));
	}

	function get_contents($str, $startDelimiter, $endDelimiter) {
	  $contents = array();
	  $startDelimiterLength = strlen($startDelimiter);
	  $endDelimiterLength = strlen($endDelimiter);
	  $startFrom = $contentStart = $contentEnd = 0;
	  while (false !== ($contentStart = strpos($str, $startDelimiter, $startFrom))) {
	    $contentStart += $startDelimiterLength;
	    $contentEnd = strpos($str, $endDelimiter, $contentStart);
	    if (false === $contentEnd) {
	      break;
	    }
	    $contents[] = substr($str, $contentStart, $contentEnd - $contentStart);
	    $startFrom = $contentEnd + $endDelimiterLength;
	  }

	  return $contents;
	} 

	function get_content($str, $startDelimiter, $endDelimiter) {
	  $contents;
	  $startDelimiterLength = strlen($startDelimiter);
	  $endDelimiterLength = strlen($endDelimiter);
	  $startFrom = $contentStart = $contentEnd = 0;
	  while (false !== ($contentStart = strpos($str, $startDelimiter, $startFrom))) {
	    $contentStart += $startDelimiterLength;
	    $contentEnd = strpos($str, $endDelimiter, $contentStart);
	    if (false === $contentEnd) {
	      break;
	    }
	    $contents = substr($str, $contentStart, $contentEnd - $contentStart);
	    $startFrom = $contentEnd + $endDelimiterLength;
	  }

	  return $contents;
	}

}
