<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jmeter extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);

		$this->load->model('user_m');
		$this->load->model('jmeter_testscripts_m');
		$this->load->model('unsafecrypto');
	}

	public function index() {
		$this->user_m->checkPermissions("add-test-script",true);
		$page_data['title'] = 'Projects Management';
		$page_data['subview'] = 'projects/project_management_v';
		$page_data['page_header_data'] = array('title' => '<i class="ion-easel"></i> Project Management', 'description' => 'You can manage your Serino Project Initialization here.');

		$page_data['test_scripts'] = $this->project_m->get();

		$page_data['styles'] = array('plugins/datatables/dataTables.bootstrap');
		$page_data['scripts'] = array('plugins/datatables/jquery.dataTables.min','plugins/datatables/dataTables.bootstrap.min','plugins/quicksearch/jquery.quicksearch');
		$this->load->view('layouts/main_layout', $page_data);
	}

	public function test_script($page = "list", $id = 0){
		
		if(empty($_POST) && $page != "delete") {
			$page_data['title'] = 'Test Scripts';
			$page_data['subview'] = 'jmeter/test_scripts/'.$page.'_v';

			$page_data['styles'] = array(
				'plugins/datatables/dataTables.bootstrap',
				'plugins/codemirror/codemirror'
			);
			$page_data['scripts'] = array(
				'plugins/datatables/jquery.dataTables.min',
				'plugins/codemirror/codemirror',
				'plugins/datatables/dataTables.bootstrap.min',
				'plugins/quicksearch/jquery.quicksearch',
				'custom/jmeter/test_script/index',
				'plugins/codemirror/mode/css/css',
				'plugins/codemirror/mode/htmlmixed/htmlmixed',
				'plugins/codemirror/mode/javascript/javascript',
				'plugins/codemirror/mode/htmlembedded/htmlembedded',
				'plugins/codemirror/mode/xml/xml',
				'plugins/codemirror/mode/php/php'
			);

			if($page == "list"){
				$page_data['test_scripts'] = $this->jmeter_testscripts_m->get();
				$desc = 'You can manage your JMeter Test Scripts here.';
			}
			else if($page == "edit" || $page == "add"){

				array_push($page_data['scripts'],"");

				if($page = "edit"){
					$page_data["test_script"] = $this->jmeter_testscripts_m->get($id);
				}

				$page_data['projects'] = $this->jmeter_testscripts_m->get_projects();
				$desc = '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('jmeter/test_script').'"><i class="ion-ios-arrow-thin-left"></i> Back to Test Scripts List</a>';
			}

			$page_data['page_header_data'] = array('title' => '<i class="ion-settings"></i> Test Scripts', 'description' => $desc);


			

			

			$this->load->view('layouts/main_layout', $page_data);
		}
		else{
			switch($page){
				case "add":
					$ret = $this->jmeter_testscripts_m->create_testscript();
					redirect('/jmeter/test_script/edit/'.$ret,'refresh');
				break;
				case "edit":
					$this->jmeter_testscripts_m->update_template($id);
					redirect('/jmeter/test_script/edit/'.$id,'refresh');
				break;
				case "delete":
					$this->jmeter_testscripts_m->delete($id);
					$this->logs_m->save_log('Delete Email Template', serialize($id));
					echo json_encode(array("successful"=>true));
				break;
			}
		}
	}

	public function runner($page = "list", $id = 0){
		$this->load->model('jmeter_testrunners_m');
		if(empty($_POST) && $page != "delete") {
			$page_data['title'] = 'Scheduled Runner';
			$page_data['subview'] = 'jmeter/runner/'.$page.'_v';

			$page_data['styles'] = array(
				'plugins/datatables/dataTables.bootstrap',
			);
			$page_data['scripts'] = array(
				'plugins/datatables/jquery.dataTables.min',
				'plugins/datatables/dataTables.bootstrap.min',
				'plugins/quicksearch/jquery.quicksearch',
				'custom/jmeter/test_runner/index'
			);

			if($page == "list"){
				$page_data['test_scripts'] = $this->jmeter_testscripts_m->get();
				$page_data['test_runners'] = $this->jmeter_testrunners_m->get_runners();
				$desc = 'You can manage/set automated Jmeter Scheduled Runner here.';
			}
			else if($page == "edit" || $page == "add"){

				array_push($page_data['scripts'],"");

				if($page = "edit"){
					$page_data["test_script"] = $this->jmeter_testscripts_m->get($id);
				}

				$page_data['projects'] = $this->jmeter_testscripts_m->get_projects();
				$desc = '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('jmeter/runner').'"><i class="ion-ios-arrow-thin-left"></i> Back to Scheduled Runners List</a>';
			}

			$page_data['page_header_data'] = array('title' => '<i class="ion-settings"></i> Scheduled Runner', 'description' => $desc);

			$this->load->view('layouts/main_layout', $page_data);
		}
		else{
			switch($page){
				case "add":
					$ret = $this->jmeter_testrunners_m->create_runner();
					if($ret){
						echo json_encode(array("successful"=>true,"message"=>"","id"=>$ret));
					}
				break;
				case "edit":
					$this->jmeter_testrunners_m->update_runner($id);
					redirect('/jmeter/runner/edit/'.$id,'refresh');
				break;
				case "delete":
					$this->jmeter_testrunners_m->delete($id);
					$this->logs_m->save_log('Delete Email Template', serialize($id));
					echo json_encode(array("successful"=>true));
				break;
			}
		}
	}

	public function view_runner_logs($id){
		$context = stream_context_create(array(
			'http' => array(
				// http://www.php.net/manual/en/context.http.php
				'method' => 'GET'
			)
		));
		$response = @file_get_contents("http://34.210.71.54/api/get-results/".$id, FALSE,$context);
		echo $response;
	}

	public function view_runner_results($id){
		$context = stream_context_create(array(
			'http' => array(
				// http://www.php.net/manual/en/context.http.php
				'method' => 'GET'
			)
		));
		$response = @file_get_contents("http://34.210.71.54/api/get-results/".$id, FALSE,$context);
		echo $response;
	}

	public function view_runner_history($id){
		$this->load->model('jmeter_testrunners_m');
		$arr = array(
			"successful"=>true,
			"message"=>""
		);
		$arr["list"] = $this->jmeter_testrunners_m->get_history($id);
		echo json_encode($arr);
	}

}
