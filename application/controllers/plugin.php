<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Plugin extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);

		$this->load->model('user_m');
		$this->load->model('plugin_m');
	}

	public function index() {
		$this->plugin_management();
	}

	/*********************************************/ 

	#PLUGINS MANAGEMENT

	/*********************************************/
	
	public function plugin_management() {
		$this->user_m->checkPermissions("view-plugins",true);
		$page_data['title'] = 'Plugin Management';
		$page_data['subview'] = 'plugins/plugin_management_v';
		$page_data['page_header_data'] = array('title' => 'Plugins Management', 'description' => 'You can manage your Serino Page Builder Plugins here.');
		$page_data['styles'] = array('plugins/datatables/datatables.min');
		$page_data['scripts'] = array('plugins/datatables/datatables.min','custom/plugin.functions','custom/plugin.management');

		$this->load->view('layouts/main_layout', $page_data);
	}

	public function view_plugin($plugin_id) {
		$this->user_m->checkPermissions("view-plugins",true);
		$plugin = $this->plugin_m->get($plugin_id);
		$page_data['title'] = 'View Plugin - '.$plugin->title;
		$page_data['subview'] = 'plugins/view_plugin_v';
		$page_data['page_header_data'] = array('title' => '<span class="plugin-title">View Plugin - '.$plugin->title.'</span>', 'description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('plugin/plugin_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to Plugins Management</a>');
		$page_data['plugin'] = $plugin;

		$page_data['styles'] = array('plugins/bootstrap-milestones/bootstrap-milestones.min','plugins/lightcase/css/lightcase');
		$page_data['scripts'] = array('custom/plugin.functions','plugins/lightcase/js/lightcase','custom/preview.plugin');
		
		$this->load->view('layouts/main_layout', $page_data);
	}	

	public function save_plugin($plugin_id = NULL) {
		$this->user_m->checkPermissions("add-plugins",true);
		if(empty($_POST)) {
			$this->load->model('cms_m');

			$page_data['title'] = ($plugin_id) ? 'Edit Plugin' : 'Add Plugin';
			$page_data['subview'] = 'plugins/save_plugin_v';
			$page_data['page_header_data'] = array('description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('plugin/plugin_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to Plugins Management</a>');
			$page_data['styles'] = array('plugins/jasny-bootstrap/css/jasny-bootstrap.min','plugins/bootstrap-tags/bootstrap-tagsinput','plugins/bootstrap-multiselect/css/bootstrap-multiselect','plugins/summernote/summernote');
			$page_data['scripts'] = array('plugins/jasny-bootstrap/js/jasny-bootstrap.min','plugins/bootstrap-tags/bootstrap-tagsinput.min','plugins/bootstrap-multiselect/js/bootstrap-multiselect','plugins/summernote/summernote.min','custom/plugin.functions','custom/save.plugin');
			$page_data['categories'] = $this->plugin_m->get_distinct('category');
			$page_data['cms'] = $this->cms_m->get();
			
			if($plugin_id) {
				$page_data['plugin'] = $this->plugin_m->get($plugin_id);
				$page_data['page_header_data']['title'] = 'Edit Plugin - <span class="plugin-title">'.$page_data['plugin']->title.'</span>';		
			} else {
				$page_data['plugin'] = $this->plugin_m->get_new();
				$page_data['page_header_data']['title'] = 'Add Plugin - <span class="plugin-title">'.$page_data['plugin']->title.'</span>';
			}

			$this->load->view('layouts/main_layout', $page_data);
		} else {
			$this->plugin_m->save_plugin($plugin_id);
		}
	}

	public function update_plugin($plugin_id) {
		$this->user_m->checkPermissions("edit-plugins",true);
		if(empty($_POST)) {
			$this->load->model('cms_m');

			$plugin = $this->plugin_m->get($plugin_id);
			$page_data['title'] = 'Update Plugin - '.$plugin->title;
			$page_data['subview'] = 'plugins/update_plugin_v';
			$page_data['page_header_data'] = array('description' => '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('plugin/plugin_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to Plugin Management</a>');
			$page_data['styles'] = array('plugins/jasny-bootstrap/css/jasny-bootstrap.min','plugins/bootstrap-multiselect/css/bootstrap-multiselect','plugins/summernote/summernote');
			$page_data['scripts'] = array('plugins/jasny-bootstrap/js/jasny-bootstrap.min','plugins/summernote/summernote.min','plugins/bootstrap-multiselect/js/bootstrap-multiselect','custom/update.plugin');

			$page_data['cms'] = $this->cms_m->get();
			$page_data['plugin'] = $plugin;
			$page_data['page_header_data']['title'] = 'Update Plugin - '.$plugin->title;
			
			$this->load->view('layouts/main_layout', $page_data);
		}
	}

	function _delete_directory($directory, $empty = false) { 
	    if(substr($directory,-1) == "/") { 
	        $directory = substr($directory,0,-1); 
	    } 

	    if(!file_exists($directory) || !is_dir($directory)) { 
	        return false; 
	    } elseif(!is_readable($directory)) { 
	        return false; 
	    } else { 
	        $directoryHandle = opendir($directory); 
	        
	        while ($contents = readdir($directoryHandle)) { 
	            if($contents != '.' && $contents != '..') { 
	                $path = $directory . "/" . $contents; 
	                
	                if(is_dir($path)) { 
	                    $this->_delete_directory($path); 
	                } else { 
	                    unlink($path); 
	                } 
	            } 
	        } 
	        
	        closedir($directoryHandle); 

	        if($empty == false) { 
	            if(!rmdir($directory)) { 
	                return false; 
	            } 
	        } 
	        
	        return true; 
	    } 
	}
}