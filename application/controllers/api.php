<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);

		$this->load->model('user_m');
		$this->load->model('tool_m');
	}

	/*********************************************/

	#API CALLS

	/*********************************************/

	function tools_api() {
		header("Access-Control-Allow-Origin: *");
		header('Content-Type: application/json');

		$tools = $this->tool_m->get();

		foreach ($tools as $tool) {
			$tool_sets[] = array(
				'tool_id' => $tool->tool_id,
				'tool_name' => $tool->tool_name,
				'tool_title' => $tool->tool_title,
				'tool_description' => $tool->tool_description,
				'tool_category' => $tool->tool_category,
				'tool_version' => $tool->tool_version,
				'tool_image' => base_url('tools/'.$tool->tool_name.'/screenshot.png')
			);
		}

		echo json_encode($tool_sets);
	}

	function convertCSVtoJSON() {
		header("Access-Control-Allow-Origin: *");
		header('Content-Type: application/json');

		$index = array('a','b','c','d','e','f','g');

		switch ($this->input->server('REQUEST_METHOD')) {
			case 'POST':
				
				$data = $this->_csv_to_php($this->input->post('FileSourceURL'));
				$result = array(); 
				
				$headers = array_shift($data);
				unset($data[0]);

				$result['Headers'] = $headers;

				foreach ($data as $key => $row_data) {
					$row_temp_data = array();
					foreach ($row_data as $row_data_key => $row_data_value) {
						$row_temp_data[$headers[$row_data_key]] = $row_data_value;
					}

					$row = $row_temp_data;
					$result['Items'][] = $row;
				}

				$result['Successful'] = true;
				$result['Message'] = 'Parse successful';
				echo json_encode($result);

				break;
		}
	}

	function _csv_to_php($path_to_file) {
		$result = array();

		$file = fopen($path_to_file, 'r');
			while (($line = fgetcsv($file)) !== FALSE) {
			//$line is an array of the csv elements
			$result[] = $line;
		}

		fclose($file);

		return $result;
	}
}
