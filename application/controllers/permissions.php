<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permissions extends Backend_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->permissions_management();
	}

	public function permissions_management() {
		$page_data = array(
			'title' 			=> 'Permissions Management',
			'subview' 			=> 'permissions/permission_management_v',
			'page_header_data'	=> array(
				'title' 		=> '<i class="ion-paintbucket"></i> Permissions Management',
				'description' 	=> 'You can manage your Serino CMS Permissions here.'
			),
			'styles'			=> array('plugins/datatables/datatables.min'),
			'scripts'			=> array(
				'plugins/datatables/datatables.min',
				'custom/permission.functions',
				'custom/permission.management'
			)
		);

		$this->load->view('layouts/main_layout.php', $page_data);
	}

	function save_permission($permission_id = null) {
		$this->load->model('permission_m');

		$page_data = array(
			'title'				=> ($permission_id) ? 'Edit Permission' : 'Add Permission',
			'subview'			=> 'permissions/save_permission_v',
			'page_header_data' 	=> array(
				'description' 	=> '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('permissions/permissions_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to Permissions Management</a>'
			),
			'styles'			=> array('plugins/summernote/summernote'),
			'scripts'			=> array(
				'plugins/summernote/summernote.min', 
				'custom/permission.functions',
				'custom/save.permission'),
			'groups' 			=> $this->permission_m->get_distinct('permission_group')
		);

		if($permission_id) {
			$page_data['permission'] = $this->permission_m->get($permission_id);
			$page_data['page_header_data']['title'] = 'Edit Permission - <span class="permission-title">'.$page_data['permission']->permission_title.'</span>';		
		} else {
			$page_data['page_header_data']['title'] = 'Add Permission - <span class="permission-title"> New Permission </span>';
		}

		$this->load->view('layouts/main_layout', $page_data);
	}
}

?>