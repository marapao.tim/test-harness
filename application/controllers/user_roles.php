<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Roles extends Backend_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->user_roles_management();
	}

	public function user_roles_management() {
		$this->user_m->checkPermissions("view-roles",true);
		$page_data = array(
			'title' 			=> 'User Roles Management',
			'subview' 			=> 'user_roles/user_role_management_v',
			'page_header_data'	=> array(
				'title' 		=> '<i class="ion-paintbucket"></i> User Roles Management',
				'description' 	=> 'You can manage your Serino CMS User Roles here.'
			),
			'styles'			=> array('plugins/datatables/datatables.min'),
			'scripts'			=> array(
				'plugins/datatables/datatables.min',
				'custom/user_role.functions',
				'custom/user_role.management'
			)
		);

		$this->load->view('layouts/main_layout.php', $page_data);
	}

	function save_user_role($user_role_id = null) {

		$this->load->model('user_role_m');
		$this->load->model('permission_m');

		$page_data = array(
			'title'				=> ($user_role_id) ? 'Edit User Role' : 'Add User Role',
			'subview'			=> 'user_roles/save_user_role_v',
			'page_header_data' 	=> array(
				'description' 	=> '<a class="btn btn-primary btn-sm pull-right" href="'.base_url('user_roles/user_roles_management').'"><i class="ion-ios-arrow-thin-left"></i> Back to User Roles Management</a>'
			),
			'styles'			=> array(
				'plugins/summernote/summernote'
			),
			'scripts'			=> array(
				'plugins/summernote/summernote.min',
				'custom/user_role.functions',
				'custom/save.user_role'
			),
			'permissions' 		=> $this->permission_m->getPermissions(array(
				array(
					'column'	=> 'permission_group',
					'order' 	=> 'ASC'
				),
				array(
					'column'	=> 'permission_code',
					'order'		=> 'ASC'
				)
			))
		);

		if($user_role_id) {
			$this->user_m->checkPermissions("edit-roles",true);
			$page_data['user_role'] = $this->user_role_m->get($user_role_id);
			$page_data['user_role']->user_role_permissions = unserialize($page_data['user_role']->user_role_permissions);
			$page_data['page_header_data']['title'] = 'Edit User Role - <span class="user-role-title">'.$page_data['user_role']->user_role_title.'</span>';		
		} else {
			$this->user_m->checkPermissions("add-roles",true);
			$page_data['page_header_data']['title'] = 'Add User Role - <span class="user-role-title"> New User Role </span>';
		}

		$this->load->view('layouts/main_layout', $page_data);
	}
}

?>