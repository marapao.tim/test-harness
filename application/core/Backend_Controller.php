<?php

class Backend_Controller extends MY_Controller {

	function __construct() {
		parent::__construct();
		
		//model init
		$this->load->model('user_m');
		$this->load->model('logs_m');
		//$this->output->enable_profiler(TRUE);
        if(!$this->user_m->loggedin()) {
            $this->session->set_flashdata('error', 'You mus\'t login in to view that page.');
            redirect('login/?redirect='.current_url());
        }

	}
}
