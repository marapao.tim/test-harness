<?php

class MY_Controller extends CI_Controller {

	public $master_config = array(
		'date_format' 		=> 'F d, Y',
		'date_time_format' 	=> 'F d, Y h:i:s A'
	);

	function __construct() {
		parent::__construct();
	}
}
