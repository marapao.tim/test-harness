<?php
class MY_Model extends CI_Model {
	
	protected $_table_name = '';
	protected $_primary_key = '';
	protected $_primary_filter = '';
	protected $_order_by = '';
	protected $_order = '';
	public $rules = array();
	protected $_timestamps = FALSE;
	
	function __construct() {
		parent::__construct();
	}

	public function get($id = NULL, $single = FALSE){
		
		if ($id != NULL) {
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->where($this->_primary_key, $id);
			$method = 'row';
		}
		elseif($single == TRUE) {
			$method = 'row';
		}
		else {
			$method = 'result';
		}
		
		if (!count($this->db->ar_orderby)) {
			$this->db->order_by($this->_order_by, $this->_order);
		}
		return $this->db->get($this->_table_name)->$method();
	}
	
	public function get_by($where, $single = FALSE){
		$this->db->where($where);
		return $this->get(NULL, $single);
	}
	
	public function get_distinct($column){
		$this->db->select($column);
		$this->db->distinct($column);
		$this->db->where($column.' !=', '');
		if (!count($this->db->ar_orderby)) {
			$this->db->order_by($column, 'ASC');
		}
		return $this->db->get($this->_table_name)->result();
	}

	public function save($data, $id = NULL){
		
		// Set timestamps
		if ($this->_timestamps == TRUE) {
			$now = date('Y-m-d H:i:s');
			if($id) {
				$data['date_modified'] = $now;
			} else {
				$data['date_created'] = $now;
			}
		}
		
		// Insert
		if ($id === NULL) {
			$data['created_by'] = $this->session->userdata('user_id');
			!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
			$this->db->set($data);
			$this->db->insert($this->_table_name) or die('boo');
			$id = $this->db->insert_id();
		}
		// Update
		else {
			$data['modified_by'] = $this->session->userdata('user_id');
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->set($data);
			$this->db->where($this->_primary_key, $id);
			$this->db->update($this->_table_name);
		}
		
		return $id;
	}
	
	public function delete($id){
		$filter = $this->_primary_filter;
		$id = $filter($id);
		
		if (!$id) {
			return FALSE;
		}
		
		$this->db->where($this->_primary_key, $id);
		$this->db->limit(1);
		$this->db->delete($this->_table_name);
	}

	public function display_alert() {
		if($this->session->flashdata('alert_message')) {
			?>
				<script type="text/javascript">
					$.notify("<?= $this->session->flashdata('alert_message'); ?>", {type:"success"});
				</script>
			<?php
		}
	}

 	public function _delete_directory($directory, $empty = false) { 
	    if(substr($directory,-1) == "/") { 
	        $directory = substr($directory,0,-1); 
	    } 

	    if(!file_exists($directory) || !is_dir($directory)) { 
	        return false; 
	    } elseif(!is_readable($directory)) { 
	        return false; 
	    } else { 
	        $directoryHandle = opendir($directory); 
	        
	        while ($contents = readdir($directoryHandle)) { 
	            if($contents != '.' && $contents != '..') { 
	                $path = $directory . "/" . $contents; 
	                
	                if(is_dir($path)) { 
	                    $this->_delete_directory($path); 
	                } else { 
	                    unlink($path); 
	                } 
	            } 
	        } 
	        
	        closedir($directoryHandle); 

	        if($empty == false) { 
	            if(!rmdir($directory)) { 
	                return false; 
	            } 
	        } 
	        
	        return true; 
	    } 
	}

	public function clean_string($string) {
		$string = trim(strtolower($string));
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

		return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}
}