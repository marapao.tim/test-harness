<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "main";

//api routing
$route['api/analytics'] = "api/convertCSVtoJSON";

$route['v1/tools'] = "v1/tools/index";
$route['v1/tools/(:num)'] = "v1/tools/index/$1";
$route['v1/tools/(:num)/update'] = "v1/tools/index/$1/update";

$route['v1/tools/update'] = "v1/tools/update";
$route['v1/tools/update/(:number)/(:number)'] = "v1/tools/update/$1/$2";

//api routing
$route['v1/themes'] = "v1/themes/index";
$route['v1/themes/(:num)'] = "v1/themes/index/$1";
$route['v1/themes/(:num)/update'] = "v1/themes/index/$1/update";

$route['v1/themes/update'] = "v1/themes/update";
$route['v1/themes/update/(:number)/(:number)'] = "v1/themes/update/$1/$2";

$route['v1/plugins'] = "v1/plugins/index";
$route['v1/plugins/(:num)'] = "v1/plugins/index/$1";
$route['v1/plugins/(:num)/update'] = "v1/plugins/index/$1/update";

$route['v1/plugins/update'] = "v1/plugins/update";
$route['v1/plugins/update/(:number)/(:number)'] = "v1/plugins/update/$1/$2";

$route['v1/permissions/(:any)'] = "v1/permissions/index/$1";

$route['v1/user_roles/(:any)'] = "v1/user_roles/index/$1";

$route['v1/cms/(:any)'] = "v1/cms/index/$1";
$route['v1/cms/update'] = "v1/cms/update";

$route['v1/(:any)'] = "v1/$1";

$route['404_override'] = '';

$route['v1/cms/update'] = "v1/cms/update";




/* End of file routes.php */
/* Location: ./application/config/routes.php */