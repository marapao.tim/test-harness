<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config["site_permissions"] = array(
	"CMS"=>array(
		"Add CMS"=>"add-cms",
		"Edit CMS"=>"edit-cms",
		"Delete CMS"=>"delete-cms",
		"View CMS"=>"view-cms",
	),
	"Plugins"=>array(
		"Add Plugins"=>"add-plugins",
		"Edit Plugins"=>"edit-plugins",
		"Delete Plugins"=>"delete-plugins",
		"View Plugins"=>"view-plugins",
	),
	"Roles"=>array(
		"Add Roles"=>"add-roles",
		"Edit Roles"=>"edit-roles",
		"Delete Roles"=>"delete-roles",
		"View Roles"=>"view-roles",
	),
	"Theme"=>array(
		"Add Theme"=>"add-themes",
		"Edit Theme"=>"edit-themes",
		"Delete Theme"=>"delete-themes",
		"View Theme"=>"view-themes",
	),
	"Tools"=>array(
		"Add Tools"=>"add-tools",
		"Edit Tools"=>"edit-tools",
		"Delete Tools"=>"delete-tools",
		"View Tools"=>"view-tools",
	),
	"Users"=>array(
		"Add Users"=>"add-users",
		"Edit Users"=>"edit-users",
		"Delete Users"=>"delete-users",
		"View Users"=>"view-users",
	),
	"Projects"=>array(
		"Add Projects"=>"add-projects",
		"Edit Projects"=>"edit-projects",
		"Delete Projects"=>"delete-projects",
		"View Projects"=>"view-projects",
		"View Project Root Credentials"=>"view-projects-root-creds",
	),
	"Logs"=>array(
		"View Logs"=>"view-logs",
	),
	"JMeter"=>array(
		"Add Test Script"=>"add-test-script",
		"Edit Test Script"=>"edit-test-script",
		"Delete Test Script"=>"delete-test-script",
		"View Test Scripts"=>"view-test-scripts",
		"Use Runner"=>"jmeter-use-runner",
		"View Test Logs"=>"jmeter-view-test-logs"
	),
	"Email Templates"=>array(
		"Add Templates"=>"add-email-templates",
		"Edit Templates"=>"edit-email-templates",
		"Delete Templates"=>"delete-email-templates",
		"View Templates"=>"view-email-templates"
	)
);