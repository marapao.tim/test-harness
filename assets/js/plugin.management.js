$(document).ready(function(){
	//Datatables
	$('.datatable').dataTable({
		responsive: true,
		'order': [],
    	aoColumnDefs: [
		  {
		     bSortable: false,
		     bSearchable: false,
		     aTargets: [-1, 0]
		  }
		]
	});
});