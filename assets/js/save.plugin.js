$(document).ready(function(){

	$('input[name="icon"][type="file"]').on('change', function(){
		input_file = $(this);

		file = input_file[0].files[0];

		if (file.type.match('image.*')) {
			$('.plugin-image-preview').attr('src', URL.createObjectURL(event.target.files[0]));
		} else {
			bootbox.alert('Invalid file. Please select again.', function(){
				input_file.trigger('click');
			});
		}
	});

	$('input[name="name"]').keyup(function(event) {
		if($(this).val() !== '')
			$('span.plugin-name').text($(this).val());
		else
			$('span.plugin-name').text('New Plugin');
	});

	$('#plugin-help-block').click(function(){
		$('input[name="icon"][type="file"]').trigger('click');
	});
});

$('.bs-editor').wysihtml5();