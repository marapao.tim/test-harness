$(document).ready(function(){
	//event listener for theme name
	$('input[name="project_title"]').keyup(function(){
		var project_title = $(this).val();
		project_title = project_title.split(' ').join('-').toLowerCase();
		$('input[name="project_name"]').val(project_title);
	});

	window.Parsley
		.addValidator('projectnameexists', {
		requirementType: 'string',
		validateString: function(value, requirement) {
			return check_project_exists(requirement, value);
		},
		messages: {
			en: 'Project name already exists.'
		}
	});
});

function check_project_exists(url, value) {

	//var response = 'false';

	$.ajax({
		method: 'POST',
		data: {project_name: value},
		url: url,
		async: false,
		success: function(data){
			if(data == 1) {
				response = true;
			} else {
				response = false;
			}
		},
		error: function(data) {
		}
	});

	return response;
}

