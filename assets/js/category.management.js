$(document).ready(function(){
	//Datatables
	$('.datatable').dataTable({
		responsive: true,
		'order': [[0, 'asc']],
		'columnDefs': [{
			'render': function (data, type, full, meta){
				return '<input type="checkbox">';
			}
		}],
    	aoColumnDefs: [
		  {
		     bSortable: false,
		     bSearchable: false,
		     aTargets: [-1]
		  }
		]
	});
});