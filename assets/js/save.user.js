$(document).ready(function(){

	window.Parsley
		.addValidator('usernameexists', {
		requirementType: 'string',
		validateString: function(value, requirement) {

			var response = false;

			$.ajax({
				method: 'POST',
				data: {user_name: value},
				url: requirement,
				async: false,
				success: function(data){
					if(data == 1) {
						response = true;
					} else {
						response = false;
					}
				},
				error: function(data) {
				}
			});

			return response;
		},
		messages: {
			en: 'Username already exists.'
		}
	});	

	window.Parsley
		.addValidator('emailexists', {
		requirementType: 'string',
		validateString: function(value, requirement) {

			var response = false;

			$.ajax({
				method: 'POST',
				data: {user_email: value},
				url: requirement,
				async: false,
				success: function(data){
					if(data == 1) {
						response = true;
					} else {
						response = false;
					}
				},
				error: function(data) {
				}
			});

			return response;
		},
		messages: {
			en: 'Email already exists.'
		}
	});
});