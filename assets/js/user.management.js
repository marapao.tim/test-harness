$(document).ready(function(){
	//Datatables
	$('.datatable').dataTable({
		responsive: true,
		'order': [[0, 'asc']],
    	aoColumnDefs: [
		  {
		     bSortable: false,
		     bSearchable: false,
		     aTargets: [-1]
		  }
		]
	});
});