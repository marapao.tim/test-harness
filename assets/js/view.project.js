$(document).ready(function(){
	
	$('.multiselect').multiSelect({
		selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Search...'><br>",
		selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Search...'><br>",
		afterInit: function(ms){
			var that = this,
			$selectableSearch = that.$selectableUl.prev().prev(),
			$selectionSearch = that.$selectionUl.prev().prev(),
			selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
			selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

			that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
			.on('keydown', function(e){
				if (e.which === 40){
					that.$selectableUl.focus();
					return false;
				}
			});

			that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
			.on('keydown', function(e){
				if (e.which == 40){
					that.$selectionUl.focus();
					return false;
				}
			});
		},
		afterSelect: function(){
			this.qs1.cache();
			this.qs2.cache();
		},
		afterDeselect: function(){
			this.qs1.cache();
			this.qs2.cache();
		}
	});

	//event listener for theme name
	$('input[name="project_title"]').keyup(function(){
		var project_title = $(this).val();
		project_title = project_title.split(' ').join('-').toLowerCase();
		$('input[name="project_name"]').val(project_title);
	});

	window.Parsley
		.addValidator('projectnameexists', {
		requirementType: 'string',
		validateString: function(value, requirement) {
			return check_project_exists(requirement, value);
		},
		messages: {
			en: 'Project name already exists.'
		}
	});
});

//project logo upload
$('.project-logo-uploader').on('change',function(event){
	
	var project_logo = $(this)[0].files[0];
	var form = $(this).closest('form');
	var fd = new FormData();

	fd.append('project_logo', project_logo);

	$.ajax({
		url: form.attr('action'),
		type: 'POST',
		data: fd,
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		beforeSend: function() {
			$('.project-logo-container img').addClass('hidden');
			$('.project-logo-container div.loader4').removeClass('hidden');
		},
		success: function(data) {
			$('.project-logo-container img').attr('src', base_url+'store/projects/'+data.project_name+'/images/150x150/'+data.new_image);
		},
		complete: function(data) {
			setTimeout(function(){
				$('.project-logo-container img').removeClass('hidden');
				$('.project-logo-container div.loader4').addClass('hidden');
			}, 5000);
		},
		error: function(data) {
			console.log(data);
		}
	});
});

function check_project_exists(url, value) {

	//var response = 'false';

	$.ajax({
		method: 'POST',
		data: {project_name: value},
		url: url,
		async: false,
		success: function(data){
			if(data == 1) {
				response = true;
			} else {
				response = false;
			}
		},
		error: function(data) {
		}
	});

	return response;
}

//select2
$('.select2').select2();