$(document).ready(function(){

	// .modal-backdrop classes
	$(".modal-transparent").on('show.bs.modal', function () {
	  setTimeout( function() {
	    $(".modal-backdrop").addClass("modal-backdrop-transparent");
	  }, 0);
	});
	$(".modal-transparent").on('hidden.bs.modal', function () {
	  $(".modal-backdrop").addClass("modal-backdrop-transparent");
	});

	$(".modal-fullscreen").on('show.bs.modal', function () {
	  setTimeout( function() {
	    $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
	  }, 0);
	});
	$(".modal-fullscreen").on('hidden.bs.modal', function () {
	  $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
	});

	//bootbox action confirm
	$('body').on('click','a.action-modal-trigger', function(e){
		e.preventDefault();

		element = $(this);

		var response = bootbox.confirm(element.attr('data-action-modal-message'), function(response){
			if(response) {
				window.location.replace(element.attr('href'));
			}
		});
	});

	//panel collapse
	$(document).on('click', '[data-widget="collapse"]', function(e){
	    var $this = $(this);
		if(!$this.hasClass('panel-collapsed')) {
			$this.parents('.panel').find('.panel-body').hide();
			$this.addClass('panel-collapsed');
			$this.find('i').removeClass('ion-minus').addClass('ion-plus');
		} else {
			$this.parents('.panel').find('.panel-body').show();
			$this.removeClass('panel-collapsed');
			$this.find('i').removeClass('ion-plus').addClass('ion-minus');
		}
	})

	//timeago
	$("abbr.timeago").timeago();

	//Bootstrap Tooltips
	$('[rel="tooltip"]').tooltip();
});