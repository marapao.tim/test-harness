$(document).ready(function(){

	//event listener for theme name
	$('input[name="theme_title"]').keyup(function(){
		var theme_title = $(this).val();
		theme_title = theme_title.split(' ').join('-').toLowerCase();
		$('input[name="theme_name"]').val(theme_title);
	});

	window.Parsley
		.addValidator('themenameexists', {
		requirementType: 'string',
		validateString: function(value, requirement) {
			return check_theme_exists(requirement, value);
		},
		messages: {
			en: 'Theme name already exists.'
		}
	});
});

function check_theme_exists(url, value) {

	//var response = 'false';

	$.ajax({
		method: 'POST',
		data: {theme_name: value},
		url: url,
		async: false,
		success: function(data){
			if(data == 1) {
				response = true;
			} else {
				response = false;
			}
		},
		error: function(data) {
		}
	});

	return response;
}