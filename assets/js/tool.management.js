$(document).ready(function(){
	//Datatables
	$('.datatable').dataTable({
		responsive: true,
		'order': [[0, 'asc']],
		'columnDefs': [{
			'render': function (data, type, full, meta){
				return '<input type="checkbox">';
			}
		}],
    	aoColumnDefs: [
		  {
		     bSortable: false,
		     bSearchable: false,
		     aTargets: [-1]
		  }
		]
	});

	$('#filter-by-category').change(function(){
		window.location = base_url+'tools/tools_management?filter='+$(this).val();
	});

	$('.filter-tunnel-toggle').click(function(){
		if($('.filter-panel').hasClass('hidden'))
			$('.filter-panel').removeClass('hidden');
		else
			$('.filter-panel').addClass('hidden');
	});
});