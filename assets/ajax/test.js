var page_selected = 0;
var totaldata = 0;
var loader = '.loader';
var table = '.table';
var body = 'body';
//var totaldata_second = 0;
$(function(){ 
	display_data(page_selected);  
	//$(loader).hide(); 
});  
function display_data(page_number){   
	page_selected = page_number; 
	$(loader).show(); 
	$(table).css({'filter':'blur(5px)'});
	$(body).css({'cursor':'progress'}); 
	$.ajax({
        dataType: 'json',
        type:'post',
        url: base_url + "test/pagination_json/", 
        data:{
        	page_selected:page_selected, 
        	search:$('#txtsearch').val()
        },
    }).done(function(result){ 
    	var row = '';
    	//console.log(result);
    	$('.test-table tbody').html('');
    	$.each(result, function (key, value) {
    		var no_data = '';
    		if(value.id == 'NoData'){
    			no_data = "display:none";
    		}
    		row += '<tr style="'+ no_data +'">'; 
    		row += '<td>' + value.id + '</td>';
   			row += '<td>' + value.full_name + '</td>';
   			row += '<td>' + value.age + '</td>';
   			row += '<td>' + value.gender + '</td>';
   			row += '<td class="text-center"><a href="#" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> | <a href="#" data-toggle="tooltip" title="Delete" class="text-danger"><i class="fa fa-trash" aria-hidden="true"></i></a></td>'; 
   			row += '</tr>';	
   		});
   		$('.test-table tbody').append(row);   
   		display_pagination(); 
    });

}

function display_pagination(){ 
	$.ajax({
        dataType: 'json',
        type:'post',
         data:{ 
        	search:$('#txtsearch').val()
        },
        url: base_url + "test/page_link/",  
    }).done(function(result){
    	total_data(result.total_pagination); 
    	$('.total-count label').html('Total Count: ' + result.total_count);
    	$('nav.test-paginate ul.pagination').html('');	
    	var row = ''; 
    	var a = 0;
    	var b = 0;
    	var displayPrevious = '';
    	var displayNext = ''; 
    	if (page_selected <= 0) 
    	{
    		displayPrevious = 'display:none';
    	}  
		row += '<li class="page-item" style="'+ displayPrevious +'">'; 
		row += '<a class="page-link" href="#"> Previous </a></li>'; 
	  	if (page_selected > 2) { 
			row += '<li class="page-item">'; 
			row += '<a class="page-link" onclick="display_data(0); return false" href="#"> 1 </a>';
			row += '</li>';	
			row += '<li class="disabled" style="background:transparent;"><span style="background: none;border:none;color:#000">. . .</span></li>';	
		} 

	    for (i = parseInt(parseInt(page_selected) + 1) - 2; i <= result.total_pagination; i++) { 
	    	if(i >= 0){
	    		a++; 
		    	if (a <= 5) 
		    	{
		    		var active = '';
		    		if ((parseInt(page_selected) + 1) == i) {
		    			active = 'active';
		    		}
		    		var displayZero = '';
		    		if (i == 0) { displayZero = 'display:none'; }
			    	var pageselect = parseInt(i) - 1; 
				    row += '<li class="page-item test-page-number '+ active +'" style="'+ displayZero +'">';
				    row += '<a class="page-link" onclick="display_data('+ pageselect +'); return false" href="#"> '+ i +' </a>';
				    row += '</li>';
				    b = i;  
		    	}
	    	} 
		}  
		var last = parseInt(result.total_pagination) - 1; 
		if (b < result.total_pagination) {
			row += '<li class="disabled" style="background:transparent;"><span style="background: none;border:none;color:#000">. . .</span></li>';	
			row += '<li class="page-item test-page-number">'; 
			row += '<a class="page-link" onclick="display_data('+ last +'); return false" href="#"> '+ result.total_pagination +' </a>';
			row += '</li>';	
		}

		if ((parseInt(page_selected) + 1) == result.total_pagination) 
    	{
    		displayNext = 'display:none';
    	} 
		row += '<li class="page-item" style="'+ displayNext +'">'; 
		row += '<a class="page-link" href="#"> Next </a></li>';    
		$('nav.test-paginate ul.pagination').append(row); 
   		$(loader).hide(); 
   		$(table).css({'filter':'blur(0px)'});
   		$(body).css({'cursor':'default'}); 	 
    }); 
}

function total_data(total){
	totaldata = parseInt(total) - 1; 
}

$(document).on("click","nav.test-paginate li:first-child",function(e){ 
	if(page_selected != 0){
		var pageselect = parseInt(page_selected) - 1; 
		display_data(pageselect); 
	} 
	return false; 
}); 
$(document).on("click","nav.test-paginate li:last-child",function(e){  
	if(page_selected != totaldata){
		var pageselect = parseInt(page_selected) + 1;  
		display_data(pageselect);  
	} 
	return false;
}); 

$(document).on("keyup","#txtsearch",function(e){   
	display_data(0);  
}); 
