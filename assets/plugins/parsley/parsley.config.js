window.ParsleyConfig = {
    errorClass: 'has-error',
    successClass: 'has-success',
    classHandler: function(ParsleyField) {
        return ParsleyField.$element.parent();
    },
    errorsContainer: function(ParsleyField) {
        return ParsleyField.$element.parent();
    },
    errorsWrapper: '<span class="help-block">',
    errorTemplate: '<div></div>'
};