$(function(){

	var table = '';
	var getCMSDeferred = '';

	init();

	function init() {
		getCMSDeferred = getCMS();
	}

	$(document).on('click', '.delete-cms-btn', function(event){
		event.preventDefault();

		var cms_id = $(this).attr('data-cms-id');

		bootbox.confirm('Are you sure?', function(response){
			if(response) {
				deleteCMS(cms_id);
			} else {
				$('.modal').modal('hide');
			}
		});
	});

	function getCMS() {
		var deferred = $.Deferred();

		$('.master-cms-panel').addClass('serino-loading');
	
		setTimeout(function(){
			$.ajax({
				url: base_url+'v1/cms',
				method: 'GET',
				accepts: 'application/json',
				dataType: 'json'
			}).done(function(data){
				var template = $('#cms-row-template').html();
				var table_tbody = '';
				Mustache.parse(template);

				$(data.items).each(function(i, rows){
					var date_created = moment(rows.data_created).format('MMM DD, YYYY');
					rows.date_created = date_created;
					table_tbody += Mustache.render(template, rows);
				});

				$('#cms-table tbody').html(table_tbody);

				//Datatables
				table = $('#cms-table').dataTable({
					responsive: true,
				  	destroy: true,
					'order': [0, 'desc'],
			    	'aoColumnDefs': [
					  {
					    bSortable: false,
					    bSearchable: false,
					    aTargets: [-1]
					  }
					]
				});

			}).always(function(){
				$('.master-cms-panel').removeClass('serino-loading');
				deferred.resolve();
			}).fail(function(){
				$('.master-cms-panel').addClass('serino-error');
			});
		}, 1000);

		return deferred.promise();
	}

	function deleteCMS(cms_id) {
		$('.master-cms-panel').addClass('serino-loading');

		$.ajax({
			url: base_url+'v1/cms/'+cms_id,
			method: 'DELETE',
			accepts: 'application/json',
			dataType: 'json'
		}).done(function(data){
			table.fnDestroy();
			getCMSDeferred = getCMS();
			getCMSDeferred.done(function(){
				if(data.successful) {
					$.notify(data.message, {type:"success"});
				} else {
					$.notify(data.message, {type:"danger"});
				}
			});
		}).always(function(){
			
		}).fail(function(){
			$.notify(data.message, {type:"danger"});
		});
	}
});