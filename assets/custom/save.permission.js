$(function(){

	init();

	function init() {
		$('#permission_remarks').summernote({
			height: 250
		});

		if($('#form-mode').val() == 'edit') {
			getPermissionDeferred = getPermission($('#form-mode').attr('data-permission-id'));

			getPermissionDeferred.done(function(data){
				$('#permission_title').val(data.model.permission_title);
				$('#permission_description').val(data.model.permission_description);
				$('#permission_group').val(data.model.permission_group);
				$("#permission_remarks").summernote("code",data.model.permission_remarks);
			});
		}
	}

	$('#save-permission-btn').on('click',function(event){
		event.preventDefault();
		savePermission($('#form-mode').attr('data-permission-id'));
	});

	function savePermission(permission_id) {
		$('.master-cms-panel').addClass('serino-loading');

		if(permission_id) {
			$.ajax({
				url: base_url+'v1/permissions/'+permission_id,
				type: 'POST',
				data: $('#save-permission-form').serialize(),
				dataType: 'json',
				accepts: 'application/json'
			}).done(function(data){
				if(data.successful) {
					$.notify(data.message, {type:"success"});
				} else {
					$.notify(data.message, {type:"danger"});
				}
			}).fail(function(){
				$('.master-cms-panel').addClass('serino-error');
			}).always(function(){
				$('.master-cms-panel').removeClass('serino-loading');
			});
		} else {
			$.ajax({
				url: base_url+'v1/permissions/',
				type: 'POST',
				data: $('#save-permission-form').serialize(),
				dataType: 'json',
				accepts: 'application/json'
			}).done(function(data){
				if(data.successful) {
					window.location = base_url+'permissions/save_permission/'+data.permission_id;
				} else {
					$.notify(data.message, {type:"danger"});
					$('.master-cms-panel').removeClass('serino-loading');
				}
			}).fail(function(){
				$('.master-cms-panel').addClass('serino-error');
			});
		}
	}

});