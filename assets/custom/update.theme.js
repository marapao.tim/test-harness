$(function(){

	init();

	function init() {
		$('#version_remarks').summernote({
			height: 250
		});

		$('#cms_support').multiselect();
	}

	$(document).on('click','.update-theme-btn', function(event){
		event.preventDefault();
		updateTheme($('#form-mode').attr('data-theme-id'));
	});

	function updateTheme(theme_id) {
		$('.master-cms-panel').addClass('serino-loading');

		var formData = new FormData($('#update-theme-form')[0]);

		console.log($('#update-theme-form')[0]);

		setTimeout(function(){
			$.ajax({
				url: base_url+'v1/themes/'+theme_id+'/update',
				type: 'POST',
				contentType: false,
	        	processData: false,
				data: formData,
				dataType: 'json',
				accepts: 'application/json'
			}).done(function(data){
				if(data.successful) {
					window.location = base_url+'themes/view_theme/'+getUriSegment(4);
				} else {
					$.notify(data.message, {type:"danger"});
				}
			}).fail(function(){
				$('.master-cms-panel').addClass('serino-error');
			}).always(function(){
				$('.master-cms-panel').removeClass('serino-loading');
			});
		}, 1000);
	}
});