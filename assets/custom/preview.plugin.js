$(function(){

	var getPluginDeffered = '';
	init();

	$(document).on('click', '.delete-plugin-btn', function(event){
		event.preventDefault();

		var plugin_id = $(this).attr('data-plugin-id');

		bootbox.confirm('Are you sure?', function(response){
			if(response) {

				getPluginDeffered = deletePlugin(plugin_id);
				getPluginDeffered.done(function(data){
					if(data.successful) {
						window.location = base_url+'plugin/plugin_management/';
					} else {
						$.notify(data.message, {type:"danger"});
					}
				});

			} else {
				$('.modal').modal('hide');
			}
		});
	});

	function init() {
		$('.master-cms-panel').addClass('serino-loading');
		$('.plugin-versions-panel').addClass('serino-loading');

		getPluginDeffered = getPlugin(getUriSegment(4));

		getPluginDeffered.done(function(data){
			$('.data-plugin-id').html(data.model.plugin_id);
			$('.data-plugin-cms-id').html(data.model.plugin_cms_id);
			$('.data-plugin-title').html(data.model.title);
			$('.data-plugin-author').html(data.model.author);
			$('.data-plugin-current-version').html(data.model.version);
			$('.data-plugin-category').html(data.model.category);
			$('.data-plugin-tags').html('<span class="label label-primary">'+data.model.tags.split(',').join('</span> <span class="label label-primary">')+'</span>');
			$('.data-plugin-description').html(data.model.plugin_id);
			$('.data-plugin-created-by').text(data.model.created_by);
			$('.data-plugin-date-created').html('<abbr class="timeago" rel="tooltip" title="'+moment(data.model.date_created).format('MMM D, YYYY h:mm a')+'"></abbr>');

			if(data.model.modified_by !== null) {
				$('.modification-details-container').removeClass('hidden');

				$('.data-plugin-modified-by').text(data.model.modified_by);
				$('.data-plugin-date-modified').html('<abbr class="timeago" rel="tooltip" title="'+moment(data.model.date_modified).format('MMM D, YYYY h:mm a')+'"></abbr>');
			}

			var version_metas = Object.keys(data.model.version_metas).map(function (key) {return data.model.version_metas[key]});
			version_metas = version_metas.reverse();

			var template = $('#plugin-version-template').html();
			Mustache.parse(template);
			var timeline_html = '';

			$(version_metas).each(function(i, rows){
				console.log(rows);

				if(parseFloat(rows.version) == parseFloat(data.model.version))
					rows.icon_class = 'milestone-success';
				else
					rows.icon_class = 'milestone-default';

				rows.release_date = '<abbr class="timeago" rel="tooltip" title="'+moment(rows.release_date).format('MMM D, YYYY h:mm a')+'"></abbr>';

				rows.cms_support = '<span class="badge">'+rows.cms_support.split(',').join('</span> <span class="badge">')+'</span>';
				rows.path_to_file = base_url+'store/plugins/'+data.model.plugin_cms_id+'/'+data.model.version+'/'+data.model.plugin_cms_id+'.zip';
				rows.path_to_screenshot = base_url+'/store/plugins/'+data.model.plugin_cms_id+'/'+data.model.version+'/screenshot.png';
				rows.plugin_cms_id = data.model.plugin_cms_id;

				var row_html = Mustache.render(template, rows);
				timeline_html += row_html;
			});

			$('.plugin-version-timeline').html(timeline_html);
			$("abbr.timeago").timeago();

			$('a[data-toggle^=lightcase]').lightcase();

			$('.master-cms-panel').removeClass('serino-loading');
			$('.plugin-versions-panel').removeClass('serino-loading');
		});
	}
});