$(function(){

	var table = '';
	var getThemesDeferred = '';

	init();

	function init() {
		getThemesDeferred = getThemes();
	}

	function getThemes() {
		var deferred = $.Deferred();

		$('.master-cms-panel').addClass('serino-loading');

		setTimeout(function(){
			$.ajax({
				url: base_url+'v1/themes',
				method: 'GET',
				accepts: 'application/json',
				dataType: 'json'
			}).done(function(data){
				var template = $('#theme-row-template').html();
				var table_tbody = '';
				Mustache.parse(template);

				$(data.items).each(function(i, rows){
					var row_html = Mustache.render(template, rows);
					table_tbody += row_html;
				});

				$('#themes-table tbody').html(table_tbody);

				//Datatables
				table = $('#themes-table').dataTable({
					responsive: true,
					destroy: true,
					'order': [],
			    	'aoColumnDefs': [
					  {
					     bSortable: false,
					     bSearchable: false,
					     aTargets: [-1]
					  }
					]
				});
			}).always(function(){
					$('.master-cms-panel').removeClass('serino-loading');
					deferred.resolve();
			}).fail(function(){
				$('.master-cms-panel').addClass('serino-error');
			});
		}, 1000);

		return deferred.promise();
	}

	$(document).on('click', '.delete-theme-btn', function(event){
		event.preventDefault();

		var theme_id = $(this).attr('data-theme-id');

		bootbox.confirm('Are you sure?', function(response){
			if(response) {
				deleteThemeDeferred = deleteTheme(theme_id);
				deleteThemeDeferred.done(function(data){
					table.fnDestroy();
					getThemesDeferred = getThemes();
					getThemesDeferred.done(function(){
						if(data.successful) {
							$.notify(data.message, {type:"success"});
						} else {
							$.notify(data.message, {type:"danger"});
						}
					});

				});
			} else {
				$('.modal').modal('hide');
			}
		});
	});
});