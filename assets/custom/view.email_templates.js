$(document).ready(function(){
	//Datatables
	var _datatable = $('.datatable').dataTable({
		responsive: true,
		'order': [],
	});

	$(document).on('click', '.delete-template-btn', function(event){
		event.preventDefault();

		var template_id = $(this).attr('data-template-id');

		bootbox.confirm('Are you sure?', function(response){
			if(response) {

				$.ajax({
					url: base_url+'email_templates/delete/'+template_id,
					method: 'DELETE',
					accepts: 'application/json',
					dataType: 'json'
				}).done(function(data){
					//deferred.resolve(data);
				}).always(function(){
					_datatable.fnDestroy()
					$("#tools-table").parent().load(document.URL + " #tools-table",function(){
						_datatable = $('.datatable').dataTable({
							responsive: true,
							"order": []
						});
					});
				}).fail(function(){
					$.notify(data.message, {type:"danger"});
				});

			} else {
				$('.modal').modal('hide');
			}
		});
	});


});