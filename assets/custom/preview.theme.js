$(function(){

	var getThemeDeferred = '';
	init();

	$(document).on('click', '.delete-theme-btn', function(event){
		event.preventDefault();

		var theme_id = $(this).attr('data-theme-id');

		bootbox.confirm('Are you sure?', function(response){
			if(response) {

				deleteThemeDeferred = deleteTheme(theme_id);
				deleteThemeDeferred.done(function(data){
					if(data.successful) {
						window.location = base_url+'themes/themes_management/';
					} else {
						$.notify(data.message, {type:"danger"});
					}
				});

			} else {
				$('.modal').modal('hide');
			}
		});
	});

	function init() {
		$('.master-cms-panel').addClass('serino-loading');
		$('.theme-versions-panel').addClass('serino-loading');

		getThemeDeferred = getTheme(getUriSegment(4));

		getThemeDeferred.done(function(data){
			$('.data-theme-id').html(data.model.theme_id);
			$('.data-theme-cms-id').html(data.model.theme_cms_id);
			$('.data-theme-title').html(data.model.theme_title);
			$('.data-theme-author').html(data.model.theme_author);
			$('.data-theme-current-version').html(data.model.theme_version);
			$('.data-theme-category').html(data.model.theme_category);
			$('.data-theme-tags').html('<span class="label label-primary">'+data.model.theme_tags.split(',').join('</span> <span class="label label-primary">')+'</span>');
			$('.data-theme-description').html(data.model.theme_id);
			$('.data-theme-created-by').text(data.model.created_by);
			$('.data-theme-date-created').html('<abbr class="timeago" rel="tooltip" title="'+moment(data.model.date_created).format('MMM D, YYYY h:mm a')+'"></abbr>');

			if(data.model.modified_by !== null) {
				$('.modification-details-container').removeClass('hidden');

				$('.data-theme-modified-by').text(data.model.modified_by);
				$('.data-theme-date-modified').html('<abbr class="timeago" rel="tooltip" title="'+moment(data.model.date_modified).format('MMM D, YYYY h:mm a')+'"></abbr>');
			}

			var version_metas = Object.keys(data.model.version_metas).map(function (key) {return data.model.version_metas[key]});
			version_metas = version_metas.reverse();

			var template = $('#theme-version-template').html();
			Mustache.parse(template);
			var timeline_html = '';


			$(version_metas).each(function(i, rows){

				if(parseFloat(rows.version) == parseFloat(data.model.theme_version))
					rows.icon_class = 'milestone-success';
				else
					rows.icon_class = 'milestone-default';

				rows.release_date = '<abbr class="timeago" rel="tooltip" title="'+moment(rows.release_date).format('MMM D, YYYY h:mm a')+'"></abbr>';

				rows.cms_support = '<span class="badge">'+rows.cms_support.split(',').join('</span> <span class="badge">')+'</span>';
				rows.path_to_file = base_url+'/store/themes/'+data.model.theme_cms_id+'/'+data.model.theme_version+'/'+data.model.theme_cms_id+'.zip';
				rows.path_to_screenshot = base_url+'/store/themes/'+data.model.theme_cms_id+'/'+data.model.theme_version+'/screenshot.png';
				rows.theme_cms_id = data.model.theme_cms_id;

				var row_html = Mustache.render(template, rows);
				timeline_html += row_html;
			});

			$('.theme-version-timeline').html(timeline_html);
			$("abbr.timeago").timeago();

			$('a[data-toggle^=lightcase]').lightcase();

			$('.master-cms-panel').removeClass('serino-loading');
			$('.theme-versions-panel').removeClass('serino-loading');
		});
	}
});