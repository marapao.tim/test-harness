$(function(){
	var table = $('#user-roles-table');
	var oTable = "";

	init();

	function init() {
		getUserRoles();
	}

	function getUserRoles() {
		$('.master-cms-panel').addClass('serino-loading');

		$.ajax({
			url: base_url+'v1/user_roles',
			method: 'GET',
			accepts: 'application/json',
			dataType: 'json',
			beforeSend:function() {
				if($.fn.DataTable.fnIsDataTable(table)) {
					table.fnDestroy();
				}
			}
		}).done(function(data){
			var template = $('#user-role-row-template').html();
			var table_tbody = '';
			Mustache.parse(template);

			$(data.items).each(function(i, rows){
				var row_html = Mustache.render(template, rows);
				table_tbody += row_html;
			});

			$('#user-roles-table tbody').html(table_tbody);
			//Datatables
			oTable = table.dataTable({
				responsive: true,
				destroy: true,
				autoWidth: false,
				'order': [],
		    	'aoColumnDefs': [
					{
						 bSortable: false,
						 bSearchable: false,
						 aTargets: [ 0, -1]
					},
					{
						"targets": [ 7 ],
						"visible": false,
						"searchable": false
					}
				]
			});
		}).always(function(){
				$('.master-cms-panel').removeClass('serino-loading');
		}).fail(function(){
			$('.master-cms-panel').addClass('serino-error');
		});
	}

	function format ( d ) {
    	// `d` is the original data object for the row
    	var permissions = d[7].split(',');
    	var appendString = '<h6>Permissions: <h6> <p>';
    	for (index = 0; index < permissions.length; index++) {
		    appendString += '<button class="btn btn-primary btn-sm" type="button" style="margin-bottom:3px;">' + permissions[index] + ' <i class="ion-key"></i></button> ';
		}
	    appendString += "</p>"
	    return appendString;
	}

	$(document).on('click', '.delete-user-role-btn', function(event){
		event.preventDefault();

		var user_role_id = $(this).attr('data-user-role-id');

		bootbox.confirm('Are you sure?', function(response){
			if(response) {
				deleteUserRoleDeferred = deleteUserRole(user_role_id);
				deleteUserRoleDeferred.done(function(data){
					if(data.successful) {
						getUserRoles();
						$.notify(data.message, {type:"success"});
					} else {
						$.notify(data.message, {type:"danger"});
					}

				});
			} else {
				$('.modal').modal('hide');
			}
		});
	});

	 // Add event listener for opening and closing details
	$('#user-roles-table tbody').on('click', 'td.details-control', function() {
        var tr = $(this).closest('tr');
        var row = oTable.api().row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
});