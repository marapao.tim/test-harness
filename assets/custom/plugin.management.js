$(function(){

	var table = '';
	var getPluginsDeffered = '';
	var deletePluginDeffered = '';

	init();

	function init() {
		getPluginsDeffered = getplugins();
	}

	function getplugins() {
		var deferred = $.Deferred();

		$('.master-cms-panel').addClass('serino-loading');

		setTimeout(function(){
			$.ajax({
				url: base_url+'v1/plugins',
				method: 'GET',
				accepts: 'application/json',
				dataType: 'json'
			}).done(function(data){
				var template = $('#plugin-row-template').html();
				var table_tbody = '';
				Mustache.parse(template);

				$(data.items).each(function(i, rows){
					var row_html = Mustache.render(template, rows);
					table_tbody += row_html;
				});

				$('#plugins-table tbody').html(table_tbody);

				//Datatables
				table = $('#plugins-table').dataTable({
					responsive: true,
					destroy: true,
					'order': [],
			    	'aoColumnDefs': [
					  {
					     bSortable: false,
					     bSearchable: false,
					     aTargets: [-1]
					  }
					]
				});
			}).always(function(){
					$('.master-cms-panel').removeClass('serino-loading');
					deferred.resolve();
			}).fail(function(){
				$('.master-cms-panel').addClass('serino-error');
			});
		}, 1000);

		return deferred.promise();
	}

	$(document).on('click', '.delete-plugin-btn', function(event){
		event.preventDefault();

		var tool_id = $(this).attr('data-plugin-id');

		bootbox.confirm('Are you sure?', function(response){
			if(response) {

				deletePluginDeffered = deletePlugin(tool_id);
				deletePluginDeffered.done(function(data){
					table.fnDestroy();
					getPluginsDeffered = getplugins();
					getPluginsDeffered.done(function(){
						if(data.successful) {
							$.notify(data.message, {type:"success"});
						} else {
							$.notify(data.message, {type:"danger"});
						}
					});

				});

			} else {
				$('.modal').modal('hide');
			}
		});
	});
});

