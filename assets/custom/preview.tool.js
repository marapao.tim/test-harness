$(function(){

	var getToolDeferred = '';
	init();

	$(document).on('click', '.delete-tool-btn', function(event){
		event.preventDefault();

		var tool_id = $(this).attr('data-tool-id');

		bootbox.confirm('Are you sure?', function(response){
			if(response) {

				deleteToolDeffered = deleteTool(tool_id);
				deleteToolDeffered.done(function(data){
					if(data.successful) {
						window.location = base_url+'tools/tools_management/';
					} else {
						$.notify(data.message, {type:"danger"});
					}
				});

			} else {
				$('.modal').modal('hide');
			}
		});
	});

	function init() {
		$('.master-cms-panel').addClass('serino-loading');
		$('.tool-versions-panel').addClass('serino-loading');

		getToolDeferred = getTool(getUriSegment(4));

		getToolDeferred.done(function(data){
			$('.data-tool-id').html(data.model.tool_id);
			$('.data-tool-cms-id').html(data.model.tool_cms_id);
			$('.data-tool-title').html(data.model.tool_title);
			$('.data-tool-type').html(data.model.tool_type);
			$('.data-tool-author').html(data.model.tool_author);
			$('.data-tool-current-version').html(data.model.tool_version);
			$('.data-tool-category').html(data.model.tool_category);
			$('.data-tool-tags').html('<span class="label label-primary">'+data.model.tool_tags.split(',').join('</span> <span class="label label-primary">')+'</span>');
			$('.data-tool-description').html(data.model.tool_id);
			$('.data-tool-created-by').text(data.model.created_by);
			$('.data-tool-date-created').html('<abbr class="timeago" rel="tooltip" title="'+moment(data.model.date_created).format('MMM D, YYYY h:mm a')+'"></abbr>');

			if(data.model.modified_by !== null) {
				$('.modification-details-container').removeClass('hidden');

				$('.data-tool-modified-by').text(data.model.modified_by);
				$('.data-tool-date-modified').html('<abbr class="timeago" rel="tooltip" title="'+moment(data.model.date_modified).format('MMM D, YYYY h:mm a')+'"></abbr>');
			}

			var version_metas = Object.keys(data.model.version_metas).map(function (key) {return data.model.version_metas[key]});
			version_metas = version_metas.reverse();

			var template = $('#tool-version-template').html();
			Mustache.parse(template);
			var timeline_html = '';

			$(version_metas).each(function(i, rows){
				console.log(rows);

				if(parseFloat(rows.version) == parseFloat(data.model.tool_version))
					rows.icon_class = 'milestone-success';
				else
					rows.icon_class = 'milestone-default';

				rows.release_date = '<abbr class="timeago" rel="tooltip" title="'+moment(rows.release_date).format('MMM D, YYYY h:mm a')+'"></abbr>';

				rows.cms_support = '<span class="badge">'+rows.cms_support.split(',').join('</span> <span class="badge">')+'</span>';
				rows.path_to_file = base_url+'/store/tools/'+data.model.tool_cms_id+'/'+data.model.tool_version+'/'+data.model.tool_cms_id+'.zip';
				rows.path_to_screenshot = base_url+'/store/tools/'+data.model.tool_cms_id+'/'+data.model.tool_version+'/screenshot.png';
				rows.tool_cms_id = data.model.tool_cms_id;

				var row_html = Mustache.render(template, rows);
				timeline_html += row_html;
			});

			$('.tool-version-timeline').html(timeline_html);
			$("abbr.timeago").timeago();

			$('a[data-toggle^=lightcase]').lightcase();

			$('.master-cms-panel').removeClass('serino-loading');
			$('.tool-versions-panel').removeClass('serino-loading');
		});
	}
});