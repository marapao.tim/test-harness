$(function(){

	init();

	function init() {
		$('[data-toggle="tooltip"]').tooltip()
		
		$('#user_role_remarks').summernote({
			height: 250
		});	

		if($('#form-mode').val() == 'edit') {
			getUserRoleDeferred = getUserRole($('#form-mode').attr('data-user-role-id'));

			getUserRoleDeferred.done(function(data){
				$('#user_role_title').val(data.model.user_role_title);
				$('#user_role_description').val(data.model.user_role_description);
				$("#user_role_remarks").summernote("code",data.model.user_role_remarks);
			});
		}
	}

	$('#save-user-role-btn').on('click',function(event){
		event.preventDefault();
		saveUserRole($('#form-mode').attr('data-user-role-id'));
	});

	function saveUserRole(user_role_id) {
		$('.master-cms-panel').addClass('serino-loading');

		if(user_role_id) {
			$.ajax({
				url: base_url+'v1/user_roles/'+user_role_id,
				type: 'POST',
				data: $('#save-user-role-form').serialize(),
				dataType: 'json',
				accepts: 'application/json'
			}).done(function(data){
				if(data.successful) {
					$.notify(data.message, {type:"success"});
				} else {
					$.notify(data.message, {type:"danger"});
				}
			}).fail(function(){
				$('.master-cms-panel').addClass('serino-error');
			}).always(function(){
				$('.master-cms-panel').removeClass('serino-loading');
			});
		} else {
			$.ajax({
				url: base_url+'v1/user_roles/',
				type: 'POST',
				data: $('#save-user-role-form').serialize(),
				dataType: 'json',
				accepts: 'application/json'
			}).done(function(data){
				if(data.successful) {
					window.location = base_url+'user_roles/save_user_role/'+data.user_role_id;
				} else {
					$.notify(data.message, {type:"danger"});
					$('.master-cms-panel').removeClass('serino-loading');
				}
			}).fail(function(){
				$('.master-cms-panel').addClass('serino-error');
			});
		}
	}

	$('li .select-all').click(function() {
		var container = $(this).closest('ul');
		var selectAll = container.find('input[type=checkbox].select-all').is(':checked');
		var listItems = container.find('input[type=checkbox].select-permission');
		listItems.each(function() {
			$(this).prop('checked', selectAll);
		})
	})

});