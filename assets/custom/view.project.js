var _dataTable;
$(document).ready(function(){
	$(".fetch-config").click(function(){
		var _token = $(this).attr("token");
		var _instance = $(this).attr("data-instance");
		var _ins_env = $(this).attr("ins-env");
		var _accountID = $("#"+_ins_env+"_account_id").val();
		var _siteID = $("#"+_ins_env+"_site_id").val();

		if(_accountID != "" && _siteID != ""){
			bootbox.confirm("Fetch Configs from AccountID: "+_accountID+"; SiteID: "+_siteID,function(resp){
				if(resp){
					$.ajax({
						url: _instance + "/" + _ins_env + '/get-config/'+ _accountID + '/'+_siteID,
						method: 'GET',
						headers: {
							Authorization: 'Bearer ' + _token
						},
						accept: 'application/json',
					}).done(function(data) {
						if(!data.message){
							$("#"+_ins_env+"_context").val(data["idms_context"]);
							$("#"+_ins_env+"_integration_key").val(data["idms_integration_key"]);
							$("#"+_ins_env+"_db_name").val(data["db_name"]);

							bootbox.hideAll();
						}
						else{
							var _message = "An error has occured!";
							if(data.message){
								_message = data.message;
							}
							bootbox.alert(_message);
						}

					}).fail(function(data) {
						var _message = "An error has occured!";
						if(data.message){
							_message = data.message;
						}
						bootbox.alert(_message);
					}) 
				}
			});
		}
		else{
			bootbox.alert("Account ID and Site ID are required!");
		}
		
		/*bootbox.dialog({
			title: "Instance",
			message: $("#fetch-config-template").html(),
			buttons: {
		        confirm: {
		            label: 'Confirm',
		            className: 'btn-primary',
		            callback: function(){
		            	var _accountID = $(".bootbox.modal [name='fetch-accountid']").val();
		            	var _siteID = $(".bootbox.modal [name='fetch-siteid']").val();
				    	$.ajax({
							url: _instance + '?action=get-config&accountid='+ _accountID + '&siteid='+_siteID,
							method: 'GET',
							headers: {
								Authorization: 'Bearer ' + _token
							},
							accept: 'application/json',
						}).done(function(data) {
							if(!data.message){
								$("#"+_ins_env+"_context").val(data["idms_context"]);
								$("#"+_ins_env+"_integration_key").val(data["idms_integration_key"]);
								$("#"+_ins_env+"_db_name").val(data["db_name"]);

								bootbox.hideAll();
							}
							else{
								$(".bootbox.modal #fetch-modal-alerts").html(data.message).removeClass("hidden");
							}

						}).fail(function(data) {
							$(".bootbox.modal #fetch-modal-alerts").html(data.message).removeClass("hidden");
						}) 

				    	
				    	return false;
				    }
		        },
		        cancel: {
		            label: 'Cancel',
		            className: 'btn-default'
		        }
		    },

		});*/
	});

	$(".fetch-assets").click(function(){
		var _token = $(this).attr("token");
		var _instance = $(this).attr("data-instance");
		var _ins_env = $(this).attr("ins-env");
		var _accountID = $("#"+_ins_env+"_account_id").val();
		var _siteID = $("#"+_ins_env+"_site_id").val();

		if(_accountID != "" && _siteID != ""){
			bootbox.confirm("Fetch Assets from AccountID: "+_accountID+"; SiteID: "+_siteID,function(resp){
				if(resp){
					$.ajax({
						url: _instance + "/" + _ins_env+'/get-srn-contents/'+ _accountID + '/'+_siteID,
						method: 'GET',
						headers: {
							Authorization: 'Bearer ' + _token
						},
						accept: 'application/json',
					}).done(function(data) {
						if(!data.message){
							var _themes = "";
							$.each(data["themes"],function(i,val){
								_themes += val+"\n";
							});
							$("#"+_ins_env+"_assets_themes").html(_themes);

							var _tools = "";
							$.each(data["tools"],function(i,val){
								_tools += val+"\n";
							});
							$("#"+_ins_env+"_assets_tools").html(_tools);

							var _plugins = "";
							$.each(data["plugins"],function(i,val){
								if(val.indexOf("tmp_") < 0){
									_plugins += val+"\n";	
								}
								
							});
							$("#"+_ins_env+"_assets_plugins").html(_plugins);

							bootbox.hideAll();
						}
						else{
							var _message = "An error has occured!";
							if(data.message){
								_message = data.message;
							}
							bootbox.alert(_message);
						}

					}).fail(function(data) {
						var _message = "An error has occured!";
						if(data.message){
							_message = data.message;
						}
						bootbox.alert(_message);
					}) 
				}
			});
		}
		else{
			bootbox.alert("Account ID and Site ID are required!");
		}
		
		/*bootbox.dialog({
			title: "Instance",
			message: $("#fetch-config-template").html(),
			buttons: {
		        confirm: {
		            label: 'Confirm',
		            className: 'btn-primary',
		            callback: function(){
		            	var _accountID = $(".bootbox.modal [name='fetch-accountid']").val();
		            	var _siteID = $(".bootbox.modal [name='fetch-siteid']").val();
				    	$.ajax({
							url: _instance + '?action=get-config&accountid='+ _accountID + '&siteid='+_siteID,
							method: 'GET',
							headers: {
								Authorization: 'Bearer ' + _token
							},
							accept: 'application/json',
						}).done(function(data) {
							if(!data.message){
								$("#"+_ins_env+"_context").val(data["idms_context"]);
								$("#"+_ins_env+"_integration_key").val(data["idms_integration_key"]);
								$("#"+_ins_env+"_db_name").val(data["db_name"]);

								bootbox.hideAll();
							}
							else{
								$(".bootbox.modal #fetch-modal-alerts").html(data.message).removeClass("hidden");
							}

						}).fail(function(data) {
							$(".bootbox.modal #fetch-modal-alerts").html(data.message).removeClass("hidden");
						}) 

				    	
				    	return false;
				    }
		        },
		        cancel: {
		            label: 'Cancel',
		            className: 'btn-default'
		        }
		    },

		});*/
	});

	$('.multiselect').multiSelect({
		selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Search...'><br>",
		selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Search...'><br>",
		afterInit: function(ms){
			var that = this,
			$selectableSearch = that.$selectableUl.prev().prev(),
			$selectionSearch = that.$selectionUl.prev().prev(),
			selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
			selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

			that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
			.on('keydown', function(e){
				if (e.which === 40){
					that.$selectableUl.focus();
					return false;
				}
			});

			that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
			.on('keydown', function(e){
				if (e.which == 40){
					that.$selectionUl.focus();
					return false;
				}
			});
		},
		afterSelect: function(){
			this.qs1.cache();
			this.qs2.cache();
		},
		afterDeselect: function(){
			this.qs1.cache();
			this.qs2.cache();
		}
	});

	$("#upload-artifact").click(function(){

		bootbox.dialog({
			title: "Upload New Artifact",
			message: $("#modal-artifact-template").html(),
			buttons: {
		        confirm: {
		            label: 'Confirm',
		            className: 'btn-primary',
		            callback: function(){
		            	var _this = $(this);
		            	var _modal = $(".bootbox.modal .modal-content");

		            	var formData = new FormData(_modal.find('form')[0]);

		            	var project_file = _modal.closest("[type='file']").prop('files');

						var form = _modal.closest('form');
						var fd = new FormData();

						$.ajax({
							url: _modal.find("form").attr('data-target'),
							type: 'POST',
							data: formData,
							processData: false,
							contentType: false,
							dataType: 'json',
							accepts: 'application/json',
							beforeSend: function() {
								
							},
							success: function(data) {
								_dataTable.fnDestroy()
								$("#artifacts_list").parent().load(document.URL + " #artifacts_list",function(){
									_dataTable = $('.datatable').dataTable({
										"order": [[ 0, "desc" ]]
									});
								});
							},
							complete: function(data) {

							},
							error: function(data) {

							}
						});
				    }
		        },
		        cancel: {
		            label: 'Cancel',
		            className: 'btn-default'
		        }
		    },

		});
	});

	$(document).on("click",".btn-view-files",function(){
		var _this = $(this);
		var _container = _this.closest("tr").find(".files-list-container").html();

		var dialog = bootbox.dialog({
			title: "File Versions",
			size: "large",
			message: _container,
			buttons: {
		        
		        cancel: {
		            label: 'Close',
		            className: 'btn-default'
		        }
		    },

		});

		dialog.init(function(){
		    dialog.find("table").dataTable({
		    	"order": [[ 0, "desc" ]]
		    });
		});
	})

	$(document).on("click",".btn-artifact-files",function(){
		bootbox.hideAll();
		var _this = $(this);
		var artifact_id = _this.attr("artifact-id");
		var api_url = _this.attr("url");
		var v_major = _this.attr("version-major");
		var v_minor = _this.attr("version-minor");
		var v_patch = _this.attr("version-patch");

		var x_container = $("#modal-files-uploader-template").clone();
		var _container = $("<div>");
		_container.html(x_container.html());

		_container.find("[name='file_major_version']").attr("value",v_major);
		_container.find("[name='file_major_version']").attr("min",v_major);

		_container.find("[name='file_minor_version']").attr("value",v_minor);
		_container.find("[name='file_minor_version']").attr("min",v_minor);

		_container.find("[name='file_patch_version']").attr("value",v_patch);
		_container.find("[name='file_patch_version']").attr("min",v_patch);

		_container.find(".artifact_name").html(_this.attr("a-name"));
		_container.find(".latest_version").html(v_major+"."+v_minor+"."+v_patch);
		

		var dialog = bootbox.dialog({
			title: "Upload New File Versions",
			message: _container.html(),
			buttons: {
		        confirm: {
		            label: 'Confirm',
		            className: 'btn-primary',
		            callback: function(){
		            	var _this = $(this);
		            	var _modal = $(".bootbox.modal .modal-content");

		            	var formData = new FormData(_modal.find('form')[0]);

		            	var project_file = _modal.closest("[type='file']").prop('files');

						var form = _modal.closest('form');
						var fd = new FormData();

						$.ajax({
							url: api_url,
							type: 'POST',
							data: formData,
							processData: false,
							contentType: false,
							dataType: 'json',
							accepts: 'application/json',
							beforeSend: function() {
								
							},
							success: function(data) {
								_dataTable.fnDestroy()
								$("#artifacts_list").parent().load(document.URL + " #artifacts_list",function(){
									_dataTable = $('.datatable').dataTable({
										"order": [[ 0, "desc" ]]
									});
								});
							},
							complete: function(data) {

							},
							error: function(data) {

							}
						});
				    }
		        },
		        cancel: {
		            label: 'Close',
		            className: 'btn-default'
		        }
		    },

		});

		

	});

	//event listener for theme name
	$('input[name="project_title"]').keyup(function(){
		var project_title = $(this).val();
		project_title = project_title.split(' ').join('-').toLowerCase();
		$('input[name="project_name"]').val(project_title);
	});

	window.Parsley
		.addValidator('projectnameexists', {
		requirementType: 'string',
		validateString: function(value, requirement) {
			return check_project_exists(requirement, value);
		},
		messages: {
			en: 'Project name already exists.'
		}
	});
});

//project logo upload
$('.project-logo-uploader').on('change',function(event){
	
	var project_logo = $(this)[0].files[0];
	var form = $(this).closest('form');
	var fd = new FormData();

	fd.append('project_logo', project_logo);

	$.ajax({
		url: form.attr('action'),
		type: 'POST',
		data: fd,
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		beforeSend: function() {
			$('.project-logo-container img').addClass('hidden');
			$('.project-logo-container div.loader4').removeClass('hidden');
		},
		success: function(data) {
			$('.project-logo-container img').attr('src', base_url+'store/projects/'+data.project_name+'/images/150x150/'+data.new_image);
		},
		complete: function(data) {
			setTimeout(function(){
				$('.project-logo-container img').removeClass('hidden');
				$('.project-logo-container div.loader4').addClass('hidden');
			}, 5000);
		},
		error: function(data) {
			console.log(data);
		}
	});
});

function check_project_exists(url, value) {

	//var response = 'false';

	$.ajax({
		method: 'POST',
		data: {project_name: value},
		url: url,
		async: false,
		success: function(data){
			if(data == 1) {
				response = true;
			} else {
				response = false;
			}
		},
		error: function(data) {
		}
	});

	return response;
}

//select2
$('.select2').select2();

_dataTable = $('.datatable').dataTable({
	"order": [[ 0, "desc" ]]
});