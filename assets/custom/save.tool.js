$(document).ready(function(){

	var getToolDeferred = '';

	init();

	function init() {
		$('#tool_description').summernote({
			height: 250
		});			

		if($('#form-mode').val() == 'edit') {
			getToolDeferred = getTool($('#form-mode').attr('data-tool-id'));

			getToolDeferred.done(function(data){

				$('#tool_title').val(data.model.tool_title);
				$('#tool_author').val(data.model.tool_author);
				$('#tool_title').val(data.model.tool_title);
				$('#tool_type').val(data.model.tool_type);
				$('#tool_category').val(data.model.tool_category);
				$('#tool_tags').val(data.model.tool_tags);
				$("#tool_description").summernote("code",data.model.tool_description);

				$('#tool_tags').tagsinput({
					tagClass: 'label label-primary'
				});
			});
		} else {
			$('#tool_tags').tagsinput({
				tagClass: 'label label-primary'
			});

			$('#version_remarks').summernote({
				height: 250
			});	

			$('#cms_support').multiselect();
		}
	}

	$('#save-tool-btn').on('click',function(event){
		event.preventDefault();
		saveTool($('#form-mode').attr('data-tool-id'));
	});

	$('#tool_title').on('keyup', function(){
		$('.tool-title').text($(this).val());
	});

	function saveTool(tool_id) {
		$('.master-cms-panel').addClass('serino-loading');

		var formData = new FormData($('#save-tool-form')[0]);

		if(tool_id) {
			setTimeout(function(){
				$.ajax({
					url: base_url+'v1/tools/'+tool_id,
					type: 'POST',
					contentType: false,
		        	processData: false,
					data: formData,
					dataType: 'json',
					accepts: 'application/json'
				}).done(function(data){
					if(data.successful) {
						$.notify(data.message, {type:"success"});
					} else {
						$.notify(data.message, {type:"danger"});
					}
				}).fail(function(){
					$('.master-cms-panel').addClass('serino-error');
				}).always(function(){
					$('.master-cms-panel').removeClass('serino-loading');
				});
			}, 1000);
		} else {
			setTimeout(function(){
				$.ajax({
					url: base_url+'v1/tools/',
					type: 'POST',
					contentType: false,
		        	processData: false,
					data: formData,
					dataType: 'json',
					accepts: 'application/json'
				}).done(function(data){
					if(data.successful) {
						window.location = base_url+'tools/view_tool/'+data.tool_id;
					} else {
						$.notify(data.message, {type:"danger"});
						$('.master-cms-panel').removeClass('serino-loading');
					}
				}).fail(function(){
					$('.master-cms-panel').addClass('serino-error');
				});
			}, 1000);
		}
	}
});