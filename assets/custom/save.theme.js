$(document).ready(function(){

	var getThemeDeferred = '';

	init();

	function init() {
		$('#theme_description').summernote({
			height: 250
		});			

		if($('#form-mode').val() == 'edit') {
			getThemeDeferred = getTheme($('#form-mode').attr('data-theme-id'));

			getThemeDeferred.done(function(data){

				$('#theme_title').val(data.model.theme_title);
				$('#theme_author').val(data.model.theme_author);
				$('#theme_category').val(data.model.theme_category);
				$('#theme_tags').val(data.model.theme_tags);
				$("#theme_description").summernote("code",data.model.theme_description);

				$('#theme_tags').tagsinput({
					tagClass: 'label label-primary'
				});
			});
		} else {
			$('#theme_tags').tagsinput({
				tagClass: 'label label-primary'
			});

			$('#version_remarks').summernote({
				height: 250
			});	

			$('#cms_support').multiselect();
		}
	}

	$('#save-theme-btn').on('click',function(event){
		event.preventDefault();
		saveTheme($('#form-mode').attr('data-theme-id'));
	});

	$('#theme_title').on('keyup', function(){
		$('.theme-title').text($(this).val());
	});

	function saveTheme(theme_id) {
		$('.master-cms-panel').addClass('serino-loading');

		var formData = new FormData($('#save-theme-form')[0]);

		if(theme_id) {
			setTimeout(function(){
				$.ajax({
					url: base_url+'v1/themes/'+theme_id,
					type: 'POST',
					contentType: false,
		        	processData: false,
					data: formData,
					dataType: 'json',
					accepts: 'application/json'
				}).done(function(data){
					if(data.successful) {
						$.notify(data.message, {type:"success"});
					} else {
						$.notify(data.message, {type:"danger"});
					}
				}).fail(function(){
					$('.master-cms-panel').addClass('serino-error');
				}).always(function(){
					$('.master-cms-panel').removeClass('serino-loading');
				});
			}, 1000);
		} else {
			setTimeout(function(){
				$.ajax({
					url: base_url+'v1/themes/',
					type: 'POST',
					contentType: false,
		        	processData: false,
					data: formData,
					dataType: 'json',
					accepts: 'application/json'
				}).done(function(data){
					if(data.successful) {
						window.location = base_url+'themes/view_theme/'+data.theme_id;
					} else {
						$.notify(data.message, {type:"danger"});
						$('.master-cms-panel').removeClass('serino-loading');
					}
				}).fail(function(){
					$('.master-cms-panel').addClass('serino-error');
				});
			}, 1000);
		}
	}
});