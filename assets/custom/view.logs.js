$(document).ready(function(){
	//Datatables
	$('.datatable').dataTable({
		responsive: true,
		'order': [[0, 'desc']],
    	aoColumnDefs: [
		  {
		     bSortable: false,
		     bSearchable: false,
		     aTargets: [-1]
		  }
		]
	});
});