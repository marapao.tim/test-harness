$(function(){
	var table = $('#permissions-table');
	
	init();

	function init() {
		getPermissions();
	}

	function getPermissions() {
		$('.master-cms-panel').addClass('serino-loading');

		$.ajax({
			url: base_url+'v1/permissions',
			method: 'GET',
			accepts: 'application/json',
			dataType: 'json',
			beforeSend:function() {
				table.dataTable().fnDestroy();
			}
		}).done(function(data){
			var template = $('#permission-row-template').html();
			var table_tbody = '';
			Mustache.parse(template);

			$(data.items).each(function(i, rows){
				var row_html = Mustache.render(template, rows);
				table_tbody += row_html;
			});

			$('#permissions-table tbody').html(table_tbody);
			//Datatables
			table.dataTable({
				responsive: true,
				destroy: true,
				'order': [],
		    	'aoColumnDefs': [
				  {
				     bSortable: false,
				     bSearchable: false,
				     aTargets: [-1]
				  }
				]
			});
		}).always(function(){
				$('.master-cms-panel').removeClass('serino-loading');
		}).fail(function(){
			$('.master-cms-panel').addClass('serino-error');
		});
	}

	$(document).on('click', '.delete-permission-btn', function(event){
		event.preventDefault();

		var permission_id = $(this).attr('data-permission-id');

		bootbox.confirm('Are you sure?', function(response){
			if(response) {
				deletePermissionDeferred = deletePermission(permission_id);
				deletePermissionDeferred.done(function(data){
					if(data.successful) {
						getPermissions();
						$.notify(data.message, {type:"success"});
					} else {
						$.notify(data.message, {type:"danger"});
					}

				});
			} else {
				$('.modal').modal('hide');
			}
		});
	});
});