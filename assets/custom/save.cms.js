$(function(){
	init();

	function init() {
		$('#release_notes').summernote({
			height: 250
		});
	}

	$('#save-cms-btn').on('click',function(event){
		event.preventDefault();
		saveCms($('#form-mode').attr('data-cms-id'));
	});

	$('#codename').on('keyup', function(event){
		event.preventDefault();
		$('.page-header span.cms_codename').text($(this).val());
	});

	function saveCms(cms_id) {
		var formData = new FormData($('#save-cms-form')[0]);
		$('.master-cms-panel').addClass('serino-loading');

		setTimeout(function(){
			$.ajax({
				url: base_url+'v1/cms/',
				type: 'POST',
				contentType: false,
	        	processData: false,
				data: formData,
				dataType: 'json',
				accepts: 'application/json'
			}).done(function(data){
				if(data.successful) {
					window.location = base_url+'cms/cms_management';
				} else {
					$.notify(data.message, {type:"danger"});
					$('.master-cms-panel').removeClass('serino-loading');
				}
			}).fail(function(){
				$('.master-cms-panel').addClass('serino-error');
			})
		}, 1000);
	}

});