$(document).ready(function(){

	var getPluginDeferred = '';

	init();

	function init() {
		$('#description').summernote({
			height: 250
		});			

		if($('#form-mode').val() == 'edit') {
			getPluginDeferred = getPlugin($('#form-mode').attr('data-plugin-id'));

			getPluginDeferred.done(function(data){

				$('#author').val(data.model.author);
				$('#title').val(data.model.title);
				$('#category').val(data.model.category);
				$('#tags').val(data.model.tags);
				$("#description").summernote("code",data.model.description);

				$('#tags').tagsinput({
					tagClass: 'label label-primary'
				});
			});
		} else {
			$('#tags').tagsinput({
				tagClass: 'label label-primary'
			});

			$('#version_remarks').summernote({
				height: 250
			});	

			$('#cms_support').multiselect();
		}
	}

	$('#save-plugin-btn').on('click',function(event){
		event.preventDefault();
		savePlugin($('#form-mode').attr('data-plugin-id'));
	});

	$('#title').on('keyup', function(){
		$('.plugin-title').text($(this).val());
	});

	function savePlugin(plugin_id) {
		$('.master-cms-panel').addClass('serino-loading');

		var formData = new FormData($('#save-plugin-form')[0]);

		if(plugin_id) {
			setTimeout(function(){
				$.ajax({
					url: base_url+'v1/plugins/'+plugin_id,
					type: 'POST',
					contentType: false,
		        	processData: false,
					data: formData,
					dataType: 'json',
					accepts: 'application/json'
				}).done(function(data){
					if(data.successful) {
						$.notify(data.message, {type:"success"});
					} else {
						$.notify(data.message, {type:"danger"});
					}
				}).fail(function(){
					$('.master-cms-panel').addClass('serino-error');
				}).always(function(){
					$('.master-cms-panel').removeClass('serino-loading');
				});
			}, 1000);
		} else {
			setTimeout(function(){
				$.ajax({
					url: base_url+'v1/plugins/',
					type: 'POST',
					contentType: false,
		        	processData: false,
					data: formData,
					dataType: 'json',
					accepts: 'application/json'
				}).done(function(data){
					if(data.successful) {
						window.location = base_url+'plugin/view_plugin/'+data.plugin_id;
					} else {
						$.notify(data.message, {type:"danger"});
						$('.master-cms-panel').removeClass('serino-loading');
					}
				}).fail(function(){
					$('.master-cms-panel').addClass('serino-error');
				});
			}, 1000);
		}
	}
});