var cm;
$(function(){
	
	var _datatable = $('.datatable').dataTable({
		responsive: true,
		'order': [],
	});

	$(document).on("change","[name='schedule[recur]']",function(){
		var _this = $(this);

		$(".tabs-sched").addClass("hidden");
		$("#tab-"+_this.val()).removeClass("hidden");
	});

	$(document).on("submit","#form-add-runner",function(e){
		e.preventDefault();
		alert();
	});

	$("#add-runner").click(function(){
		bootbox.dialog({
			title: "Add New Runner",
			message: $("#modal-add-runner").html(),
			buttons: {
		        confirm: {
		            label: 'Confirm',
		            className: 'btn-primary',
		            callback: function(){
		            	var _this = $(this);
		            	var _modal = $(".bootbox.modal .modal-content");

		            	var formData = new FormData(_modal.find('form')[0]);

		            	var project_file = _modal.closest("[type='file']").prop('files');

						var form = _modal.closest('form');
						var fd = new FormData();

						$.ajax({
							url: _modal.find("form").attr('data-target'),
							type: 'POST',
							data: formData,
							processData: false,
							contentType: false,
							dataType: 'json',
							accepts: 'application/json',
							beforeSend: function() {
								
							},
							success: function(data) {
								_datatable.fnDestroy()
								$("#testrunner-table").parent().load(document.URL + " #testrunner-table",function(){
									_datatable = $('.datatable').dataTable({
										"order": [[ 0, "desc" ]]
									});
								});
							},
							complete: function(data) {

							},
							error: function(data) {

							}
						});
				    }
		        },
		        cancel: {
		            label: 'Cancel',
		            className: 'btn-default'
		        }
		    },

		});
	});


	$(document).on('click', '.delete-runner-btn', function(event){
		event.preventDefault();

		var template_id = $(this).attr('data-runner-id');
		var _url = $(this).attr("data-url");

		bootbox.confirm('Are you sure?', function(response){
			if(response) {

				$.ajax({
					url: _url,
					method: 'DELETE',
					accepts: 'application/json',
					dataType: 'json'
				}).done(function(data){
					//deferred.resolve(data);
				}).always(function(){
					_datatable.fnDestroy()
					$("#testrunner-table").parent().load(document.URL + " #testrunner-table",function(){
						_datatable = $('.datatable').dataTable({
							"order": [[ 0, "desc" ]]
						});
					});
				}).fail(function(){
					$.notify(data.message, {type:"danger"});
				});

			} else {
				$('.modal').modal('hide');
			}
		});
	});

	$(document).on('click', '.view-runner-results', function(event){
		var _this = $(this);

		$.ajax({
				url: _this.attr("data-url"),
				method: 'GET',
				accepts: 'application/json',
				dataType: 'json'
			}).done(function(data){
				//deferred.resolve(data);
				//data = $.parseJSON(data);
				var _t = "<table class='table'><thead> <tr> <th>File</th><th class='text-center'>Action</th> </tr> </thead><tbody>"
				$.each(data.model,function(i,val){
					_t += "<tr> <td>"+val+"</td><td class='text-center'><a href='"+val+"'><i class='fa fa-download'></i></a></td> </tr>";
				});
				_t += "</tbody></table>";

				bootbox.dialog({
					title: "Runner Results: "+ _this.attr("data-name"),
					message: _t,
					size: "large",
					buttons: {
				        
				        cancel: {
				            label: 'Close',
				            className: 'btn-default'
				        }
				    },

				});

			}).always(function(){

			}).fail(function(){
			$.notify(data.message, {type:"danger"});
		});


		
	});

	$(document).on('click', '.view-runner-history', function(event){
		var _this = $(this);

		$.ajax({
				url: _this.attr("data-url"),
				method: 'GET',
				accepts: 'application/json',
				dataType: 'json'
			}).done(function(data){
				//deferred.resolve(data);
				//data = $.parseJSON(data);
				var _t = "<table class='table'><thead> <tr> <th>Date</th><th class=''>Status</th> </tr> </thead><tbody>"
				$.each(data.list,function(i,val){
					_t += "<tr> <td>"+val.datecreated+"</td><td class='' style='position:relative'><a class='project-status-circle "+(val.status == "Successful" ? "completed" : "cancelled")+"' href='javascript:;'></a>"+val.status+"</td> </tr>";
				});
				_t += "</tbody></table>";

				bootbox.dialog({
					title: "Runner History: "+ _this.attr("data-name"),
					message: _t,
					size: "large",
					buttons: {
				        
				        cancel: {
				            label: 'Close',
				            className: 'btn-default'
				        }
				    },

				});

			}).always(function(){

			}).fail(function(){
			$.notify(data.message, {type:"danger"});
		});


		
	});
})