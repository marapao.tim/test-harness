var cm;
var tr;
var new_cache_obj;
$(function(){
	
	var _datatable = $('.datatable').dataTable({
		responsive: true,
		'order': [],
	});

	//var myCodeMirror = CodeMirror.fromTextArea($("#template_body")[0]);
	if($("#template_body").length > 0){
		cm = CodeMirror.fromTextArea($("#template_body")[0], {
						lineNumbers: true,
						//lineWrapping: true,
						mode: 'xml',
						indentUnit: 4,
						tabindex: 1,
						//inputStyle: "contenteditable",
						
						placeholder: 'No contents'
					});

		cm.setOption("mode", "xml");

		//$("#template_body").parent().find(".CodeMirror").css({"height":"300px"});
	}

	if($("#test_result").length > 0){
		tr = CodeMirror.fromTextArea($("#test_result")[0], {
						lineNumbers: true,
						//lineWrapping: true,
						mode: 'xml',
						indentUnit: 4,
						tabindex: 1,
						//inputStyle: "contenteditable",
						
						placeholder: 'No contents'
					});

		tr.setOption("mode", "xml");

		//$("#test_result").parent().find(".CodeMirror").css({"height":"500px"});
	}


	$(document).on('click', '.delete-testscript-btn', function(event){
		event.preventDefault();

		var testscript_id = $(this).attr('data-testscript-id');

		bootbox.confirm('Are you sure?', function(response){
			if(response) {

				$.ajax({
					url: base_url+'jmeter/test_script/delete/'+testscript_id,
					method: 'DELETE',
					accepts: 'application/json',
					dataType: 'json'
				}).done(function(data){
					//deferred.resolve(data);
				}).always(function(){
					_datatable.fnDestroy()
					$("#testscript-table").parent().load(document.URL + " #testscript-table",function(){
						_datatable = $('.datatable').dataTable({
							responsive: true,
							"order": []
						});
					});
				}).fail(function(){
					$.notify(data.message, {type:"danger"});
				});

			} else {
				$('.modal').modal('hide');
			}
		});
	});



	$("#run-script-btn").click(function(){
		var _progress = $("<div>");
		var _this = $(this);

		bootbox.confirm("Proceed with Testing?",function(resp){
			if(resp){
				bootbox.dialog({
					message: _progress,
					
				});

				_progress.html("Preparing Script...");

				$.post(
					_this.attr("data-id")+"/generate-script",
					{
						"script":$("#template_body").html()
					},
					function(data){
						//console.log(data);
						tr.setValue(data);  
						/*===================================================================*/
						new_cache_obj = tr.getValue();
            			localStorage.setItem('new_cache_obj', new_cache_obj); 
            			/*====================================================================*/
						bootbox.hideAll();
						$('[href="#template_panel"]').trigger("click");
					}
				)
			}
		})
		
	}); 

	/*=======================================================================================*/ 

	localStorage.removeItem('new_cache_obj');

	$(".output").resizable({
        handles: 'n, s'
    }); 

	$('.output ul.list-format li:first-child a').find('i').hide(); 
    $('.output ul.list-format li:nth-child(2) a').find('i').hide(); 
    $('.output ul.list-format li:last-child a').find('i').hide();

	$(document).on("click",".output .pull-right a:first-child",function(e){
		$('.output').css({'top' : '640px'});  
	});

	$(document).on("click",".output .pull-right a:last-child",function(e){
		$('.output').css({'top' : '140px'});   
	});

	$(document).on("click",".output ul.list-format li:first-child a",function(e){ 
		$('.output ul.list-format li:first-child a').find('i').show();
 		tr.setValue(localStorage.getItem('new_cache_obj'));
 		$('.output ul.list-format li:first-child a').find('i').hide();     
	});

	$(document).on("click",".output ul.list-format li:nth-child(2) a",function(e){
		$('.output ul.list-format li:nth-child(2) a').find('i').show();
	 	$.ajax({
		    dataType: 'json',
		    type:'post',
		    url: base_url + "test_scripts/display_headers/", 
		    data:{
		    	test_result:localStorage.getItem('new_cache_obj'), 
		    },
		}).done(function(result){
 			tr.setValue(result); 
 			//console.log(result);
 			$('.output ul.list-format li:nth-child(2) a').find('i').hide();  
		});
	 	
	}); 

	$(document).on("click",".output ul.list-format li:last-child a",function(e){ 
		//$('.output ul.list-format li:last-child a').find('i').show();
		display_response_tabs();
		_display_response(0);
	 	 
	}); 

	function display_response_tabs(){
		 $('.output ul.list-format li:last-child a').find('i').show();  
		$.ajax({
		    dataType: 'json',
		    type:'post',
		    url: base_url + "test_scripts/count_response/", 
		    data:{
		    	test_result:localStorage.getItem('new_cache_obj'), 
		    },
		}).done(function(result){
			var row = ''; 
			var arrayIndex;
			//tr.setValue(result); 
			//console.log(result);
 			 for (var i = 1; i <= result; i++) {
 			 	arrayIndex = parseInt(i) - 1;
 			 	row += '<li><a onclick="_display_response('+ arrayIndex +'); return false" href="#">Response '+ i +'</a></li>';
 			 }
 			 $('ul.reponse-tabs').html(row);
 			 $('ul.reponse-tabs li:first-child').addClass('active');  
 			 $('.output ul.list-format li:last-child a').find('i').hide();  
		});
	}

	$(document).on("click","ul.reponse-tabs li",function(e){ 
		$('ul.reponse-tabs li').removeClass('active');  
		$(this).addClass('active');  
	}); 

})


function _display_response(arrayIndex){
	$.ajax({
		   dataType: 'html',
		   type:'post',
		   url: base_url + "test_scripts/display_response/", 
		   data:{
		    test_result:localStorage.getItem('new_cache_obj'),
		    arrayIndex:arrayIndex, 
		   },
	}).done(function(result){ 
 		var doc = document.getElementById('iframe').contentWindow.document;
		doc.open();
		doc.write(result);
		doc.close();  
	});   
}