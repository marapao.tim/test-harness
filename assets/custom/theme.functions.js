function getTheme(theme_id) {
	var deferred = $.Deferred();

	$('.master-cms-panel').addClass('serino-loading');
	
	setTimeout(function(){
		$.ajax({
			url: base_url+'v1/themes/'+theme_id,
			method: 'GET',
			accepts: 'application/json',
			dataType: 'json'
		}).done(function(data){
			deferred.resolve(data);
		}).always(function(){
			$('.master-cms-panel').removeClass('serino-loading');
			
		}).fail(function(){
			$('.master-cms-panel').addClass('serino-error');
		});
	}, 1000);

	return deferred.promise();
}

function deleteTheme(theme_id) {
	var deferred = $.Deferred();

	$('.master-cms-panel').addClass('serino-loading');

	setTimeout(function(){
		$.ajax({
			url: base_url+'v1/themes/'+theme_id,
			method: 'DELETE',
			accepts: 'application/json',
			dataType: 'json'
		}).done(function(data){
			deferred.resolve(data);
		}).always(function(){
			
		}).fail(function(){
			$.notify(data.message, {type:"danger"});
		});
	}, 500);

	return deferred.promise();
}