$(function(){

	init();

	function init() {
		$('#version_remarks').summernote({
			height: 250
		});

		$('#cms_support').multiselect();
	}

	$(document).on('click','.update-tool-btn', function(event){
		event.preventDefault();
		updateTool($('#form-mode').attr('data-tool-id'));
	});

	function updateTool(tool_id) {
		$('.master-cms-panel').addClass('serino-loading');

		var formData = new FormData($('#update-tool-form')[0]);

		console.log($('#update-tool-form')[0]);

		setTimeout(function(){
			$.ajax({
				url: base_url+'v1/tools/'+tool_id+'/update',
				type: 'POST',
				contentType: false,
	        	processData: false,
				data: formData,
				dataType: 'json',
				accepts: 'application/json'
			}).done(function(data){
				if(data.successful) {
					window.location = base_url+'tools/view_tool/'+getUriSegment(4);
				} else {
					$.notify(data.message, {type:"danger"});
				}
			}).fail(function(){
				$('.master-cms-panel').addClass('serino-error');
			}).always(function(){
				$('.master-cms-panel').removeClass('serino-loading');
			});
		}, 1000);
	}
});